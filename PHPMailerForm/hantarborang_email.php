<?php
use PHPMailer\PHPMailer\PHPMailer;

require_once 'phpmailer/Exception.php';
require_once 'phpmailer/PHPMailer.php';
require_once 'phpmailer/SMTP.php';

$mail = new PHPMailer(true);
$alert = '';
try{
  $mail->isSMTP();
  $mail->Host = 'smtp.gmail.com';
  $mail->SMTPAuth = true;
    $mail->Username = 'nnz.workie@gmail.com'; // Gmail address which you want to use as SMTP server
    $mail->Password = 'nnzwork@123'; // Gmail address Password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port = '587';

    $mail->SetFrom('jnadmin@moe.gov.my', 'Jemaah Nazir');
    $mail->addAddress($emelgb); // Email Penerima
    $mail->isHTML(true);
    $mail->AddReplyTo("jnadmin@moe.gov.my");
    $mail->Subject = "PENGESAHAN PERMOHANAN NAZIR OLEH ".$_SESSION['namapemohon'];
    $mail->Body = "
    <h3>PERMOHONAN NAZIR SEKOLAH<br>
    Pegawai berikut telah membuat permohonan nazir :<br><br>
    Nama : ".$_SESSION['namapemohon']."<br>
    No Kad Pengenalan : ".$_SESSION['nokppemohon']."<br>
    Token : ".$_SESSION['tokenpemohon']."<br>
    Pautan Pengeshan : <a href='http://localhost/admin-borang/pengesahan.php'>http://enazirlatihan.moe.gov.my/form/pengesahan.php</a><br><br>
    Kerjasama dan perhatian Tuan/Puan amatlah di hargai.<br><br>
    Sekian, terima kasih.<br><br>
    URUSETIA PERMOHONAN NAZIR.<br><br><br>

    <hr><h4 align=center>Emel ini dihantar secara automatik oleh komputer, sila jangan balas emel ini</h4>";

    $mail->send();
    $alert = '<div class="alert-success">
    <span>Message Sent to your Email! Thank you for contacting us.</span>
    </div>';
  } catch (Exception $e){
    $alert = '<div class="alert-error">
    <span>'.$e->getMessage().'</span>
    </div>';
  }
?>
