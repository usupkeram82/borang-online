<?php include("utilities/menu.php"); ?>
<?php include("utilities/header.php"); ?>

<body>

  <table width="70%" border="0" cellpadding="0" cellspacing="0" class="table-bordered">

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <div class="card card-primary">
                <!-- /.card-header -->
                <div class="card-body">
                  <fieldset>
                    <legend>
                      <font color="#3333FF">MAKLUMAT TAWARAN</font>
                    </legend>
                    PERMOHONAN NAZIR BAHARU 2022 KINI BOLEH DIBUAT SECARA ATAS TALIAN<br>
                    PERMOHONAN INI AKAN DIAMBIL SECARA BERPERINGAKAT BERGANTUNG PADA TARIKH PERMOHONAN<br>
                    SEBARANG MAKLUMAT KEPADA PEMOHON AKAN DIBERI MELALUI EMEL PEMOHON
                    <br>
                    <br>
                    Calon yang berkelayakan dan memenuhi syarat permohonan boleh mengemukakan<br>
                    permohonan secara dalam talian.
                    <br>
                    <br>
                    Sebarang persoalan boleh hubungi urusetia Jemaah.<br>
                    Terima Kasih
                    <br>
                    <br>
                    <!-- PERMOHONAN HLP / HLPS PPP KPM TAHUN 2022 -->
                  </fieldset>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-4">
              <!-- Form Element sizes -->
              <div class="card card-orange card-outline">
                <div class="card-header">
                  <h5 class="card-title m-0 card-outline">MENU</h5>
                </div>
                <div class="card-body">
                <i class="fas fa-edit"></i><a href="permohonannazir.php" style="background-color:yellow;">Borang Permohonan Nazir Baharu Online </a><br>
			        	<i class="fas fa-edit"></i><a href="permohonannazir.php"> Kemaskini Borang Permohonan </a><br>
				        <i class="fas fa-edit"></i><a href="pengesahan.php"> Pengesahan Guru Besar </a><br>
              </div> 
              </div>

              <!-- Form Element sizes -->
              <div class="card card-orange card-outline">
                <div class="card-header">
                  <h5 class="card-title m-0 card-outline">SEMAKAN KEPUTUSAN PERMOHONAN</h5>
                </div>
                <div class="card-body">
                <a href="#"> Semak Keputusan Permohonan Nazir Baharu</a><br>
              </div> 
              </div>

              </div>

            </div>
            <!--/.col (right) -->
          </div>
          <!-- /.row -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </table>
</body>
<?php include('utilities/footer.php'); ?>