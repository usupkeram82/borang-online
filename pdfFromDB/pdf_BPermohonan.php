<?php
    session_start();
    require_once 'FPDF/fpdf.php';
    require_once 'connection.php';
    // $nokp = '990215075786';
    
    if(isset($_GET['u']))
    {
        $nokp = $_GET['u'];
        $sql = " select * from fm_tbl_pemohon where pem_nokp_baru = '$nokp'";
        $data = mysqli_query($con,$sql);

        $pdf = new FPDF('P','mm','A4');
        $pdf->SetFont('Times', '', '12');
        $pdf->AddPage(); 
 
        while($row = mysqli_fetch_assoc($data))
        {
            //$pdf->cell('width', 'hight', 'text', 'border [1/0]', 'newline [1/0]', 'Center/Right/Left');
            $pdf->cell('200','4', 'JEMAAH NAZIR', '0', '1', 'C');
            $pdf->cell('200','4', 'KEMENTERIAN PENDIDIKAN MALAYSIA', '0', '1', 'C');
            $pdf->Ln();
            $pdf->cell('200','4', 'PERMOHONAN UNTUK JAWATAN NAZIR SEKOLAH', '0', '1', 'C');
            $pdf->cell('200','4', 'PEGAWAI PERKHIDMATAN PENDIDIKAN', '0', '1', 'C');
            $pdf->Ln();
            // image passport
            $pdf->Image('../datafiles/profile/'.$row['pem_gambar'],100,30,20);
            $pdf->cell('200', '30', '', '0', '0', 'C');
            $pdf->Ln();
            $pdf->cell('51', '5', 'Gred Penyandang    : ', '0', '0', 'R');
            $pdf->cell('27','5', $row['pem_gred'], '0', '0', 'L');
            $pdf->cell('70', '5', 'Opsyen Mata Pelajaran (Major)  :', '0', '0', 'R');
            $pdf->cell('5','5', $row['pem_opsyen_major'], '0', '1', 'L');
            $pdf->cell('148', '5', 'Opsyen Mata Pelajaran (Minor)  :', '0', '0', 'R');
            $pdf->cell('5','5', $row['pem_opsyen_minor'], '0', '0', 'L');
            $pdf->Ln();
            $pdf->cell('50', '5', 'Pilihan Penempatan :', '0', '0', 'R');
            $pdf->cell('6', '5', ' 1.', '0', '0', 'R');
            $pdf->cell('20','5', $row['pem_pilihan1'], '0', '1', 'L');
            $pdf->cell('56', '5', ' 2.', '0', '0', 'R');
            $pdf->cell('10','5', $row['pem_pilihan2'], '0', '1', 'L');
            $pdf->Ln();
            $pdf->cell('41', '10', 'BAHAGIAN A', '0', '1', 'R');
            $pdf->cell('65', '7', '1. BUTIR-BUTIR PERIBADI', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('73', '7', '1.1  Nama Penuh                           :', '0', '0', 'R');
            $pdf->cell('45','7', $row['pem_nama_main'], '0', '1', 'L');
            $pdf->cell('73', '7', '1.2  Jantina                                    :', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_jantina'], '0', '1', 'L');
            $pdf->cell('73', '7', '1.3  No. Kad Pengenalan (Baru)   :', '0', '0', 'R');
            $pdf->cell('20', '7', $row['pem_nokp_baru'], '0', '1', 'L');
            $pdf->cell('74', '7', '1.4  Tarikh Lahir                           : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_tkh_Lahir'], '0', '1', 'L');
            $pdf->cell('74', '7', '1.5  Umur pada 1 Januari 2022     : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_umur_mohon'], '0', '1', 'L');
            $pdf->cell('74', '7', '1.6  Tempat Lahir (Negeri)           : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_tpt_lahir'], '0', '1', 'L');
            $pdf->cell('74', '7', '1.7  Alamat Pejabat                       : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_pjbt_alamat'], '0', '1', 'L');
            $pdf->cell('74', '7', '      Poskod                                   : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_pjbt_poskod'], '0', '1', 'L');
            $pdf->cell('74', '7', 'No. Telefon Pejabat              : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_pjbt_phone'], '0', '1', 'L');
            $pdf->cell('74', '7', '1.8  Alamat Rumah                       : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_rumah_alamat'], '0', '1', 'L');
            $pdf->cell('74', '7', '      Poskod                                   : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_rumah_poskod'], '0', '1', 'L');
            $pdf->cell('74', '7', 'No. Telefon (Bimbit)             : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_rumah_phone'], '0', '1', 'L');
            
            $sqlaktifkan = " select * from fm_tbl_aktifkan WHERE aktif_nokp = '$nokp'";
            $aktif = mysqli_query($con,$sqlaktifkan);
            while($rows = mysqli_fetch_assoc($aktif)){
                $pdf->cell('74', '7', '      Email                                     : ', '0', '0', 'R');
                $pdf->cell('40', '7', $rows['aktif_email'], '0', '1', 'L');
            }

            $pdf->cell('73', '7', '1.9  Taraf Perkahwinan                :', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_kahwin_status'], '0', '1', 'L');
            $pdf->cell('75', '7', '           Jika berkahwin nyatakan      :- ', '0', '1', 'R');
            $pdf->cell('74', '7', '        Nama Suami/Isteri                : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_kahwin_psg_nama'], '0', '1', 'L');
            $pdf->cell('74', '7', '      Pekerjaan Suami/Isteri          : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_kahwin_psg_kerja'], '0', '1', 'L');
            $pdf->cell('74', '7', '      Alamat Tempat Bertugas      : ', '0', '0', 'R');
            $pdf->cell('40', '7', $row['pem_kahwin_psg_pejabat'], '0', '1', 'L');
            $pdf->Ln();
            $pdf->AddPage();

            //BAHAGIAN B
            $pdf->Ln(5);
            $pdf->cell('41', '10', 'BAHAGIAN B', '0', '1', 'R');
            //2. BUTIR-BUTIR PERKIHDMATAN
            $pdf->cell('80', '7', '2. BUTIR-BUTIR PERKIHDMATAN', '0', '1', 'R');

            $sqlperkhidmatan = " select * from fm_tbl_perkhidmatan WHERE pkdt_nokp = '$nokp'";
            $perkhidmatan = mysqli_query($con,$sqlperkhidmatan);
            while($pkdt = mysqli_fetch_assoc($perkhidmatan))
            {
                $pdf->cell('65', '7', '2.1  Tarikh lantikan pertama :', '0', '0', 'R');
                $pdf->cell('40', '7', $pkdt['pkdt_tkh_lantik'], '0', '1', 'L');
                $pdf->cell('105', '7', '2.2  Tarikh lantikan ke skim perkhidmatan sekarang :', '0', '0', 'R');
                $pdf->cell('40', '7', $pkdt['pkdt_tkh_skg'], '0', '1', 'L');
                $pdf->cell('87', '7', '2.3  Tarikh disahkan dalam perkhidmatan :', '0', '0', 'R');
                $pdf->cell('40', '7', $pkdt['pkdt_tkh_sah'], '0', '1', 'L');
                $pdf->cell('88', '2', '(Dalam skim perkhidmatan sekarang)', '0', '1', 'R');
                $pdf->Ln();
                $pdf->cell('88', '7', '2.4  Gred hakiki penyandang : PPP GRED ', '0', '0', 'R');
                $pdf->cell('40', '7', $pkdt['pkdt_gred_hakiki'], '0', '1', 'L');
                $pdf->cell('56', '7', '2.5  Jawatan sekaraang :', '0', '0', 'R');
                $pdf->cell('40', '7', $pkdt['pkdt_jwtn_skg'], '0', '1', 'L');
                $pdf->cell('48', '7', '2.6 Taraf jawatan : ', '0', '0', 'R');
                $pdf->cell('40', '7', $pkdt['pkdt_taraf_pencen'], '0', '1', 'L');
                $pdf->Ln(5);
    
            }
           
           
            //3. KELAYAKAN AKADEMIK CALON
                $pdf->cell('85', '10', '3. KELAYAKAN AKADEMIK CALON', '0', '0', 'R');
                $pdf->Ln();
                $pdf->cell('10', '27', 'Bil', '1', '0', 'C');
                $pdf->cell('60', '27', 'Nama Maktab/', '1', '0', 'C');
                $pdf->cell('30', '27', 'Peringkat ', '1', '0', 'C');
                $pdf->cell('60', '27', 'Bidang', '1', '0', 'C');
                $pdf->cell('30', '27', 'Tahun', '1', '1', 'C');
                $pdf->cell('80', '-15', 'Institusi/Universiti', '0', '0', 'C');
                $pdf->cell('110', '-15', 'Penganugerahan', '0', '1', 'R');
                $pdf->Ln(15);
            $sqlakademik = " select * from fm_tbl_akademik WHERE aka_pemohon_nokp = '$nokp'";
            $akademik = mysqli_query($con,$sqlakademik);
            while($aka = mysqli_fetch_assoc($akademik))
            {
                $pdf->cell('10', '10', $aka['akademik_id'], '1', '0', 'C');
                $pdf->cell('60', '10', $aka['aka_institusi'], '1', '0', 'C');
                $pdf->cell('30', '10', $aka['aka_peringkat'], '1', '0', 'C');
                $pdf->cell('60', '10', $aka['aka_bidang'], '1', '0', 'C');
                $pdf->cell('30', '10', $aka['aka_tahun'], '1', '1', 'C');
            }
            $pdf->Ln(5);

            //4. PENGALAMAN PERKHIDMATAN
            $pdf->cell('85', '7', '4. PENGALAMAN PERKHIDMATAN', '0', '1', 'R');
            $pdf->cell('70', '7', '4.1 Bilangan tahun berkhidmat', '0', '1', 'R');
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
		        if($pglmn['pglmn_group']=='4.1')
                {
                	if($pglmn['pglmn_keterangan']=='Sekolah Rendah')
                    {
                    	$pdf->cell('48', '7', 'Sekolah Rendah', '0', '0', 'R');
                    	$pdf->cell('75', '7', 'Tahun :', '0', '0', 'R');
                    	$pdf->cell('40', '7', $pglmn['pglmn_peringkat'], '1', '1', 'L');
                	}
                	else if($pglmn['pglmn_keterangan']=='Sekolah Menengah')
                	{
                    	$pdf->cell('53', '7', 'Sekolah Menengah', '0', '0', 'R');
                    	$pdf->cell('70', '7', 'Tahun :', '0', '0', 'R');
                    	$pdf->cell('40', '7', $pglmn['pglmn_peringkat'], '1', '1', 'L');
                	}    
                	else if($pglmn['pglmn_keterangan']=='Bahagian')
                	{
                    	$pdf->cell('50', '7', 'Bahagian             ', '0', '0', 'R');
                    	$pdf->cell('73', '7', 'Tahun :', '0', '0', 'R');
                    	$pdf->cell('40', '7', $pglmn['pglmn_peringkat'], '1', '1', 'L');
                	}
                	else if($pglmn['pglmn_keterangan']=='JPN/PPD')
                	{
                    	$pdf->cell('50', '7', 'JPN / PPD           ', '0', '0', 'R');
                    	$pdf->cell('73', '7', 'Tahun :', '0', '0', 'R');
                    	$pdf->cell('40', '7', $pglmn['pglmn_peringkat'], '1', '1', 'L');
                	}
                	else if($pglmn['pglmn_keterangan']=='Jumlah Tahun Berkhidmat')
                	{
                    	$pdf->cell('123', '7', 'Jumlah Tahun Keseluruhan Perkhidmatan                              ', '0', '0', 'R');
                    	$pdf->cell('40', '7', $pglmn['pglmn_peringkat'], '1', '1', 'L');
                    	$pdf->Ln();
                	}    
			    }
            }

            //4.1.1
            $pdf->cell('130', '10', '4.1.1 Senarai Pengalaman Perkhidmatan di Institusi Pendidikan', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('65', '15', 'Nama Institusi', '1', '0', 'C');
            $pdf->cell('50', '15', 'Jawatan', '1', '0', 'C');
            $pdf->cell('35', '15', 'Kategori ', '1', '0', 'C');
            $pdf->cell('20', '15', 'Dari', '1', '0', 'C');
            $pdf->cell('20', '15', 'Hingga', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='4.1.1')
                {
                    $pdf->cell('65', '10', $pglmn['pglmn_jns'], '1', '0', 'C');
                    $pdf->cell('50', '10', $pglmn['pglmn_keterangan'], '1', '0', 'C');
                    $pdf->cell('35', '10', $pglmn['pglmn_peringkat'], '1', '0', 'C');
                    $pdf->cell('20', '10', $pglmn['pglmn_mula'], '1', '0', 'C');
                    $pdf->cell('20', '10', $pglmn['pglmn_tamat'], '1', '1', 'C');
                   
                }
            }
            $pdf->Ln(15);

            $pdf->cell('49', '10', '8. KEGIATAN LUAR', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('90', '15', 'Persatuan / Pertubuhan', '1', '0', 'C');
            $pdf->cell('90', '15', 'Jawatan', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='8')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('90', '10', $pglmn['pglmn_keterangan'], '1', '0', 'C');
                    $pdf->cell('90', '10', $pglmn['pglmn_peringkat'], '1', '1', 'C');
                }
            }
            $pdf->Ln(15);
            
            //4.2
            $pdf->cell('100', '10', '4.2 Pengalaman Mengajar Di Institusi Pendidikan', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('85', '15', 'Mata Pelajaran Diajar', '1', '0', 'C');
            $pdf->cell('75', '15', 'Darjah/Tingkatan/Semester/Kohort ', '1', '0', 'C');
            $pdf->cell('20', '15', 'Tahun', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='4.2')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('85', '10', $pglmn['pglmn_keterangan'], '1', '0', 'C');
                    $pdf->cell('75', '10', $pglmn['pglmn_peringkat'], '1', '0', 'C');
                    $pdf->cell('20', '10', $pglmn['pglmn_mula_tahun'], '1', '1', 'C');
                }
            }          
            $pdf->Ln(15);

            //4.3
            $pdf->cell('165', '10', '4.3 Tugas Dan Tanggungjawab Lain semasa Di Institusi Pendidikan (3 Tahun Terakhir)', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('180', '15', 'Jawatan Dipegang ', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='4.3')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('180', '10', $pglmn['pglmn_keterangan'], '1', '1', 'L');
                }
            }
            $pdf->Ln(15);

            //5
            $pdf->cell('115', '10', '5. KURSUS-KURSUS YANG DIHADIRI (3 Tahun Terkini)', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('70', '15', 'Nama Kursus', '1', '0', 'C');
            $pdf->cell('70', '15', 'Tempat ', '1', '0', 'C');
            $pdf->cell('20', '15', 'Dari', '1', '0', 'C');
            $pdf->cell('20', '15', 'Hingga', '1', '1', 'C');
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='5')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('70', '10', $pglmn['pglmn_keterangan'], '1', '0', 'C');
                    $pdf->cell('70', '10', $pglmn['pglmn_peringkat'], '1', '0', 'C');
                    $pdf->cell('20', '10', $pglmn['pglmn_mula'], '1', '0', 'C');
                    $pdf->cell('20', '10', $pglmn['pglmn_tamat'], '1', '1', 'C');
                }
            }
            $pdf->Ln(15);
                      
            //6
            $pdf->cell('119', '10', '6. KAJIAN / PENERBITAN YANG PERNAH DIHASILKAN', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('150', '15', 'Nama Kajian / Penerbitan', '1', '0', 'C');
            $pdf->cell('30', '15', 'Tahun ', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='6')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('150', '10', $pglmn['pglmn_keterangan'], '1', '0', 'L');
                    $pdf->cell('30', '10', $pglmn['pglmn_mula_tahun'], '1', '1', 'C');
                }
            }
            $pdf->Ln(15);
           

            //7
            $pdf->cell('119', '10', '7. SUMBANGAN PROFESIONAL DI LUAR TUGAS RASMI', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('80', '15', 'Jenis Sumbangan', '1', '0', 'C');
            $pdf->cell('70', '15', 'Peringkat', '1', '0', 'C');
            $pdf->cell('30', '15', 'Tarikh ', '1', '1', 'C');
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='7')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('80', '10', $pglmn['pglmn_keterangan'], '1', '0', 'C');
                    $pdf->cell('70', '10', $pglmn['pglmn_peringkat'], '1', '0', 'C');
                    $pdf->cell('30', '10', $pglmn['pglmn_mula'], '1', '1', 'C');
                }
            }
            $pdf->Ln(15);
           
             
            //8
            $pdf->cell('49', '10', '8. KEGIATAN LUAR', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('90', '15', 'Persatuan / Pertubuhan', '1', '0', 'C');
            $pdf->cell('90', '15', 'Jawatan', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='8')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('90', '10', $pglmn['pglmn_keterangan'], '1', '0', 'C');
                    $pdf->cell('90', '10', $pglmn['pglmn_peringkat'], '1', '1', 'C');
                }
            }
            $pdf->Ln(15);
             
            //9
            $pdf->cell('110', '10', '9. PENGIKTIRAFAN / ANUGERAH YANG DITERIMA', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('150', '15', 'Jenis Pengiktirafan/Anugerah', '1', '0', 'C');
            $pdf->cell('30', '15', 'Tahun', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='9')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('150', '10', $pglmn['pglmn_keterangan'], '1', '0', 'C');
                    $pdf->cell('30', '10', $pglmn['pglmn_mula_tahun'], '1', '1', 'C');
                }
            }
            $pdf->Ln(15);
             

            //10
            $pdf->cell('145', '10', '10. KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI', '0', '0', 'R');
            $pdf->Ln();
            $pdf->cell('10', '15', 'Bil', '1', '0', 'C');
            $pdf->cell('60', '15', 'Jenis Aplikasi', '1', '0', 'C');
            $pdf->cell('40', '15', 'Peringkat Rendah', '1', '0', 'C');
            $pdf->cell('40', '15', 'Peringkat Sederhana', '1', '0', 'C');
            $pdf->cell('40', '15', 'Peringkat Tinggi', '1', '1', 'C');
            // $pdf->Ln(10);
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            $no = 1;
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='10')
                {
                    $pdf->cell('10', '10', $no++, '1', '0', 'C');
                    $pdf->cell('60', '10', $pglmn['pglmn_keterangan'], '1', '0', 'L');
                    $pdf->cell('40', '10', $pglmn['pglmn_peringkat']=='Peringkat Rendah', '1', '0', 'C');
                    $pdf->cell('40', '10', $pglmn['pglmn_peringkat']=='Peringkat Sederhana', '1', '0', 'C');
                    $pdf->cell('40', '10', $pglmn['pglmn_peringkat']=='Peringkat Tinggi', '1', '1', 'C');
                }       
            }
            $pdf->Ln(15);
            $pdf->AddPage(); 

            //11
            $pdf->cell('70', '10', '11. MAKLUMAT-MAKLUMAT LAIN', '0', '0', 'R');
            $pdf->Ln();
            $sqlextrainfo = " select * from fm_tbl_extrainfo WHERE ei_pemohon_nokp = '$nokp'";
            $extrainfo = mysqli_query($con,$sqlextrainfo);
            while($ei = mysqli_fetch_assoc($extrainfo))
            {
                    //11.1
                    if($ei['ei_code']=='11.1')
                    {
                        $pdf->cell('170', '7', '11.1 Pernahkan anda memohon jawatan Nazir Sekolah?', '0', '0', 'L ');
                        $pdf->cell('20', '7', $ei['ei_jawapan'], '1', '1', 'R');
                        $pdf->cell('40', '7', 'Jika YA, Tahun :', '0', '0', 'R');
                        $pdf->cell('40', '7', $ei['ei_extra1'], '0', '1', 'L');
                        $pdf->Ln(5);
                    }
                    //11.2
                    else if($ei['ei_code']=='11.2')
                    {
                        $pdf->cell('170', '7', '11.2 Pernahkan anda menghadiri temu duga Nazir Sekolah?', '0', '0', 'L ');
                        $pdf->cell('20', '7', $ei['ei_jawapan'], '1', '1', 'R');
                        $pdf->cell('40', '7', 'Jika YA, Tahun :', '0', '0', 'R');
                        $pdf->cell('40', '7', $ei['ei_extra1'], '0', '1', 'L');
                        $pdf->Ln(5);
                    }
                
                    //11.3
                    else if($ei['ei_code']=='11.3')
                    {
                        $pdf->cell('68', '7', '11.3 Adakah anda memohon bertukar ke tempat atau jawatan lain di ', '0', '1', 'L ');
                        $pdf->cell('170', '2', '        Bahagian-Bahagian KPM dalam tahun ini?', '0', '0', 'L ');
                        $pdf->cell('20', '7', $ei['ei_jawapan'], '1', '1', 'R');
                        $pdf->cell('45', '7', 'Jika YA, Bahagian :', '0', '0', 'R');
                        $pdf->cell('40', '7', $ei['ei_extra1'], '0', '1', 'L');
                        $pdf->Ln(5);
                    }
                    
                    //11.4
                    else if($ei['ei_code']=='11.4')
                    {
                        $pdf->cell('170', '7', '11.4 Adakah anda sedang mengikuti kursus jangka pendek / panjang / NPQEL?', '0', '0', 'L ');
                        $pdf->cell('20', '7', $ei['ei_jawapan'], '1', '1', 'R');
                        $pdf->cell('50', '7', 'Jika YA, nama kursus :', '0', '0', 'R');
                        $pdf->cell('40', '7', $ei['ei_extra1'], '0', '1', 'L');
                        $pdf->cell('46', '7', 'Tarikh tamat kursus :', '0', '0', 'R');
                        $pdf->cell('40', '7', $ei['ei_extra2'], '0', '1', 'L');
                        $pdf->Ln(5);
                    }  
               
                    //11.5
                    else if($ei['ei_code']=='11.5')
                    {
                        $pdf->cell('170', '7', '11.5 Adakah anda sedang mengikuti program Pendidikan Jarak Jauh (PJJ) / Separuh Masa?', '0', '0', 'L ');
                        $pdf->cell('20', '7', $ei['ei_jawapan'], '1', '1', 'R');
                        $pdf->cell('54', '7', 'Jika YA, nama program :', '0', '0', 'R');
                        $pdf->cell('40', '7', $ei['ei_extra1'], '0', '1', 'L');
                        $pdf->cell('50', '7', 'Tarikh Jangka Tamat  :', '0', '0', 'R');
                        $pdf->cell('40', '7', $ei['ei_extra2'], '0', '1', 'L');
                        $pdf->Ln(5);
                    }
                    
                    //11.6
                    else
                    {
                        $pdf->cell('68', '7', '11.6 Maklumat-maklumat lain, jika ada :', '0', '1', 'L ');
                        $pdf->cell('20', '7', $ei['ei_extra1'], '0', '1', 'R');
                        $pdf->Ln(7);
                    }
                    
            }
            //12
            $pdf->cell('70', '7', '12. PUSAT TEMUDUGA YANG DIPILIH', '0', '1', 'L ');
            $sqlpengalaman = " select * from fm_tbl_pengalaman WHERE pglmn_pemohon_nokp = '$nokp'";
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='12')
                {
                    $pdf->cell('30', '7', $pglmn['pglmn_keterangan'], '0', '1', 'L');
                }
            }
            $pdf->Ln(10);
          

            //13
            $pdf->cell('65', '7', '13. SEBAB-SEBAB MEMOHON JAWATAN INI :', '0', '1', 'L ');
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='13')
                {
                    $pdf->cell('50', '7', $pglmn['pglmn_keterangan'], '0', '1', 'L');
                }
            }
            $pdf->Ln(10);
            
            //14
            $pdf->cell('65', '7', '14.	LAPORAN PERISYTIHARAN HARTA', '0', '1', 'L ');
            $pengalaman = mysqli_query($con,$sqlpengalaman);
            while($pglmn = mysqli_fetch_assoc($pengalaman))
            {
                if($pglmn['pglmn_group']=='14')
                {
                    $pdf->cell('50', '7', '        Pengisytiharan harta :', '0', '0', 'L ');
                    $pdf->cell('20', '7', $pglmn['pglmn_peringkat'], '0', '1', 'R');
                    $pdf->cell('50', '7', '        Rujukan :', '0', '0', 'L ');
                    $pdf->cell('20', '7', $pglmn['pglmn_keterangan'], '0', '1', 'R');
                    $pdf->cell('50', '7', '        Tarikh :', '0', '0', 'L ');
                    $pdf->cell('20', '7', $pglmn['pglmn_mula'], '0', '1', 'R');
                }
            }
            $pdf->Ln(10);
            

            //15
            $pdf->cell('18', '7', '16. Saya ', '0', '0', 'R');
            $pdf->cell('50','7', $row['pem_nama_main'], '0', '0', 'L');
            $pdf->cell('50','7', 'mengaku bahawa segala keterangan di atas adalah benar', '0', '1', 'L');
            $pdf->cell('50','7', 'dan bersetuju untuk ditempatkan di mana-mana Pejabat Jemaah Nazir.', '0', '1', 'L');


            $pdf->Ln(10);

        }
        $pdf->Output();
    }