<?php

require_once 'connection.php';
$sql = " select * from fm_tbl_pemohon ";
$data = mysqli_query($con, $sql);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.css">
    <title>Maklumat Peribadi</title>
</head>
<body>
    <div class="row">
        <div class="col">
            <div class="container">
            <div class="card">
                <div class="card-header">
                    <form action="pdf_gen.php" method="POST">
                        <button type="submit" name="btn_pdf" class="btn btn-success">PDF</button>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>ID Pemohon</th>
                            <th>Gred Pemohon</th>
                            <th>Opsyen Pemohon</th>
                            <th>Pilihan 1</th>
                            <th>Pilihan 2</th>
                            <th>Nama Penuh Pemohon</th>
                            <th>Gelaran Pemohon</th>
                        </tr>
                        <?php
                            while($row = mysqli_fetch_assoc($data))
                            {
                            ?>
                            <tr>
                                <td><a href="pdf_gen.php?u=<?php echo $row['pem_nokp_baru']; ?>"><?php echo $row['pemohon_id']; ?></a></td>
                                <td><?php echo $row['pem_gred']; ?></td>
                                <td><?php echo $row['pem_opsyen']; ?></td>
                                <td><?php echo $row['pem_pilihan1']; ?></td>
                                <td><?php echo $row['pem_pilihan2']; ?></td>
                                <td><?php echo $row['pem_nama_main']; ?></td>
                                <td><?php echo $row['pem_nama_option']; ?></td>

                            </tr>
                        <?php

                            }
                        ?>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>
    
</body>
</html>