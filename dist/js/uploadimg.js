
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#callimage')
                .attr('src', e.target.result)
                .width(180);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function(){
var count = 1;
var kira = 6;


$('#akatableFunction').click(function(){
    count = count + 1;
    var Akanama = document.getElementById("akanama").value;
    var Akaperingkat = document.getElementById("akaperingkat").value;
    var Akabidang = document.getElementById("akabidang").value;
    var Akatahun = document.getElementById("akatahun").value;
    document.getElementById("akatablePoint").innerHTML += "<tr id='row"+count+"'><td class='akanama'>" + Akanama + "</td><td class='akaperingkat'>" + Akaperingkat + "</td><td class='akabidang'>" + Akabidang + "</td><td class='akatahun'>" + Akatahun + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("akanama").value = "";
    document.getElementById("akaperingkat").value = "";
    document.getElementById("akabidang").value = "";
    document.getElementById("akatahun").value = "";
});

$('#srtableFunction').click(function(){
    count = count + 1;
    var Srnama = document.getElementById("srnama").value;
    var Srjawatan = document.getElementById("srjawatan").value;
    var SrKategori = document.getElementById("srkategori").value;
    var Srdari = document.getElementById("srdari").value;
    var Srsehingga = document.getElementById("srsehingga").value;
    document.getElementById("srtablePoint").innerHTML += "<tr id='row"+count+"'><td class='srnama'>" + Srnama + "</td><td class='srjawatan'>" + Srjawatan + "</td><td class='srkategori'>" + SrKategori + "</td><td class='srdari'>" + Srdari + "</td><td class='srsehingga'>" + Srsehingga + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("srnama").value = "";
    document.getElementById("srjawatan").value = "";
    document.getElementById("srkategori").value = "";
    document.getElementById("srdari").value = "";
    document.getElementById("srsehingga").value = "";

});

$('#smtableFunction').click(function(){
    count = count + 1;
    var Smnama = document.getElementById("smnama").value;
    var Smjawatan = document.getElementById("smjawatan").value;
    var Smdari = document.getElementById("smdari").value;
    var Smsehingga = document.getElementById("smsehingga").value;
    document.getElementById("smtablePoint").innerHTML += "<tr id='row"+count+"'><td class='smnama'>" + Smnama + "</td><td class='smjawatan'>" + Smjawatan + "</td><td class='smdari'>" + Smdari + "</td><td class='smsehingga'>" + Smsehingga + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >keluarkan</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("smnama").value = "";
    document.getElementById("smjawatan").value = "";
    document.getElementById("smdari").value = "";
    document.getElementById("smsehingga").value = "";
});


$('#bhgtableFunction').click(function(){
    count = count + 1;
    var Bhgnama = document.getElementById("bhgnama").value;
    var Bhgjawatan = document.getElementById("bhgjawatan").value;
    var Bhgdari = document.getElementById("bhgdari").value;
    var Bhgsehingga = document.getElementById("bhgsehingga").value;
    document.getElementById("bhgtablePoint").innerHTML += "<tr id='row"+count+"'><td class='bhgnama'>" + Bhgnama + "</td><td class='bhgjawatan'>" + Bhgjawatan + "</td><td class='bhgdari'>" + Bhgdari + "</td><td class='bhgsehingga'>" + Bhgsehingga + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >keluarkan</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("bhgnama").value = "";
    document.getElementById("bhgjawatan").value = "";
    document.getElementById("bhgdari").value = "";
    document.getElementById("bhgsehingga").value = "";

});


$('#pdstableFunction').click(function(){
    count = count + 1;
    var Pdsmpd = document.getElementById("pdsmpd").value;
    var Pdsdt = document.getElementById("pdsdt").value;
    var Pglmndari = document.getElementById("pglmndari").value;
    var Pglmnsehingga = document.getElementById("pglmnsehingga").value;
    document.getElementById("pdstablePoint").innerHTML += "<tr id='row"+count+"'><td class='pdsmpd'>" + Pdsmpd + "</td><td class='pdsdt'>" + Pdsdt + "</td><td class='pglmndari'>" + Pglmndari + "</td><td class='pglmnsehingga'>" + Pglmnsehingga + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("pdsmpd").value = "";
    document.getElementById("pdsdt").value = "";
    document.getElementById("pglmndari").value = "";
    document.getElementById("pglmnsehingga").value = "";
});


$('#tugaslaintableFunction').click(function(){
    count = count + 1;
    var Tugaslainjawatan = document.getElementById("tugaslainjawatan").value;
    document.getElementById("tugaslaintablePoint").innerHTML += "<tr id='row"+count+"'><td class='tugaslainjawatan'>" + Tugaslainjawatan + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("tugaslainjawatan").value = "";
});


$('#kursustableFunction').click(function(){
    count = count + 1;
    var Kursusnama = document.getElementById("kursusnama").value;
    var Kursustempat = document.getElementById("kursustempat").value;
    var Kssdari = document.getElementById("kssdari").value;
    var Ksssehingga = document.getElementById("ksssehingga").value;
    document.getElementById("kursustablePoint").innerHTML += "<tr id='row"+count+"'><td class='kursusnama'>" + Kursusnama + "</td><td class='kursustempat'>" + Kursustempat + "</td><td class='kssdari'>"+ Kssdari +"</td><td class='ksssehingga'>"+ Ksssehingga +"</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("kursusnama").value = "";
    document.getElementById("kursustempat").value = "";
    document.getElementById("kssdari").value = "";
    document.getElementById("ksssehingga").value = "";
});


$('#kajiantableFunction').click(function(){
    count = count + 1;
    var Kajiannama = document.getElementById("kajiannama").value;
    var Tarikhkajian = document.getElementById("tarikhkajian").value;
    document.getElementById("kajiantablePoint").innerHTML += "<tr id='row"+count+"'><td class='kajiannama'>" + Kajiannama + "</td><td class='tarikhkajian'>" + Tarikhkajian + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("kajiannama").value = "";
    document.getElementById("tarikhkajian").value = "";
});


$('#sumbangantableFunction').click(function(){
    count = count + 1;
    var Sumbanganjenis = document.getElementById("sumbanganjenis").value;
    var Sumbanganperingkat = document.getElementById("sumbanganperingkat").value;
    var Sumtarikh = document.getElementById("sumtarikh").value;
    document.getElementById("sumbangantablePoint").innerHTML += "<tr id='row"+count+"'><td class='sumbanganjenis'>" + Sumbanganjenis + "</td><td class='sumbanganperingkat'>" + Sumbanganperingkat + "</td><td class='sumtarikh'>" + Sumtarikh + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("sumbanganjenis").value = "";
    document.getElementById("sumbanganperingkat").value = "";
    document.getElementById("sumtarikh").value ="";
});


$('#kegiatantableFunction').click(function(){
    count = count + 1;
    var Kegiatannama = document.getElementById("kegiatannama").value;
    var Kegiatanjawatan = document.getElementById("kegiatanjawatan").value;
    document.getElementById("kegiatantablePoint").innerHTML += "<tr id='row"+count+"'><td class='kegiatannama'>" + Kegiatannama + "</td><td class='kegiatanjawatan'>" + Kegiatanjawatan + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("kegiatannama").value = "";
    document.getElementById("kegiatanjawatan").value = "";
});


$('#anugerahtableFunction').click(function(){
    count = count + 1;
    var Anugerahjenis = document.getElementById("anugerahjenis").value;
    var Anugerahtahun = document.getElementById("anugerahtahun").value;
    document.getElementById("anugerahtablePoint").innerHTML += "<tr id='row"+count+"'><td class='anugerahjenis'>" + Anugerahjenis + "</td><td class='anugerahtahun'>" + Anugerahtahun + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";
    
    // The below part is to clear the values after the entry is added.
    document.getElementById("anugerahjenis").value = "";
    document.getElementById("anugerahtahun").value = "";
});

$('#kemahiranliterasiFunction').click(function(){
    count = count + 1;
    count2 = count - 1;
    var Lainnama10 = document.getElementById("lainnama10").value;
    var Lain10 = document.getElementById("lain10").value;
    document.getElementById("kemahiranliterasitablePoint").innerHTML += "<tr id='row"+count+"'><td class='lainnama10'>" + Lainnama10 + "</td><td class='lain10'>" + Lain10 + "</td><td><button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove' >Padam</button></td></tr>";


    // The below part is to clear the values after the entry is added.
    document.getElementById("lainnama10").value = "";
    document.getElementById("lain10").value = "";
});

$(document).on('click', '.remove', function(){
    var delete_row = $(this).data("row");
    $('#' + delete_row).remove();
   });


});

   
