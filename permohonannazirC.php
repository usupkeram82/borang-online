<?php
session_start();

if(isset($_SESSION['namapemohon'])){
    $nokppemohon=$_SESSION['nokppemohon'];
    $emelpemohon=$_SESSION['emelpemohon'];
    include 'utilities/formdata.php';
    $b = new formdata();
    $b->select("fm_tbl_aktifkan","*"," aktif_nokp = '$nokppemohon' AND aktif_email='$emelpemohon' AND aktif_status =2 ");
    $result = $b->sql;

    $numrows = $result->num_rows;
    if($numrows > 0){
        $docu = new formdata();
        $docu->select("fm_tbl_document","*"," doc_nokp = '$nokppemohon' AND doc_status=1");
        $lsdoc = $docu->sql;
        $num=0;
        while($row = mysqli_fetch_assoc($lsdoc)){
            $link[$num]=$row['doc_link'];
            $num++;
        }
    }
}   

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Permohonan Nazir Baru</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

</head>
<body class="layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-lg navbar-dark bg-navy">
    <div class="container">
      <a href="#" class="navbar-brand">
        <img src="dist/img/jatajn.png" alt="Jemaah nazir" class="brand-image img-square elevation-1" style="opacity: .8">
        <span class="brand-text font-weight-light">JEMAAH NAZIR <strong>KEMENTERIAN PENDIDIKAN MALAYSIA</strong></span>
      </a>

    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container text-center">
            <h1>
            <strong>PERMOHONAN UNTUK JAWATAN NAZIR SEKOLAH<br>
            PEGAWAI PERKHIDMATAN PENDIDIKAN</strong></h1>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      
      <div class="container">
        <div class="row text-center">
            <div class="col-md-4">
                <a  href="permohonannazir.php">  
                    <div class="small-box bg-navy">
                        <div class="inner">
                        <br><h3>Bahagian A</h3>
                        </div>
                        <div class="icon">
                        <i class="fa fa-font"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a href="bhgB/permohonannazirB-1.php">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <br><h3>Bahagian B</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bold"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a  href="permohonannazirC.php">
                    <div class="small-box bg-lightblue">
                        <div class="inner">
                            <br><h3>Bahagian C</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-certificate "></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <form action="tamat_permohonan.php" method="post">

                    <div class="card card-navy card-outline" id="step-1">
                        <div class="card-header">
                            <h5 class="card-title m-0"><strong>Bahagian C</strong></h5>
                        </div>
                        
                        <div class="card-body">
                            <h3>Pengesahan Ketua Jabatan</h3>
                            <div class="col-md-12"><hr></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td width="5%" class="text-center">1</td>
                                                <td width="80%">Sepanjang pengetahuan saya, butir-butir yang dinyatakan oleh pemohon seperti di atas adalah benar.</td>
                                                <td width="15%" class="text-center">
                                                    <div class="custom-control custom-checkbox">
                                                        <select name="pengesahan" class="form-control">
                                                            <option value="">--Sila Pilih--</option>
                                                            <option value="Disahkan">Disahkan</option>
                                                            <option value="Tidak Disahkan">Tidak DIsahkan</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" class="text-center">2</td>
                                                <td width="80%">Bersama dengan borang JN/NS/2019 ini disertakan dokumen-dokumen berikut :</td>
                                                <td width="15%" class="text-center">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" class="text-center"></td>
                                                <td width="80%">i. Satu salinan Kad Pengenalan.</td>
                                                <td width="15%" class="text-center">
                                                    <a href="datafiles/<?php echo $link[0]; ?>" class="btn btn-xs bg-success" target="_blank">
                                                        <i class="fas fa-search mr-1"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" class="text-center"></td>
                                                <td width="80%">ii.Satu salinan Buku Perkhidmatan Kerajaan yang lengkap dan kemas kini serta disahkan.</td>
                                                <td width="15%" class="text-center">
                                                    <a href="datafiles/<?php echo $link[1]; ?>" class="btn btn-xs bg-success" target="_blank">
                                                        <i class="fas fa-search mr-1"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" class="text-center"></td>
                                                <td width="80%">iii. Perakuan markah prestasi bagi 3 tahun terkini.</td>
                                                <td width="15%" class="text-center">
                                                    <a href="datafiles/<?php echo $link[2]; ?>" class="btn btn-xs bg-success" target="_blank">
                                                        <i class="fas fa-search mr-1"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="5%" class="text-center"></td>
                                                <td width="80%">vi. Salinan sijil-sijil kelayakan akademik yang disahkan.</td>
                                                <td width="15%" class="text-center">
                                                    <a href="datafiles/<?php echo $link[3]; ?>" class="btn btn-xs bg-success" target="_blank">
                                                        <i class="fas fa-search mr-1"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>                                
                            </div>
                            <div class="col-md-12"><hr></div>                     
                            <div class="row">
                                <div class="col-md-2">
                                <label>3. Permohonan ini</label>
                                </div>        
                                <div class="col-md-2">
                                    <div class="custom-control">
                                        <select name="sokongan" class="form-control">
                                            <option value="">--Sila Pilih--</option>
                                            <option value="Disokong">Disokong</option>
                                            <option value="Tidak Disokong">Tidak Disokong</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"><hr></div>
                            <div class="row">
                                <div class="col-md-12"> 
                                    <label>4. Ulasan Ketua Jabatan :</label>
                                    <div class="col-md-12">
                                        <textarea id="ulasankj" name="ulasankj">
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"><hr></div>    
                        </div> 
                    </div>

                    <div class="card card-navy card-outline" id="step-2">
                        <div class="card-header">
                            <h5 class="card-title m-0"><strong>Bahagian C</strong></h5>
                        </div>
                        
                        <div class="card-body">
                            <h3>PERAKUAN TINDAKAN TATATERTIB / SURCAJ / KETERHUTANGAN KEWANGAN YANG SERIUS</h3>
                            <div class="col-md-12"><hr></div>
        
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Dengan ini saya mengesahkan bahawa <b><?php echo $_SESSION['namapemohon']; ?></b>
                                    no. Kad Pengenalan <b><?php echo $_SESSION['nokppemohon']; ?></b> 
                                        <select name="statusttb" id="statusttb" onchange="showHideTatatertib()">
                                            <option value="">--sila pilih--</option>
                                            <option value="pernah">pernah</option>
                                            <option value="tidak pernah"> tidak pernah</option>
                                        </select> dikenakan tindakan tatatertib / surcaj / mempunyai keterhutangan kewangan yang serius.
                                    </p>                                  
                                </div>
                            </div>
                            <div id="divtatatertib">
                                <div class="row mt-3">
                                    <label>Jika pernah diambil tindakan tatatertib, jenis hukuman yang dikenakan adalah seperti berikut :</label>
                                    <div class="offset-md-1 col-md-9">
                                       (i) 	Amaran
                                    </div>
            
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input custom-control-input-lightblue" type="checkbox" id="customCheckbox11" name="ctindakan_i" value="Ada">
                                        <label for="customCheckbox11" class="custom-control-label"></label>
                                        </div>
                                    </div>
                                </div>        
                                <div class="row">
                                <div class="offset-md-1 col-md-9">
                                    (ii) Denda
                                </div>
            
                                <div class="col-md-1">
                                    <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input custom-control-input-lightblue" type="checkbox" id="customCheckbox12" name="ctindakan_ii" value="Ada">
                                    <label for="customCheckbox12" class="custom-control-label"></label>
                                    </div>
                                </div>
                                </div>
            
                                <div class="row">
                                <div class="offset-md-1 col-md-9">
                                    (iii) Lucut Hak Emolumen
                                </div>
            
                                <div class="col-md-1">
                                    <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input custom-control-input-lightblue" type="checkbox" id="customCheckbox13" name="ctindakan_iii" value="Ada">
                                    <label for="customCheckbox13" class="custom-control-label"></label>
                                    </div>
                                </div>
                                </div>
            
                                <div class="row">
                                <div class="offset-md-1 col-md-9">
                                    (iv) Tangguh Pergerakan Gaji
                                </div>
            
                                <div class="col-md-1">
                                    <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input custom-control-input-lightblue" type="checkbox" id="customCheckbox14" name="ctindakan_iv" value="Ada">
                                    <label for="customCheckbox14" class="custom-control-label"></label>
                                    </div>
                                </div>
                                </div>
            
                                <div class="row">
                                <div class="offset-md-1 col-md-9">
                                    (v) Turun Gaji
                                </div>
            
                                <div class="col-md-1">
                                    <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input custom-control-input-lightblue" type="checkbox" id="customCheckbox15" name="ctindakan_v" value="Ada">
                                    <label for="customCheckbox15" class="custom-control-label"></label>
                                    </div>
                                </div>
                                </div>
            
                                <div class="row">
                                <div class="offset-md-1 col-md-9">
                                    (vi) Turun Pangkat
                                </div>
            
                                <div class="col-md-1">
                                    <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input custom-control-input-lightblue" type="checkbox" id="customCheckbox16" name="ctindakan_vi" value="Ada">
                                    <label for="customCheckbox16" class="custom-control-label"></label>
                                    </div>
                                </div>
                                </div>

                                <div class="row">
                                    <div class="offset-md-1 col-md-9">
                                        (vii) Buang Kerja
                                    </div>
                
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input custom-control-input-lightblue" type="checkbox" id="customCheckbox17" name="ctindakan_vii" value="Ada">
                                        <label for="customCheckbox17" class="custom-control-label"></label>
                                        </div>
                                    </div>
                                </div>
            
            
                                <div class="col-md-12"><hr></div>
                                <div class="row">
                                <div class="col-md-4">
                                    <label>Tahun dijatuhkan hukuman tatatertib:</label>
                                </div>
            
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="tahuntindakan" name="tahuntindakan" placeholder="Sila nyatakan sekiranya ada">
                                </div>
                                </div><br>

                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Tarikh Perakuan: <span class="symbol required"></span></label>
                                    <div class="input-group date col-md-3" id="tarikhperakuantatatertib" data-target-input="nearest">
                                        <input type="date" class="form-control" data-target="#tarikhperakuantatatertib" id="tarikhperakuantatatertib" name="tarikhperakuantatatertib"/>
                                        <div class="input-group-append" data-target="#tarikhperakuantatatertib" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                
                                </div>
                            </div>
        
                        </div>
        
                        <div class="card-footer">
                            <button class="btn btn-info"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                            <button type="submit" value="saveC" name="saveC" class="btn btn-info float-right">Simpan dan Sahkan<i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                        
                    </div>

                </form>
                
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer bg-navy">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Hak Cipta &copy; 2021 <a href="">JEMAAH NAZIR</a>.</strong> Hak Cipta Terpelihara.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- BS-Stepper -->
<script src="plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- dropzonejs -->
<script src="plugins/dropzone/min/dropzone.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- captcha -->
<script src="dist/js/script.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- Page specific script -->
<!-- summernote -->
<script>
  $(function () {
    // $('#summernote').summernote()
    var t = $('#sbbmohon').summernote({
        height: 300,
        focus: false
      });

      var t = $('#ulasankj').summernote({
        height: 300,
        focus: false
      });

      $("#divtatatertib").hide();
  
      // $("#btn").click(function(){
      //   $('div.note-editable').height(150);
      // });
  });

  function showHideTatatertib(){
    var statusttb = document.getElementById("statusttb").value;
    if(statusttb=='pernah'){
      $("#divtatatertib").show();
    }
    else{
      $("#divtatatertib").hide();
    }
  }
</script>

<!-- uploadimage -->
<script src="dist/js/uploadimg.js"></script>
</body>
</html>
