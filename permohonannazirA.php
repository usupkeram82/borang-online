<?php
session_start();
include 'utilities/formdata.php';
include('utilities/insertA.php');

if(isset($_SESSION['nokppemohon'])){
  //Cek data dah wujud belum1
  $nokppemohon = $_SESSION['nokppemohon'];
  $b = new formdata();
  $b->select("fm_tbl_pemohon","*"," pem_nokp_baru = '$nokppemohon'");
  $result = $b->sql;
  if($result->num_rows>0){
    $row = mysqli_fetch_assoc($result);

    $gredpenyandang = $row['pem_gred'];
    $opsyen_major = $row['pem_opsyen_major'];
    $opsyen_minor = $row['pem_opsyen_minor'];
    $penempatan1 = $row['pem_pilihan1'];
    $penempatan2 = $row['pem_pilihan2'];

    $gelaran = $row['pem_gelaran'];

    // $namapenuh = $row['namapenuh'];
    // $namalain = $row['pem_nama_option'];
    // $nokpbaru = $row['nokpbaru'];
    // $nokplama = $row['pem_nokp_lama'];
    $profile_pic= $row['pem_gambar'];

    // $tarikhlahirdate = $_POST['tarikhlahirdate'];
    $tempatlahir = $row['pem_tpt_lahir'];
    $alamatpej = $row['pem_pjbt_alamat'];
    $poskodpej = $row['pem_pjbt_poskod'];
    $notelpej = $row['pem_pjbt_phone'];
    $negeripej = $row['pem_pjbt_negeri'];
    $alamatrum = $row['pem_rumah_alamat'];
    $poskodrum = $row['pem_rumah_poskod'];
    $negerirum = $row['pem_rumah_negeri'];
    $notelbim = $row['pem_rumah_phone'];

    // $email = $_POST['email'];
    $tarafkahwin = $row['pem_kahwin_status'];
    $namasuis = $row['pem_kahwin_psg_nama'];
    $pekerjaansuis = $row['pem_kahwin_psg_kerja'];
    $alamatsuis = $row['pem_kahwin_psg_pejabat'];

    $namagb = $row['pem_nama_pengesah'];;
    $jawatangb = $row['pem_jawatan_pengesah'];;
    $emelgb = $row['pem_emel_pengesah'];;
  }
  else{
    $gredpenyandang = '';
    $opsyen_major = '';
    $opsyen_minor = '';
    $penempatan1 = '';
    $penempatan2 = '';

    // $namapenuh = $row['namapenuh'];
    $namalain = '';
    // $nokpbaru = $row['nokpbaru'];
    $nokplama = '';

    $profile_pic= 'no_picture.png';
    // $tarikhlahirdate = $_POST['tarikhlahirdate'];
    $tempatlahir = '';
    $alamatpej = '';
    $poskodpej = '';

    $notelpej = '';
    $negeripej='';
    $alamatrum = '';
    $poskodrum = '';
    $negerirum = '';
    $notelbim = '';

    // $email = $_POST['email'];
    $tarafkahwin = '';
    $namasuis = '';
    $pekerjaansuis = '';
    $alamatsuis = '';

    // $email = $_POST['email'];
    $namagb = '';
    $jawatangb = '';
    $emelgb = '';
  }
}
else{
  $mesej = 'Terdapat kesilapan teknikal,\nSila masukkan masukkan maklumat permohonan!!';
  echo '<script>alert("'.$mesej.'");';
  echo 'window.location.href="index.php";</script>';
  exit;
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Permohonan Nazir Baru</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">

  <!-- Upload IMage CSS -->
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- captcha -->
  <link rel="stylesheet" href="dist/css/captcha.css">

</head>
<body class="layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-lg navbar-dark bg-navy">
    <div class="container">
      <a href="#" class="navbar-brand">
        <img src="dist/img/jatajn.png" alt="Jemaah nazir" class="brand-image img-square elevation-1" style="opacity: .8">
        <span class="brand-text font-weight-light">JEMAAH NAZIR <strong>KEMENTERIAN PENDIDIKAN MALAYSIA</strong></span>
      </a>

    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container text-center">
            <h1>
            <strong>PERMOHONAN UNTUK JAWATAN NAZIR SEKOLAH<br>
            PEGAWAI PERKHIDMATAN PENDIDIKAN</strong></h1>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      
      <div class="container">
        <form id="quickForm" action="" method="post" enctype="multipart/form-data">
        <div class="row text-center">
          <div class="col-md-4">
            <a  href="permohonannazirA.php">  
              <div class="small-box bg-navy">
                <div class="inner">
                  <br><h3>Bahagian A</h3>
                </div>
                <div class="icon">
                  <i class="fa fa-font"></i>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-4">
            <a  href="permohonannazirB.php">
              <div class="small-box bg-primary">
                <div class="inner">
                  <br><h3>Bahagian B</h3>
                </div>
                <div class="icon">
                  <i class="fa fa-bold"></i>
                </div>
              </div>
            </a>
          </div>

          <div class="col-md-4">
            <a  href="permohonannazirC.php">
              <div class="small-box bg-lightblue">
                <div class="inner">
                  <br><h3>Bahagian C</h3>
                </div>
                <div class="icon">
                  <i class="fa fa-certificate "></i>
                </div>
              </div>
            </a>
          </div>          
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-navy card-outline">
              <div class="card-header">
                <h5 class="card-title text-center">PERMOHONAN PERKHIDMATAN PENDIDIKAN</h5>
              </div>

              <div class="card-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-12">
                        <!-- select -->
                        <div class="form-group required">
                          <label>Gred Penyandang</label>
                          <select class="form-control" id="gredpenyandang" name="gredpenyandang">
                            <option value="" <?php if($gredpenyandang=='') echo 'selected'; ?>>--Sila Pilih--</option>
                            <?php
                              $gp = new formdata();
                              $gp->select("fm_tbl_jawatan","*","");
                              $rows = $gp->sql;
                              while($row = mysqli_fetch_assoc($rows)){
                                if($gredpenyandang==$row['jwt_keterangan']){
                                  echo '<option value="'.$row['jwt_keterangan'].'" selected >'.$row['jwt_keterangan'].'</option>';
                                }
                                else{
                                  echo '<option value="'.$row['jwt_keterangan'].'">'.$row['jwt_keterangan'].'</option>';
                                }
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <hr>
                        <h5 class="card-title m-0">Opsyen Matapelajaran</h5><br><br>
                      </div>
                      <div class="col-md-6">
                        <!-- select -->
                        <div class="form-group required">
                          <label>Major<span class="symbol required"></span></label>
                          <input type="text" class="form-control" id="opsyen_major" name="opsyen_major" value="<?php echo $opsyen_major; ?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <!-- select -->
                        <div class="form-group required">
                          <label>Minor<span class="symbol required"></span></label>
                          <input type="text" class="form-control" id="opsyen_minor" name="opsyen_minor" value="<?php echo $opsyen_minor; ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <hr>
                        <h5 class="card-title m-0">Pilihan Penempatan</h5><br><br>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group required">
                          <label>Pilihan 1 <span class="symbol required"></span></label><br>
                          <?php
                            $ngr = new formdata();
                            $ngr->select("fm_tbl_negeri","*"," negeri_status='Aktif'  ORDER BY negeri ASC  ");
                            $result = $ngr->sql;

                          ?>
                          <select class="form-control" id="penempatan1" name="penempatan1">
                            <option value="">--Sila Pilih Negeri--</option>
                          <?php
                            while ($row = mysqli_fetch_assoc($result)){
                              $selected='';
                              if($row['negeri']== $penempatan1){
                                $selected='selected';
                              }
                              echo '<option value="'.$row['negeri'].'"'.$selected.' >'.$row['negeri'].'</option>';
                            }
                          ?>                            
                            
                          </select>
                        </div>
                      </div>
                      <!-- UPDATED EXCLUDE LABUAN -->
                      <div class="col-lg-6">
                        <div class="form-group required">
                          <label>Pilihan 2 <span class="symbol required"></span></label><br>
                           <?php
                            $ngr2 = new formdata();
                            $ngr2->select("fm_tbl_negeri " ,"*"," negeri_status='Aktif'  ORDER BY negeri ASC  ");
                            $result2 = $ngr2->sql;

                          ?>
                          <select class="form-control" id="penempatan2" name="penempatan2">
                            <option value="">--Sila Pilih Negeri--</option>
                            <?php
                            while ($row2 = mysqli_fetch_assoc($result2)){
                              $selected='';
                              if($row2['negeri']== $penempatan2){
                                $selected='selected';
                              }

                              echo '<option value="'.$row2['negeri'].'" '.$selected.' >'.$row2['negeri'].'</option>';
                            }
                          ?>    
                          </select>
                        </div>
                      </div>
                    </div>                 
                  </div> 
                  <div class="col-md-4 text-center">
                    <div class="row">
                      <div class="col-md-12">
                        <!-- Uploaded image area-->
                        <div class="image-area"><img id="imageResult" src="<?php echo $profile_pic? 'datafiles/profile/'.$profile_pic: 'datafiles/profile/no_picture.png'  ?>" alt="" width="120px" height="150px" class="img-fluid rounded shadow-sm mx-auto d-block"></div>
                      </div>
                      <div class="col-lg-10 mx-auto">
                        <!-- Upload image input-->
                        <div class="input-group mb-1 px-2 py-2 rounded-pill bg-white shadow-sm">
                            <input id="upload" type="file" onchange="readURL(this);" class="form-control border-0" name="profile_picture">
                            <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file</label>
                            <div class="input-group-append">
                                <label for="upload" class="btn btn-light m-0 rounded-pill px-8"> <i class="fa fa-cloud-upload mr-2 text-muted"></i>
                                <small class="text-uppercase font-weight-bold text-muted">Pilih Gambar</small></label>
                            </div>                            
                        </div>
                        <p class="font-italic text-center small">Sila pastikan imej berukuran passpot dan bersaiz kurang daripada 2MB</p>                    
                      </div>

                    </div>
                      
                  </div>
                </div>
              </div> 
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-navy card-outline">
              <div class="card-header">
                <h5 class="card-title m-0"><strong>Bahagian A (Butir-Butir Peribadi)</strong></h5>
              </div>
              <div class="card-body">                
                <div class="row">                      
                  <div class="col-lg-8">
                    <div class="form-group">
                      <label for="namapenuh" class="form-label">Nama Penuh <span class="symbol required"></span></label>
                      <input type="text" class="form-control" id="namapenuh" name="namapenuh" placeholder="nama penuh (Huruf Besar)" value="<?php echo $_SESSION['namapemohon']; ?>" style="text-transform:uppercase" readonly>
                    </div>                    
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-label">No. K/P<span class="symbol required"></span></label>
                      <input type="text" class="form-control" id="nokpbaru" name="nokpbaru" value="<?php echo $_SESSION['nokppemohon']; ?>" readonly>                  
                    </div>
                  </div>
                </div>
                <div class="row">
                <div class="col-lg-3">
                    <div class="form-group required">
                      <label>Gelaran <span class="symbol required"></span></label><br>
                      <?php
                        $gelar = new formdata();
                        $gelar->select("fm_tbl_gelaran","*", " status='Aktif' ORDER BY gelaran ASC ");
                        $rslt = $gelar->sql;

                      ?>
                      <select class="form-control" id="gelaran" name="gelaran">
                        <option value="">--Sila Pilih Gelaran--</option>
                        <?php
                        while ($row = mysqli_fetch_assoc($rslt)){
                          $selected='';
                          if($row['gelaran']== $gelaran){
                            $selected='selected';
                          }
                          echo '<option value="'.$row['gelaran'].'"'.$selected.' >('.$row['singkatan'].') '.$row['gelaran'].'</option>';
                        }
                        ?>    
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label class="form-label">Jantina <span class="symbol required"></span></label>
                    
                        <div class="form-check">
                          <input class="form-check-input" type="radio" id="jantina" name="jantina" value="Lelaki">
                          <label class="form-check-label">Lelaki</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" id="jantina" name="jantina" value="Perempuan">
                          <label class="form-check-label">Perempuan</label>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label class="form-label">Tarikh Lahir</label>
                      <div class="input-group date" id="tarikhlahir" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#tarikhlahir" placeholder="Tarikh Lahir" id="tarikhlahirdate" name="tarikhlahirdate" onchange="getAge();" />
                        <div class="input-group-append" data-target="#tarikhlahir" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                      </div>
                      <!-- <div class="col-md-1"></div> -->
                      <label for="umurtahun" class="form-label">Umur pada 1 Januari <?php echo date('Y'); ?> : </label>
                      <p id="umurtahundanbulan">-</p>              
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <label class="form-label">Negeri Kelahiran <span class="symbol required"></span></label>
                        <?php
                          $ngr3 = new formdata();
                          $ngr3->select("fm_tbl_negeri");
                          $result3 = $ngr3->sql;

                        ?>
                        <select class="form-control" id="tempatlahir" name="tempatlahir">
                          <option value="">--Sila Pilih--</option>
                          <?php
                            while ($row = mysqli_fetch_assoc($result3)){
                              if($row['negeri']== $tempatlahir){
                                echo '<option value="'.$row['negeri'].'" selected >'.$row['negeri'].'</option>';
                              }
                              echo '<option value="'.$row['negeri'].'" >'.$row['negeri'].'</option>';
                            }
                          ?>
                        </select>
                    </div>
                  </div>                  
                </div>
                <div class="row">
                  
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label class="form-label">Alamat Pejabat <span class="symbol required"></span></label>
                        <input type="text" class="form-control" name="alamatpej" id="alamatpej" value="<?php echo $alamatpej; ?>" placeholder="Alamat">
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <input type="text" class="form-control" id="poskodpej" name="poskodpej" value="<?php echo $poskodpej; ?>" placeholder="Poskod">
                      
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                        <?php
                          $ngr4 = new formdata();
                          $ngr4->select("fm_tbl_negeri");
                          $result4 = $ngr4->sql;

                        ?>
                        <select class="form-control" id="negeripej" name="negeripej">
                          <option value="">--Sila Pilih Negeri--</option>
                          <?php
                            while ($row = mysqli_fetch_assoc($result4)){
                              $selected='';
                              if($row['negeri']== $negeripej){
                                $selected='selected';
                              }
                              echo '<option value="'.$row['negeri'].'"'.$selected.' >'.$row['negeri'].'</option>';
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <input type="integer" class="form-control" id="notelpej" name="notelpej" value="<?php echo $notelpej; ?>" placeholder="No Telefon Pejabat">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">     
                    <div class="form-group">
                      <label for="alamatrumah" class="col-md-2 col-form-label">Alamat Rumah <span class="symbol required"></span></label>
                        <input type="text" class="form-control" id="alamatrum" name="alamatrum" value="<?php echo $alamatrum; ?>" placeholder="Alamat">
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                        <input type="text" class="form-control" id="poskodrum" name="poskodrum" value="<?php echo $poskodrum; ?>" placeholder="Poskod">                        
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                        <?php
                          $ngr5 = new formdata();
                          $ngr5->select("fm_tbl_negeri");
                          $result5 = $ngr5->sql;

                        ?>
                        <select class="form-control" id="negerirum" name="negerirum">
                          <option value="">--Sila Pilih Negeri--</option>
                          <?php
                            while ($row = mysqli_fetch_assoc($result5)){
                              $selected='';
                              if($row['negeri']== $negerirum){
                                $selected='selected';
                              }
                              echo '<option value="'.$row['negeri'].'"'.$selected.' >'.$row['negeri'].'</option>';
                            }
                          ?>
                        </select>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <input type="integer" class="form-control" id="notelbim" name="notelbim" value="<?php echo $notelbim; ?>" placeholder="No. Telefon Rumah">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">  
                    <div class="form-group">
                      <label class="form-label">E-Mel</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $_SESSION['emelpemohon']; ?>" readonly>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12">  
                    <div class="form-group">
                      <label for="TarafPerkahwinan" class="form-label">Taraf Perkahwinan <span class="symbol required"></span></label>
                        <select class="form-control" id="tarafkahwin" name="tarafkahwin" onchange="showHidePasangan()">
                          <option value="">--Sila Pilih--</option>
                          <option value="Berkahwin" <?php if($tarafkahwin=='Berkahwin') echo 'selected'; ?>>Berkahwin</option>
                          <option value="Bujang" <?php if($tarafkahwin=='Bujang') echo 'selected'; ?>>Bujang</option>
                          <option value="Janda" <?php if($tarafkahwin=='Janda') echo 'selected'; ?>>Janda</option>
                          <option value="Duda" <?php if($tarafkahwin=='Duda') echo 'selected'; ?>>Duda</option>
                        </select>
                    </div>
                  </div>
                </div>

                <div class="row" id="pasanganDiv">
                  <div class="col-lg-12">  
                    <div class="form-group">
                      <label class="form-label">Maklumat Pasangan (Jika berkahwin):</label>
                    </div>
                  </div>
                  <div class="col-lg-12">  
                    <div class="form-group">
                        <input type="text" class="form-control" id="namasuis" name="namasuis" value="<?php echo $namasuis; ?>" style="text-transform:uppercase" placeholder="Nama Pasangan">
                    </div>
                  </div>        
                  <div class="col-lg-12">  
                    <div class="form-group">                  
                        <input type="text" class="form-control" id="pekerjaansuis" name="pekerjaansuis" value="<?php echo $pekerjaansuis; ?>" placeholder="Pekerjaan Pasangan">
                    </div>
                  </div>
                  <div class="col-lg-12">  
                    <div class="form-group">
                        <input type="text" class="form-control" id="alamatsuis" name="alamatsuis" value="<?php echo $alamatsuis; ?>" placeholder="Alamat Tempat Bertugas">
                    </div>
                  </div>
                </div>                
              </div>
            </div> 

            <div class="card card-navy card-outline">
              <div class="card-header">
                <h5 class="card-title m-0"><strong>Maklumat Ketua Jabatan</strong></h5>
              </div>

              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="namapenuh" class="form-label">Nama Penuh<span class="symbol required"></span></label>
                      <input type="text" class="form-control" id="namagb" name="namagb" placeholder="Nama Ketua Jabatan" value="<?php echo $namagb; ?>">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-label">Jawatan<span class="symbol required"></span></label>
                      <input type="text" class="form-control" id="jawatangb" name="jawatangb" value="<?php echo $jawatangb; ?>">                  
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">  
                    <div class="form-group">
                      <label class="form-label">E-Mel</span></label>
                        <input type="email" class="form-control" id="emelgb" name="emelgb" value="<?php echo $emelgb; ?>">
                    </div>
                  </div>
                </div>              
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-4">
                    <button class="btn btn-block btn-warning"><i class="fa fa-arrow-circle-left"></i> Kembali</button>
                  </div>
                  <div class="col-md-4"></div>
                  <div class="col-md-4">
                     <button id="next-form1" name="submit" type="submit" class="btn btn-block  btn-info">Seterusnya <i class="fa fa-arrow-circle-right"></i></button>
                  </div>
                </div>              
              </div>
              <!-- /.card-footer -->
            </div>  

          </div>
              
        </div>
        </form>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer bg-navy">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Hak Cipta &copy; 2021 <a href="#">JEMAAH NAZIR</a>.</strong> Hak Cipta Terpelihara.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- jquery-validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<script>
$(function () {
  $('#quickForm').validate({
    rules: {
      gredpenyandang: {
        required: true
      },
      opsyenikhtisas: {
        required: true
      },
      penempatan1: {
        required: true
      },
      penempatan2: {
        required: true
      },
      namapenuh: {
        required: true
      },
      nokpbaru: {
        required: true
      },
      jantina: {
        required: true
      },
      alamatpej: {
        required: true
      },
      notelpej: {
        required: true
      },
      alamatrum: {
        required: true
      },
      namagb: {
        required: true
      },
      jawatangb: {
        required: true
      },
      emelgb: {
        required: true
      },
    },
    messages: {
      gredpenyandang: {
        required: "Sila pilih Gred"
      },
      opsyenikhtisas: {
        required: "Sila pilih Opsyen Ikhtisas"
      },
      penempatan1: {
        required: "Sila pilih cadangan Penempatan 1"
      },
      penempatan2: {
        required: "Sila pilih cadangan Penempatan 2"
      },
      namapenuh: {
        required: "Sila masukkan Nama"
      },
      nokpbaru: {
        required: "Sila masukkan No Kad Pengenalan"
      },
      jantina: {
        required: "Sila pilih jantina"
      },
      alamatpej: {
        required: "Sila masukkan Alamat Pejabat"
      },
      notelpej: {
        required: "Sila masukkan No Telefon Pejabat"
      },
      alamatrum: {
        required: "Sila masukkan Alamat Rumah"
      },
      namagb: {
        required: "Sila masukkan Nama Ketua Jabatan"
      },
      jawatangb: {
        required: "Sila masukkan Jawatan Ketua Jabatan"
      },
      emelgb: {
        required: "Sila masukkan Emel Ketua Jabatan"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
<script>
  $(function () {
    //Date picker
    $('#tarikhlahir').datetimepicker({
        format: 'L'
    });

    $("#pasanganDiv").hide();
    
    //Set Default value daripada value NoKP
    var nokpbaru = document.getElementById('nokpbaru');
    var tarikhlahirdate = document.getElementById('tarikhlahirdate');
    var jtn = document.getElementById('jantina');
    var tahun = nokpbaru.value.substring(0, 2);
    tahun = getFourDigitYear(parseInt(tahun));
    var bulan = nokpbaru.value.substring(2, 4);
    var hari = nokpbaru.value.substring(4, 6);
    var akhir = nokpbaru.value.substring(11, 12);
    var neg_lahir = nokpbaru.value.substring(6, 8);
    var settlahir = bulan+"/"+hari+"/"+tahun;
    tarikhlahirdate.value=settlahir;
    if(parseInt(akhir) % 2==0)
      $("input[name=jantina][value=Perempuan]").attr('checked', true);
    else
      $("input[name=jantina][value=Lelaki]").attr('checked', true);
    getAge();
    negeriLahir(neg_lahir);
    // showHidePasangan();

    function getFourDigitYear(tahun){
      if(tahun < 21 && tahun >= 0){
        tahun = tahun.toString();
        if(tahun.length == 2)
          return "20" + tahun;
        else
          return "200" + tahun;
      }
      else
        return "19" + tahun;
    }

    function getAge(){
      var caldate = document.getElementById('tarikhlahirdate');
      var dateString = caldate.value;
      var umurpada = document.getElementById('umurtahundanbulan');
      var date = new Date();
      var today = new Date(date.getFullYear(), 0, 1);
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
      {
          age--;
      }
      var umurbulan = 12 - birthDate.getMonth();
      if(umurbulan==12){
        age++;
        umurpada.innerHTML = age + " tahun";
      } else{
        umurpada.innerHTML = age + " tahun " + umurbulan + ' bulan';
      }
      
      // alert(dateString.value);
    }

    function negeriLahir(neg_lahir){
      let negeriHtml='';
      let select = $('#negerirum');
      if(neg_lahir=='01'){
        negeriHtml='Johor';
      }
      if(neg_lahir=='02'){
        negeriHtml='Kedah';
      }
      if(neg_lahir=='03'){
        negeriHtml='Kelantan';
      }
      if(neg_lahir=='04'){
        negeriHtml='Melaka';
      }
      if(neg_lahir=='05'){
        negeriHtml='Negeri Sembilan';
      }
      if(neg_lahir=='06'){
        negeriHtml='Pahang';
      }
      if(neg_lahir=='07'){
        negeriHtml='Pulau Pinang';
      }
      if(neg_lahir=='08'){
        negeriHtml='Perak';
      }
      if(neg_lahir=='09'){
        negeriHtml='Perlis';
      }
      if(neg_lahir=='10'){
        negeriHtml='Selangor';
      }
      if(neg_lahir=='11'){
        negeriHtml='Terengganu';
      }
      if(neg_lahir=='12'){
        negeriHtml='Sabah';
      }
      if(neg_lahir=='13'){
        negeriHtml='Sarawak';
      }
      if(neg_lahir=='01'){
        negeriHtml='Kuala Lumpur';
      }
      if(neg_lahir=='14'){
        negeriHtml='Labuan';
      }
      if(neg_lahir=='16'){
        negeriHtml='Putrajaya';
      }
      // select.value = negeriHtml;
      document.getElementById('tempatlahir').value=negeriHtml;

    }

  });
  function showHidePasangan(){
    var statusKahwin = document.getElementById("tarafkahwin").value;
    if(statusKahwin=='Berkahwin'){
      $("#pasanganDiv").show();
    }
    else{
      $("#pasanganDiv").hide();
    }
  }


  // Upload image JS
  /*  ==========================================
      SHOW UPLOADED IMAGE
  * ========================================== */
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#imageResult')
                  .attr('src', e.target.result);
          };
          reader.readAsDataURL(input.files[0]);
      }
  }

  $(function () {
      $('#upload').on('change', function () {
          readURL(input);
      });
  });

  /*  ==========================================
      SHOW UPLOADED IMAGE NAME
  * ========================================== */
  var input = document.getElementById( 'upload' );
  var infoArea = document.getElementById( 'upload-label' );

  input.addEventListener( 'change', showFileName );
  function showFileName( event ) {
    var input = event.srcElement;
    var fileName = input.files[0].name;
    infoArea.textContent = fileName;
  }

</script>
</body>
</html>