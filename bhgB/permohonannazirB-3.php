<?php
session_start();
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Permohonan Nazir Baru</title>

  <!-- insertiontable -->
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../plugins/summernote/summernote-bs4.min.css">

</head>
<body class="layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-lg navbar-dark bg-navy">
    <div class="container">
      <a href="#" class="navbar-brand">
        <img src="../dist/img/jatajn.png" alt="Jemaah nazir" class="brand-image img-square elevation-1" style="opacity: .8">
        <span class="brand-text font-weight-light">JEMAAH NAZIR <strong>KEMENTERIAN PENDIDIKAN MALAYSIA</strong></span>
      </a>

    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container text-center">
            <h1>
            <strong>PERMOHONAN UNTUK JAWATAN NAZIR SEKOLAH<br>
            PEGAWAI PERKHIDMATAN PENDIDIKAN</strong></h1>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      
      <div class="container">
        <div class="row text-center">
            <div class="col-md-4">
                <a  href="../permohonannazir.php">  
                    <div class="small-box bg-navy">
                        <div class="inner">
                        <br><h3>Bahagian A</h3>
                        </div>
                        <div class="icon">
                        <i class="fa fa-font"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a href="permohonannazirB-1.php">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <br><h3>Bahagian B</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bold"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a  href="../permohonannazirC.php">
                    <div class="small-box bg-lightblue">
                        <div class="inner">
                            <br><h3>Bahagian C</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-certificate "></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">

          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-6">
                <h2>Bahagian B</h2>
              </div>
            </div>
          </div>

          <div class="col-lg-12">

              <form action="../insert.php" class="form-horizontal" method="post">
                  <div class="card card-navy card-outline">
                      <div class="card-header">
                          <h5 class="card-title m-0"><strong>4. PENGALAMAN PERKHIDMATAN</strong></h5>
                      </div>
                      
                      <div class="card-body">
                          <label class="col-form-label">4.1 Bilangan tahun berkhidmat</label>
                          
                          <div class="form-group row">
                          <label class="col-md-3 col-form-label">Sekolah Rendah <span class="symbol required"></span></label>
                          <div class="col-md-1">
                              <input type="number" class="form-control tahun" id="pglmnsrtahun" name="pglmnsrtahun" placeholder="0" onblur="getAmount()">
                          </div>
                          <label class="col-md-1 col-form-label">Tahun</label>
                          <div class="col-md-1">
                              <input type="number" class="form-control bulan" id="pglmnsrbulan" name="pglmnsrbulan" placeholder="0" onblur="getAmount()">
                          </div>
                          <label class="col-md-1 col-form-label">Bulan</label>
                          </div>

                          <div class="form-group row">
                          <label class="col-md-3 col-form-label">Sekolah Menengah <span class="symbol required"></span></label>
                          
                          <div class="col-md-1">
                              <input type="number" class="form-control tahun" id="pglmnsmtahun" name="pglmnsmtahun" placeholder="0" onblur="getAmount()">
                          </div>
                          <label class="col-md-1 col-form-label">Tahun</label>
                          <div class="col-md-1">
                              <input type="number" class="form-control bulan" id="pglmnsmbulan" name="pglmnsmbulan" placeholder="0" onblur="getAmount()">
                          </div>
                          <label class="col-md-1 col-form-label">Bulan</label>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 col-form-label">Bahagian/ JPN/ PPD <span class="symbol required"></span></label>
                            
                            <div class="col-md-1">
                                <input type="number" class="form-control tahun" id="pglmnbhgntahun" name="pglmnbhgntahun" placeholder="0" onblur="getAmount()">
                            </div>
                            <label class="col-md-1 col-form-label">Tahun</label>
                            <div class="col-md-1">
                                <input type="number" class="form-control bulan" id="pglmnbhgnbulan" name="pglmnbhgnbulan" placeholder="0" onblur="getAmount()">
                            </div>
                            <label class="col-md-1 col-form-label">Bulan</label>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-1 col-form-label">Lain-Lain<span class="symbol required"></span></label>
                            <input type="text" class="col-md-2 form-control " id="pengalamanlain" name="pengalamanlain" placeholder="-sila nyataka-">
                            <div class="col-md-1">
                                <input type="number" class="form-control tahun" id="pglmnlaintahun" name="pglmnlaintahun" placeholder="0" onblur="getAmount()">
                            </div>
                            <label class="col-md-1 col-form-label">Tahun</label>
                            <div class="col-md-1">
                                <input type="number" class="form-control bulan" id="pglmnlainbulan" name="pglmnlainbulan" placeholder="0" onblur="getAmount()">
                            </div>
                            <label class="col-md-1 col-form-label">Bulan</label>
                          </div>

                          <div class="form-group row">
                            <label class="col-md-3 col-form-label">Jumlah Tahun Keseluruhan <span class="symbol required"></span></label>
                            <div class="col-md-1">
                              <input type="text" class="form-control" name="totaltahun" id="totaltahun" readonly>
                            </div>
                            <label class="col-md-1 col-form-label">Tahun</label>
                            <div class="col-md-1">
                              <input type="text" class="form-control" name="totalbulan" id="totalbulan" readonly>
                            </div>
                            <label class="col-md-1 col-form-label">Bulan</label>
                          </div>
                      </div>

                      <div class="card-footer">
                          <button type="button" onclick="location.href = 'permohonannazirB-4.php';" class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                          <button type="submit" value="save4point1" name="save4point1" class="btn btn-info float-right next-step">Simpan, Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                      </div>
                  </div>
              </form>
              
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer bg-navy">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Hak Cipta &copy; 2021 <a href="#">JEMAAH NAZIR</a>.</strong> Hak Cipta Terpelihara.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="../plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="../plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="../plugins/moment/moment.min.js"></script>
<script src="../plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="../plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="../plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- BS-Stepper -->
<script src="../plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- dropzonejs -->
<script src="../plugins/dropzone/min/dropzone.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- captcha -->
<script src="../dist/js/script.js"></script>
<!-- Summernote -->
<script src="../plugins/summernote/summernote-bs4.min.js"></script>
<!-- inserttable -->
<script src="../dist/js/inserttable.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Page specific script -->

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date picker
    $('#tarikhlahir').datetimepicker({
        format: 'L'
    });

    $('#tarikhlantikanpertama').datetimepicker({
        format: 'L'
    });

    $('#tarikhlantikankeskimperkhidmatansekarang').datetimepicker({
        format: 'L'
    });

    $('#tarikhdisahkandalamperkhidmatan').datetimepicker({
        format: 'L'
    });

    $('#srdari').datetimepicker({
        format: 'L'
    });

    $('#srsehingga').datetimepicker({
        format: 'L'
    });

    $('#smdari').datetimepicker({
      format:'L'
    });

    $('#smsehingga').datetimepicker({
      format:'L'
    });

    $('#bhgdari').datetimepicker({
      format:'L'
    });

    $('#bhgsehingga').datetimepicker({
      format:'L'
    });

    $('#pglmndari').datetimepicker({
      format:'L'
    });

    $('#pglmnsehingga').datetimepicker({
      format:'L'
    });

    $('#kssdari').datetimepicker({
      format:'L'
    });

    $('#ksssehingga').datetimepicker({
      format:'L'
    });

    $('#tarikhkajian').datetimepicker({
      format:'L'
    });

    $('#sumtarikh').datetimepicker({
      format:'L'
    });

    $('#ei_extra2').datetimepicker({
      format:'L'
    });

    $('#ei_extra2').datetimepicker({
      format:'L'
    });

    $('#isytiharharta').datetimepicker({
      format:'L'
    });

    $('#pengesahantarikh').datetimepicker({
      format:'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

  })
  // BS-Stepper Init
  document.addEventListener('DOMContentLoaded', function () {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
  })

  // DropzoneJS Demo Code Start
  Dropzone.autoDiscover = false

  // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
  var previewNode = document.querySelector("#template")
  previewNode.id = ""
  var previewTemplate = previewNode.parentNode.innerHTML
  previewNode.parentNode.removeChild(previewNode)

  var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: "/target-url", // Set the url
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
  })

  myDropzone.on("addedfile", function(file) {
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
  })

  // Update the total progress bar
  myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
  })

  myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1"
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
  })

  // Hide the total progress bar when nothing's uploading anymore
  myDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0"
  })

  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.
  document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
  }
  document.querySelector("#actions .cancel").onclick = function() {
    myDropzone.removeAllFiles(true)
  }
  // DropzoneJS Demo Code End
</script>
<!-- summernote -->
<script>
  $(function () {
    // $('#summernote').summernote()
    var t = $('#sbbmohon').summernote({
        height: 300,
        focus: false
      });

      var t = $('#ulasankj').summernote({
        height: 300,
        focus: false
      });
  
      // $("#btn").click(function(){
      //   $('div.note-editable').height(150);
      // });
  
    $.validator.setDefaults({
      submitHandler: function () {
        alert( "Form successful submitted!" );
      }
    });
    $('#quickForm').validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 5,
          maxlength:10
        },
        terms: {
          required: true
        },
      },
      messages: {
        email: {
          required: "Please enter a email address",
          email: "Please enter a valid email address"
        },
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
          maxlength:"Terlalu panjang"
        },
        terms: "Please accept our terms"
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>

<script>
  function getAmount(){
    var pglmnsrtahun =$('#pglmnsrtahun').val();
    var pglmnsrbulan =$('#pglmnsrbulan').val();

    var pglmnsmtahun =$('#pglmnsmtahun').val();
    var pglmnsmbulan =$('#pglmnsmbulan').val();

    var pglmnbhgntahun =$('#pglmnbhgntahun').val();
    var pglmnbhgnbulan =$('#pglmnbhgnbulan').val();

    var pglmnlaintahun =$('#pglmnlaintahun').val();
    var pglmnlainbulan =$('#pglmnlainbulan').val();

    var totaltahun=0;
    $('.tahun').each(function(){
      totaltahun+= +$(this).val();
      $('#totaltahun').val(totaltahun);
    })

    var totalbulan=0;
    $('.bulan').each(function(){
      totalbulan += +$(this).val();
      $('#totalbulan').val(totalbulan);
    })
  }
</script>

<!-- uploadimage -->
<script src="dist/js/uploadimg.js"></script>
</body>
</html>
