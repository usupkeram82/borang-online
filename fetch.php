<?php
//fetch.php
//include('utilities/dbcon.php');
$conn = mysqli_connect("10.22.44.53", "JNlib", "JNlib1234", "enazir");
$nokp = $_SESSION['nokppemohon'];
$output = '';

if(isset($_POST["akanama"]))
{
    $query = "SELECT * FROM fm_tbl_akademik where aka_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
        <tr>
            <th><center>Nama Maktab/ Institusi/ Universiti</center></th>
            <th><center>Peringkat</center></th>
            <th><center>Bidang</center></th>
            <th><center>Tahun Penganugerahan</center></th>					
        </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["aka_institusi"].'</td>
    <td>'.$row["aka_peringkat"].'</td>
    <td>'.$row["aka_bidang"].'</td>
    <td>'.$row["aka_tahun"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

//$query="SELECT * FROM fm_tbl_pengalaman";

if(isset($_POST["srnama"]))
{
    //$query .="WHERE pglmn_group='4.1.1'";
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='4.1.1' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
        <tr>
            <th style="width: 25%;">
                <center>Nama Sekolah</center>
            </th>
            <th style="width: 26%;">
                <center>Jawatan</center>
            </th>
            <th style="width: 17%;">
                <center>Dari</center>
            </th>
            <th style="width: 17%;">
                <center>Sehingga</center>
            </th>
        </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    <td>'.$row["pglmn_mula"].'</td>
    <td>'.$row["pglmn_tamat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["smnama"]))
{
    //$query .="WHERE pglmn_group='4.1.2'";
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='4.1.2' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th style="width: 25%;">
            <center>Nama Sekolah</center>
        </th>
        <th style="width: 26%;">
            <center>Jawatan</center>
        </th>
        <th style="width: 17%;">
            <center>Dari</center>
        </th>
        <th style="width: 17%;">
            <center>Sehingga</center>
        </th>
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    <td>'.$row["pglmn_mula"].'</td>
    <td>'.$row["pglmn_tamat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["bhgnama"]))
{
    //$query .="WHERE pglmn_group='4.1.3'";
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='4.1.3' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th style="width: 25%;"><center>Nama Sekolah</center></th>
        <th style="width: 26%;"><center>Jawatan</center></th>
        <th style="width: 17%;"><center>Dari</center></th>
        <th style="width: 17%;"><center>Sehingga</center></th> 							
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    <td>'.$row["pglmn_mula"].'</td>
    <td>'.$row["pglmn_tamat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["pdsmpd"]))
{
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='4.2' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th style="width: 25%;"><center>Mata Pelajaran Diajar</center></th>
        <th style="width: 26%;"><center>Darjah/Tingkatan</center></th>
        <th style="width: 17%;"><center>Dari</center></th>
        <th style="width: 17%;"><center>Sehingga</center></th>							
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    <td>'.$row["pglmn_mula"].'</td>
    <td>'.$row["pglmn_tamat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["tugaslainnama"]))
{
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='4.3' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th><center>Tugas/Tanggungjawab</center></th>
        <th><center>Jawatan Dipegang</center></th>						
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["kursusnama"]))
{
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='5' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th><center>Nama Kursus</center></th>
        <th><center>Tempat</center></th>
        <th><center>Dari</center></th>
        <th><center>Sehingga</center></th> 						
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    <td>'.$row["pglmn_mula"].'</td>
    <td>'.$row["pglmn_tamat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["kajiannama"]))
{
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='6' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th><center>Nama Kajian / Penerbitan</center></th>
        <th><center>Tarikh</center></th>						
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["sumbanganjenis"]))
{
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='7' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th><center>Jenis Sumbangan </center></th>
        <th><center>Peringkat (Jabatan / Kementerian / Negeri / Kebangsaan / Antarabangsa)</center></th>
        <th><center>Tarikh</center></th>						
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    <td>'.$row["pglmn_mula"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["kegiatannama"]))
{
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='8' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th><center>Nama Badan / Kegiatan</center></th>
        <th><center>Jawatan</center></th>						
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}

if(isset($_POST["anugerahjenis"]))
{
    $query = "SELECT * FROM fm_tbl_pengalaman where pglmn_group='9' and pglmn_pemohon_nokp=$nokp";
    $result = mysqli_query($conn, $query);
    $output = '
    <br />
    <table class="table table-bordered table-hover">
    <thead class="label label-sm bg-navy">
    <tr>
        <th><center>Jenis Pengiktirafan/Anugerah</center></th>
        <th><center>Tahun</center></th>							
    </tr>
    </thead>
    ';
    while($row = mysqli_fetch_array($result))
    {
    $output .= '
    <tr>
    <td>'.$row["pglmn_keterangan"].'</td>
    <td>'.$row["pglmn_peringkat"].'</td>
    </tr>
    ';
    }
    $output .= '</table>';
    echo $output;
}
?>
