<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Permohonan Nazir Baru</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  


  <!-- captcha -->
  <script src='https://www.google.com/recaptcha/api.js' async defer></script>



</head>
<body class="layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-lg navbar-dark bg-navy">
    <div class="container">
      <a href="#" class="navbar-brand">
        <img src="dist/img/jatajn.png" alt="Jemaah nazir" class="brand-image img-square elevation-1" style="opacity: .8">
        <span class="brand-text font-weight-light">JEMAAH NAZIR <strong>KEMENTERIAN PENDIDIKAN MALAYSIA</strong></span>
      </a>

    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container text-center">
            <h1>
            <strong>PERMOHONAN UNTUK JAWATAN NAZIR SEKOLAH<br>
            PEGAWAI PERKHIDMATAN PENDIDIKAN</strong></h1>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <form id="semakData" method="POST" action="sahkan_permohonan.php">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title text-center">PERMOHONAN PERKHIDMATAN PENDIDIKAN</h5>
                </div>

                <div class="card-body">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="form-group">
                        <label>NO KAD PENGENALAN BARU <span class="symbol required"></span></label>
                        <input type="text" class="form-control" name="nokppemohon" placeholder="No Kad Pengenalan Baru">
                      </div>
                      <div class="form-group">
                        <label>TOKEN <span class="symbol required"></span></label>
                        <input type="text" class="form-control" name="tokenpemohon" placeholder="Token : Rujuk di dalam emel">
                      </div>                           
                    </div>
                    <div class="col-md-4" style="padding-top: 2em;">
                      <div class="g-recaptcha" data-sitekey="6LdvirUdAAAAAHUH-LWJUZd4jp6-ZgnX02Gyf6Hk"></div>
                    </div>  
                  </div>
                </div> 
                <div class="card-footer"><!-- 
                  <button type="button" class="btn btn-warning toastrDefaultError">
                  Launch Warning Toast
                </button> -->
                  <!-- <button class="btn btn-danger"><i class="fa fa-times"></i> Batal</button> -->
                  <button id="submitButton" name="semak" type="submit" class="btn btn-info float-right">Hantar <i class="fa fa-arrow-circle-right"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer bg-navy">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Hak Cipta &copy; 2021 <a href="#">JEMAAH NAZIR</a>.</strong> Hak Cipta Terpelihara.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>

<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- captcha -->
<script src="dist/js/script.js"></script>

<script>
$(function () {
  // $.validator.setDefaults({
  //   submitHandler: function () {
  //     // if(document.getElementsByName("g-recaptcha-response").length<=0) {
  //     //   toastr.error('Sila sahkan CAPTCHA');
  //     // }
  //   }
  // });
  
  // $('.toastrDefaultError').click(function() {
  //     toastr.error('Sila sahkan CAPTCHA')
  // });

  $('#semakData').validate({
    rules: {
      namapemohon: {
        required: true
      },
      nokppemohon: {
        required: true,
        minlength: 12,
        maxlength:12
      },
      emelpemohon: {
        required: true,
        email: true
      },
    },
    messages: {
      emelpemohon: {
        required: "Sila masukkan E-Mel",
        email: "Sila masukkan emel yang betul"
      },
      nokppemohon: {
        required: "Sila masukkan No Kad Pengenalan",
        minlength: "Sila masukkan 12 digit No Kad Pengenalan",
        maxlength:"Sila masukkan 12 digit No Kad Pengenalan"
      },
      namapemohon: "Sila masukkan nama"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
</body>
</html>
