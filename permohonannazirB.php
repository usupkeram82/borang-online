<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Permohonan Nazir Baru</title>

  <!-- insertiontable -->
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

  <!-- captcha -->
  <link rel="stylesheet" href="dist/css/captcha.css">

  <!-- formwizard -->
    <link rel="stylesheet" href="plugins2/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css2/main.css">
    <link rel="stylesheet" href="css2/main-responsive.css">
    <!-- <link rel="stylesheet" href="../../plugins2/bootstrap-colorpalette/css/bootstrap-colorpalette.css"> -->
    <!-- <link rel="stylesheet" href="../../plugins2/perfect-scrollbar/src/perfect-scrollbar.css"> -->
    <!-- <link rel="stylesheet" href="../../css2/theme_light.css" type="text/css" id="skin_color"> -->
    <!-- <link rel="stylesheet" href="../../css2/print.css" type="text/css" media="print"/> -->
  <!-- ./formwizard -->

</head>
<body class="layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-lg navbar-dark bg-navy">
    <div class="container">
      <a href="#" class="navbar-brand">
        <img src="dist/img/jatajn.png" alt="Jemaah nazir" class="brand-image img-square elevation-1" style="opacity: .8">
        <span class="brand-text font-weight-light">JEMAAH NAZIR <strong>KEMENTERIAN PENDIDIKAN MALAYSIA</strong></span>
      </a>

    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper ">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container text-center">
            <h1>
            <strong>PERMOHONAN UNTUK JAWATAN NAZIR SEKOLAH<br>
            PEGAWAI PERKHIDMATAN PENDIDIKAN</strong></h1>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      
      <div class="container">
        <div class="row text-center">
            <div class="col-md-4">
                <a  href="permohonannazir.php">  
                    <div class="small-box bg-navy">
                        <div class="inner">
                        <br><h3>Bahagian A</h3>
                        </div>
                        <div class="icon">
                        <i class="fa fa-font"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a href="permohonannazirB.php">
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <br><h3>Bahagian B</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bold"></i>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-md-4">
                <a  href="permohonannazirC.php">
                    <div class="small-box bg-lightblue">
                        <div class="inner">
                            <br><h3>Bahagian C</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-certificate "></i>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <form action="#" role="form" class="smart-wizard form-horizontal" id="form">
                    <div id="wizard" class="swMain">

                        <div class="card card-navy card-outline" id="step-1">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                                <div class="card-body">
                                <h3>2. Butir-Butir Perkhidmatan</h3><br>
                                
                                <div class="form-group row">
                                    <label class="col-md-5 col-form-label">2.1  Tarikh lantikan pertama: <span class="symbol required"></span></label>
                                    <div class="input-group date col-md-2" id="tarikhlantikanpertama" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#tarikhlantikanpertama" placeholder="--Sila nyatakan" id="tarikhlantikanpertama" name="tarikhlantikanpertama"/>
                                        <div class="input-group-append" data-target="#tarikhlantikanpertama" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                
                                </div>
                
                                <div class="form-group row">
                                    <label class="col-md-5 col-form-label">2.2  Tarikh lantikan ke skim perkhidmatan sekarang: <span class="symbol required"></span></label>
                                    <div class="input-group date col-md-2" id="tarikhlantikankeskimperkhidmatansekarang" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#tarikhlantikankeskimperkhidmatansekarang" placeholder="--Sila nyatakan--" id="tarikhlantikankeskimperkhidmatansekarang" name="tarikhlantikankeskimperkhidmatansekarang" />
                                        <div class="input-group-append" data-target="#tarikhlantikankeskimperkhidmatansekarang" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                
                                <div class="form-group row">
                                    <label class="col-md-5 col-form-label">2.3  Tarikh disahkan dalam perkhidmatan: <span class="symbol required"></span></label>
                                    <div class="input-group date col-md-2" id="tarikhdisahkandalamperkhidmatan" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#tarikhdisahkandalamperkhidmatan" placeholder="--Sila nyatakan--" id="tarikhdisahkandalamperkhidmatan" name="tarikhdisahkandalamperkhidmatan" />
                                        <div class="input-group-append" data-target="#tarikhdisahkandalamperkhidmatan" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">2.4  Gred hakiki penyandang: <small>PPP GRED</small> <span class="symbol required"></span></label>
                                    <select class="form-control offset-md-1 col-md-2" id="gredhakiki" name="gredhakiki">
                                    <option>DG 44</option>
                                    <option>DG 41</option>
                                    <option>DG 32</option>
                                    <option>DG 29</option>
                                    <option>DG 38</option>
                                    </select>
                                </div>
                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">2.5  Jawatan Sekarang: <span class="symbol required"></span></label>
                                    <input type="text" class="offset-md-2 form-control col-md-2" placeholder="--Jawatan Sekarang--" id="jwtnskrg" name="jwtnskrg">
                                </div>
                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label">2.6  Taraf Jawatan: <span class="symbol required"></span></label>
                                    <select class="form-control offset-md-2 col-md-2" id="tarafjwtn" name="tarafjwtn">
                                    <option>Tetap</option>
                                    <option>Kontrak</option>
                                    </select>
                                </div>
                
                                </div>
                                <div class="card-footer">
                                    <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                    <button class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                                </div>
                        </div>

                        <div class="card card-navy card-outline" id="step-2">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>3. KELAYAKAN AKADEMIK CALON</h3><br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-form-label">Nama Maktab/ Institusi/ Universiti</label>
                                            <input type="text" class="form-control akanama" id="akanama" name="akanama" placeholder="--sila nyatakan--">
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                        <label class="col-form-label">Peringkat</label>
                                        <select class="form-control akaperingkat" id="akaperingkat" name="akaperingkat">
                                            <option>--sila pilih--</option>
                                            <option>Diploma</option>
                                            <option>Ijazah Sarjana Muda</option>
                                            <option>Ijazah Sarjana</option>
                                            <option>Kedoktoran</option>
                                        </select>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label class="col-form-label">Bidang</label>
                                        <select class="form-control akabidang" id="akabidang" name="akabidang">
                                            <option>--sila pilih--</option>
                                            <option>Perguruan</option>
                                            <option>Kejuruteraan</option>
                                            <option>Sains Komputer</option>
                                            <option>Undang-undang</option>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                        <label class="col-form-label">Tahun Penganugerahan</label>
                                        <input type="text" class="col-md-8 form-control akatahun" id="akatahun" name="akatahun" placeholder="--sila nyatakan--">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label><br></label>
                                            <div class="input-group date" id="srtambah" data-target-input="nearest">
                                                <button type="button" onClick="akatableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>      

                                <!-- table -->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="akatable" name="akatable">
                                        <thead class="label label-sm bg-navy">
                                        <tr>
                                            <th><center>Nama Maktab/ Institusi/ Universiti</center></th>
                                            <th><center>Peringkat</center></th>
                                            <th><center>Bidang</center></th>
                                            <th><center>Tahun Penganugerahan</center></th>      							
                                        </tr>
                                        </thead>
                                        <tbody id="akatablePoint">
                                        
                                        </tbody>
                                    </table>
                                </div>
                                <!-- ./table -->
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" name="saveakatable" id="saveakatable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>

                        <div class="card card-navy card-outline" id="step-3">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>4. PENGALAMAN PERKHIDMATAN</h3><br>
                                <label class="col-form-label">4.1	Bilangan tahun berkhidmat</label>
                                
                                <div class="form-group row">
                                <label class="col-md-3 col-form-label">Sekolah Rendah <span class="symbol required"></span></label>
                                <label class="col-md-1 col-form-label">Tahun:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="pglmnsrtahun" name="pglmnsrtahun" placeholder="--sila nyatakan--">
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-md-3 col-form-label">Sekolah Menengah <span class="symbol required"></span></label>
                                <label class="col-md-1 col-form-label">Tahun:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="pglmnsmtahun" name="pglmnsmtahun" placeholder="--sila nyatakan--">
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-md-3 col-form-label">Bahagian/ JPN/ PPD <span class="symbol required"></span></label>
                                <label class="col-md-1 col-form-label">Tahun:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="pglmnbhgntahun" name="pglmnbhgntahun" placeholder="--sila nyatakan--">
                                </div>
                                </div>

                                <div class="form-group row">
                                <label class="col-md-3 col-form-label">Jumlah Tahun Keselururahan <span class="symbol required"></span></label>
                                <div class="offset-md-1 col-md-4">
                                    <input type="text" class="form-control" id="pglmnjumlahtahun" name="pglmnjumlahtahun" placeholder="--sila nyatakan--">
                                </div>
                                <label class="col-md-1 col-form-label">Tahun</label>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>

                        <div class="card card-navy card-outline" id="step-4">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>4.1.1 Sekolah Rendah</h3><br>
                                <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Sekolah</label>
                                    <input type="text" class="form-control" id="srnama" name="srnama" placeholder="Nama Sekolah">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Jawatan</label>
                                    <input type="text" class="form-control" id="srjawatan" name="srjawatan" placeholder="Jawatan">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Dari</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#srdari" id="srdari" name="srdari">
                                        <div class="input-group-append" data-target="#srdari" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Sehingga</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#srsehingga" id="srsehingga" name="srsehingga">
                                        <div class="input-group-append" data-target="#srsehingga" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="srtambah" name="srtambah" data-target-input="nearest">
                                        <button type="button" onClick="srtableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="srtable">
                                        <thead class="label label-sm bg-navy">
                                        <tr>
                                            <th style="width: 25%;"><center>Nama Sekolah</center></th>
                                            <th style="width: 26%;"><center>Jawatan</center></th>
                                            <th style="width: 17%;"><center>Dari</center></th>
                                            <th style="width: 17%;"><center>Sehingga</center></th>      							
                                        </tr>
                                        </thead>
                                        <tbody id="srtablePoint">

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" name="savesrtable" id="savesrtable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                        <div class="card card-navy card-outline" id="step-5">

                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>4.1.2 Sekolah Menengah</h3><br>
                                <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Sekolah</label>
                                    <input type="text" class="form-control" id="smnama" name="smnama" placeholder="Nama Sekolah">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Jawatan</label>
                                    <input type="text" class="form-control" id="smjawatan" name="smjawatan" placeholder="Jawatan">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Dari</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#smdari" id="smdari" name="smdari" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#smdari" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Sehingga</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#smsehingga" id="smsehingga" name="smsehingga" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#smsehingga" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="smtambah" data-target-input="nearest">
                                        <button type="button" onClick="smtableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="table-responsive">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="smtable">
                                    <thead class="label label-sm bg-navy">
                                        <tr>
                                        <th style="width: 25%;"><center>Nama Sekolah</center></th>
                                        <th style="width: 26%;"><center>Jawatan</center></th>
                                        <th style="width: 17%;"><center>Dari</center></th>
                                        <th style="width: 17%;"><center>Sehingga</center></th>      							
                                        </tr>
                                    </thead>
                                    <tbody id="smtablePoint">
                                        
                                    </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" name="savesmtable" id="savesmtable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                        <div class="card card-navy card-outline" id="step-6">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>4.1.3 Bahagian / JPN / PPD</h3><br>
                                <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Bahagian</label>
                                    <input type="text" class="form-control" id="bhgnama" name="bhgnama" placeholder="Nama Sekolah">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Jawatan</label>
                                    <input type="text" class="form-control" id="bhgjawatan" name="bhgjawatan" placeholder="Jawatan">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Dari</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#bhgdari" id="bhgdari" name="bhgdari" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#bhgdari" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Sehingga</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#bhgsehingga" id="bhgsehingga" name="bhgsehingga" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#bhgsehingga" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="bhgtambah" data-target-input="nearest">
                                        <button type="button" onClick="bhgtableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="bhgtable">
                                    <thead class="label label-sm bg-navy">
                                        <tr>
                                            <th style="width: 25%;"><center>Nama Sekolah</center></th>
                                            <th style="width: 26%;"><center>Jawatan</center></th>
                                            <th style="width: 17%;"><center>Dari</center></th>
                                            <th style="width: 17%;"><center>Sehingga</center></th>      							
                                        </tr>
                                    </thead>
                                    <tbody id="bhgtablePoint">
                                    
                                    </tbody>
                                </table>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" name="savebhgtable" id="savebhgtable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                        <div class="card card-navy card-outline" id="step-7">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>4.2 Pengalaman Di Sekolah</h3><br>
                                <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Mata Pelajaran Diajar</label>
                                    <input type="text" class="form-control" id="pdsmpd" name="pdsmpd" placeholder="Mata Pelajaran Diajar">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Darjah/Tingkatan</label>
                                    <input type="text" class="form-control" id="pdsdt" name="pdsdt" placeholder="Darjah/Tingkatan">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Dari</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#pglmndari" id="pglmndari" name="pglmndari" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#pglmndari" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Sehingga</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#pglmnsehingga" id="pglmnsehingga" name="pglmnsehingga" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#pglmnsehingga" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="pdstambah" data-target-input="nearest">
                                        <button type="button" onClick="pdstableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="pdstable">
                                    <thead class="label label-sm bg-navy">
                                    <tr>
                                        <th style="width: 25%;"><center>Mata Pelajaran Diajar</center></th>
                                        <th style="width: 26%;"><center>Darjah/Tingkatan</center></th>
                                        <th style="width: 17%;"><center>Dari</center></th>
                                        <th style="width: 17%;"><center>Sehingga</center></th>      							
                                    </tr>
                                    </thead>
                                    <tbody id="pdstablePoint">
                                    
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="savepdstable" name="savepdstable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>

                        <div class="card card-navy card-outline" id="step-8">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>4.3	Tugas Dan Tanggungjawab Lain Semasa Di Sekolah (3 Tahun Terakhir)</h3><br>
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Tugas/Tanggungjawab</label>
                                    <input type="text" class="form-control tugaslainnama" id="tugaslainnama" name="tugaslainnama" placeholder="Tugas/Tanggungjawab">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Jawatan Dipegang</label>
                                    <input type="text" class="form-control tugaslainjawatan" id="tugaslainjawatan" name="tugaslainjawatan" placeholder="Jawatan Dipegang">
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="offset-md-2 col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="tugaslaintambah" data-target-input="nearest">
                                        <button type="button" onClick="tugaslaintableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                
                                </div>

                                <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="tugaslaintable">
                                    <thead class="label label-sm bg-navy">
                                    <tr>
                                        <th><center>Tugas/Tanggungjawab</center></th>
                                        <th><center>Jawatan Dipegang</center></th>      							
                                    </tr>
                                    </thead>
                                    <tbody id="tugaslaintablePoint">
                                    
                                    </tbody>
                                </table>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="savetugaslaintable" name="savetugaslaintable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                        <div class="card card-navy card-outline" id="step-9">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <form class="form-horizontal"></form>
                                <h3>5. Kursus-Kursus Yang Dihadiri</h3><br>
                                <div class="row">
                                    <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Kursus</label>
                                        <input type="text" class="form-control" id="kursusnama" name="kursusnama" placeholder="Nama Kursus">
                                    </div>
                                    <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->  
                                    <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tempat</label>
                                        <input type="text" class="form-control" id="kursusjawatan" name="kursusjawatan" placeholder="Tempat">
                                    </div>
                                    <!-- /.form-group -->
                                    </div>

                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Dari</label>
                                        <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#kssdari" id="kssdari" name="kssdari" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#kssdari" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Sehingga</label>
                                        <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#ksssehinnga" id="ksssehingga" name="kssehingga" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#ksssehinnga" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label><br></label>
                                        <div class="input-group date" id="kursustambah" data-target-input="nearest">
                                        <button type="button" onClick="kursustableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="kursustable">
                                    <thead class="label label-sm bg-navy">
                                        <tr>
                                        <th><center>Nama Kursus</center></th>
                                        <th><center>Tempat</center></th>
                                        <th><center>Dari</center></th>
                                        <th><center>Sehingga</center></th>      							
                                        </tr>
                                    </thead>
                                    <tbody id="kursustablePoint">
                                        
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="savekursustable" name="savekursustable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>

                        <div class="card card-navy card-outline" id="step-10">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <form class="form-horizontal"></form>
                                <h3>6.	KAJIAN / PENERBITAN IKHTISAS YANG PERNAH DIHASILKAN</h3><br>
                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Kajian / Penerbitan</label>
                                        <input type="text" class="form-control" id="kajiannama" name="kajiannama" placeholder="Nama Kajian / Penerbitan">
                                    </div>
                                    <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->  
                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Tarikh</label>
                                        <div class="input-group date" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" data-target="#tarikhkajian" id="tarikhkajian" name="tarikhkajian" placeholder="--Sila pilih--"/>
                                            <div class="input-group-append" data-target="#tarikhkajian" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    </div>

                                    <div class="offset-md-2 col-md-2">
                                    <div class="form-group">
                                        <label><br></label>
                                        <div class="input-group date" id="kajiantambah" data-target-input="nearest">
                                        <button type="button" onClick="kajiantableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                        </div>
                                    </div>
                                    </div>
                    
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="kajiantable">
                                    <thead class="label label-sm bg-navy">
                                        <tr>
                                        <th><center>Nama Kajian / Penerbitan</center></th>
                                        <th><center>Tarikh</center></th>     							
                                        </tr>
                                    </thead>
                                    <tbody id="kajiantablePoint">
                                        
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="savekajiantable" name="savekajiantable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>

                        <div class="card card-navy card-outline" id="step-11">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>7.	SUMBANGAN PROFESIONAL DI LUAR TUGAS RASMI</h3><br>
                                <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Jenis Sumbangan</label>
                                    <input type="text" class="form-control" id="sumbanganjenis" name="sumbanganjenis" placeholder="Jenis Sumbangan">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Peringkat</label>
                                    <select class="form-control" id="sumbanganperingkat" name="sumbanganperingkat">
                                        <option>Jabatan</option>
                                        <option>Kementerian</option>
                                        <option>Negeri</option>
                                        <option>Kebangsaan</option>
                                        <option>Antarabangsa</option>
                                    </select>
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label>Tarikh</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#sumtarikh" id="sumtarikh" name="sumtarikh" placeholder="--Sila pilih--"/>
                                        <div class="input-group-append" data-target="#sumtarikh" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <div class="offset-md-2 col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="sumbangantambah" data-target-input="nearest">
                                        <button type="button" onClick="sumbangantableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                
                                </div>

                                <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="sumbangantable">
                                    <thead class="label label-sm bg-navy">
                                    <tr>
                                        <th><center>Jenis Sumbangan </center></th>
                                        <th><center>Peringkat (Jabatan / Kementerian / Negeri / Kebangsaan / Antarabangsa)</center></th>
                                        <th><center>Tarikh</center></th>     							
                                    </tr>
                                    </thead>
                                    <tbody id="sumbangantablePoint">
                                    
                                    </tbody>
                                </table>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="savesumbangantable" name="savesumbangantable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                        <div class="card card-navy card-outline" id="step-12">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>8.	KEGIATAN LUAR</h3><br>
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Badan / Kegiatan</label>
                                    <input type="text" class="form-control" id="kegiatannama" name="kegiatannama" placeholder="Nama Badan / Kegiatan">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Jawatan</label>
                                    <input type="text" class="form-control" id="kegiatanjawatan" name="kegiatanjawatan" placeholder="Jawatan">
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="offset-md-2 col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="kegiatantambah" name="kegiatantambah" data-target-input="nearest">
                                        <button type="button" onClick="kegiatantableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="kegiatantable">
                                    <thead class="label label-sm bg-navy">
                                    <tr>
                                        <th><center>Nama Badan / Kegiatan</center></th>
                                        <th><center>Jawatan</center></th>							
                                    </tr>
                                    </thead>
                                    <tbody id="kegiatantablePoint">
                                    
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            
                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="savekegiatantable" name="savekegiatantable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                        <div class="card card-navy card-outline" id="step-13">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>9. PENGIKTIRAFAN / ANUGERAH YANG DITERIMA</h3><br>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Jenis Pengiktirafan/Anugerah</label>
                                    <input type="text" class="form-control" id="anugerahjenis" name="anugerahjenis" placeholder="Jenis Pengiktirafan/Anugerah">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->  
                                <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Tahun</label>
                                    <input type="text" class="form-control" id="anugerahtahun" name="anugerahtahun" placeholder="Tahun">
                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="offset-md-2 col-md-2">
                                    <div class="form-group">
                                    <label><br></label>
                                    <div class="input-group date" id="anugerahtambah" name="anugerahtambah" data-target-input="nearest">
                                        <button type="button" onClick="anugerahtableFunction()" value="Add entry" class="btn btn-info"><i class="fa fa-plus"></i>Tambah</button>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="anugerahtable">
                                    <thead class="label label-sm bg-navy">
                                    <tr>
                                        <th><center>Bil</center></th>
                                        <th><center>Jenis Pengiktirafan/Anugerah</center></th>
                                        <th><center>Tahun</center></th>							
                                    </tr>
                                    </thead>
                                    <tbody id="anugerahtablePoint">
                                    
                                    </tbody>
                                </table>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="saveanugerahtable" name="saveanugerahtable" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                        <!-- kiev -->
                        <div class="card card-navy card-outline" id="step-14">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>10. KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI</h3><br>
                                <div class="col-md-12">
                                <div class="table-responsive">
                                    <br>
                                    <table class="table table-bordered table-hover" id="sample-table-1">
                                    <thead class="label label-sm bg-navy">
                                        <tr>
                                        <th rowspan="2" style="width: 5%;"><center>Bil</center></th>
                                        <th rowspan="2" style="width: 35%;"><center>Jenis Aplikasi</center></th>
                                        <th colspan="3" style="width: 60%;"><center>Peringkat Kemahiran</center></th>										
                                        </tr>
                                        <tr>
                                        <th><center>Rendah</center></th>
                                        <th><center>Sederhana</center></th>
                                        <th><center>Tinggi</center></th>										
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td style="text-align: center;">1</td>
                                        <td>MS Word</td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td style="text-align: center;">2</td>
                                        <td>MS Excel</td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td style="text-align: center;">3</td>
                                        <td>MS PowerPoint</td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td style="text-align: center;">4</td>
                                        <td>MS Access </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                            </div>
                                            </center>
                                        </td>
                                        </tr>
                                        <tr>
                                        <center>
                                            <td style="text-align: center;">5</td>
                                            <td>Database</td>
                                            <td>
                                            <center>
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                                </div>
                                            </center>
                                            </td>
                                            <td>
                                            <center>
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                                </div>
                                            </center>
                                            </td>
                                            <td>
                                            <center>
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                                </div>
                                            </center>
                                            </td>
                                        </center>
                                        </tr>
                                        <tr>
                                        <center>
                                            <td style="text-align: center;">2</td>
                                            <td>SPSS</td>
                                            <td>
                                            <center>
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                                </div>
                                            </center>
                                            </td>
                                            <td>
                                            <center>
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                                </div>
                                            </center>
                                            </td>
                                            <td>
                                            <center>
                                                <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"></label>
                                                </div>
                                            </center>
                                            </td>
                                        </center>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                
                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button type="button" id="" name="" class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>
                        <!-- kiev -->

                        <div class="card card-navy card-outline" id="step-15">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>11. MAKLUMAT-MAKLUMAT LAIN</h3>
                                <div class="col-md-12"><hr></div>
                                <div class="row">
                                <div class="col-md-9">
                                    <label>11.1. Pernahkah anda memohon jawatan Nazir Sekolah? <span class="symbol required"></span></label>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                        <input type="radio" id="11point11" name="11point1" checked>
                                        <label for="11point11">YA
                                        </label>
                                        </div>
                                        <div class="icheck-danger d-inline">
                                        <input type="radio" id="11point12" name="11point1">
                                        <label for="11point12">TIDAK
                                        </label>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <p>Jika YA, sila nyatakan tahun</p>
                                    <div class="form-group">
                                        <label for="11point1tahun">Tahun:</label>
                                        <input type="text" name="11point1tahun" class="form-control" id="11point1tahun" placeholder="Tahun">
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-12"><hr></div>
                                </div>

                                <div class="row">
                                <div class="col-md-9">
                                    <label >11.2. Pernahkah anda menghadiri temu duga Nazir Sekolah? <span class="symbol required"></span></label>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                        <input type="radio" id="11point21" name="11point2" checked>
                                        <label for="11point21">YA
                                        </label>
                                        </div>
                                        <div class="icheck-danger d-inline">
                                        <input type="radio" id="11point22" name="11point2">
                                        <label for="11point22">TIDAK
                                        </label>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <p>Jika YA, sila nyatakan tahun</p>
                                    <div class="form-group">
                                        <label for="11point2tahun">Tahun:</label>
                                        <input type="text" name="11point2tahun" class="form-control" id="11point2tahun" placeholder="Tahun">
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-12"><hr></div>
                                </div>

                                <div class="row">
                                <div class="col-md-9">
                                    <label>11.3. Adakah anda memohon bertukar ke tempat atau jawatan lain di Bahagian-Bahagian KPM dalam tahun ini? <span class="symbol required"></span></label>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                        <input type="radio" id="11point31" name="11point3" checked>
                                        <label for="11point31">YA
                                        </label>
                                        </div>
                                        <div class="icheck-danger d-inline">
                                        <input type="radio" id="11point32" name="11point3">
                                        <label for="11point32">TIDAK
                                        </label>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <p>Jika YA, sila nyatakan nama Bahagian</p>
                                    <div class="form-group">
                                        <label for="11point3bhg">Bahagian:</label>
                                        <input type="text" name="11point3bhg" class="form-control" id="11point3bhg" placeholder="Bahagian">
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-12"><hr></div>
                                </div>

                                <div class="row">
                                <div class="col-md-9">
                                    <label>11.4. Adakah anda sedang memohon mengikuti kursus jangka pendek / panjang / NPQEL? <span class="symbol required"></span></label>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                        <input type="radio" id="11point41" name="11point4" checked>
                                        <label for="11point41">YA
                                        </label>
                                        </div>
                                        <div class="icheck-danger d-inline">
                                        <input type="radio" id="11point42" name="11point4">
                                        <label for="11point42">TIDAK
                                        </label>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <p>Jika YA, nyatakan nama Kursus dan tarikh tamat kursus</p>
                                    <div class="form-group">
                                        <label for="11point4nama">Nama:</label>
                                        <input type="text" name="password" class="form-control" id="11point4nama" placeholder="Nama">
                                    </div>
                                    <div class="form-group">
                                        <label>Tarikh tamat:</label>
                                        <div class="col-md-10 input-group date"  data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#tarikhnpqel" id="tarikhnpqel" name="tarikhnpqel" placeholder="--Sila nyatakan--"/>
                                        <div class="input-group-append" data-target="#tarikhnpqel" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-12"><hr></div>
                                </div>

                                <div class="row">
                                <div class="col-md-9">
                                    <label for="">11.5. Adakah anda sedang mengikuti program Pendidikan Jarak Jauh (PJJ) / Separuh Masa? <span class="symbol required"></span></label>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                        <input type="radio" id="11point51" name="11point5" checked>
                                        <label for="11point51">YA
                                        </label>
                                        </div>
                                        <div class="icheck-danger d-inline">
                                        <input type="radio" id="11point52" name="11point5">
                                        <label for="11point52">TIDAK
                                        </label>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <p>Jika YA, nyatakan tarikh jangka tamat</p>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tarikh tamat:</label>
                                        <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="col-md-8 form-control datetimepicker-input" data-target="#tamatpjj" id="tamatpjj" name="tamatpjj" placeholder="--Sila nyatakan--"/>
                                        <div class="input-group-append" data-target="#tamatpjj" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-12"><hr></div>
                                </div>

                                <div class="row">
                                <div class="col-md-12">
                                    <label>11.6. Maklumat-maklumat lain, jika ada.</label>
                                    <div class="form-group">
                                    <br><input type="text" name="maklumatlain" class="form-control" id="maklumatlain" placeholder="--Sila Nyatakan--">
                                    </div>
                                </div>
                                </div>
                                
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>

                        </div>

                        <div class="card card-navy card-outline" id="step-16">
                            <div class="card-header">
                                <h5 class="card-title m-0"><strong>Bahagian B</strong></h5>
                            </div>
                            
                            <div class="card-body">
                                <h3>12. PUSAT TEMU DUGA YANG DIPILIH</h3>
                                <div class="col-md-12"><hr></div>
                                <div class="row">                    
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond1" name="s1">
                                    <label for="radioSecond1">JN Ibu Pejabat
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond2" name="s1">
                                    <label for="radioSecond2">JN Negeri Perlis
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond3" name="s1">
                                    <label for="radioSecond3">JN Negeri Kedah
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond4" name="s1">
                                    <label for="radioSecond4">JN Negeri Pulau Pinang
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond5" name="s1">
                                    <label for="radioSecond5">JN Negeri Perak
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond6" name="s1">
                                    <label for="radioSecond6">JN Negeri Selangor
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond7" name="s1">
                                    <label for="radioSecond7">JN Negeri Perak
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond8" name="s1">
                                    <label for="radioSecond8">JN Negeri Selangor
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond9" name="s1">
                                    <label for="radioSecond9">JN WP Kuala Lumpur
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond10" name="s1">
                                    <label for="radioSecond10">JN Negeri Sembilan
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond11" name="s1">
                                    <label for="radioSecond11">JN Negeri Melaka
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond12" name="s1">
                                    <label for="radioSecond12">JN Negeri Johor
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond13" name="s1">
                                    <label for="radioSecond13">JN Negeri Pahang
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond14" name="s1">
                                    <label for="radioSecond14">JN Negeri Terengganu
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond15" name="s1">
                                    <label for="radioSecond15">JN Negeri Kelantan
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond16" name="s1">
                                    <label for="radioSecond16">JN Negeri Sarawak
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond17" name="s1">
                                    <label for="radioSecond17">JN Negeri Sabah
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond18" name="s1">
                                    <label for="radioSecond18">JN Cawangan Miri
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond19" name="s1">
                                    <label for="radioSecond19">JN Cawangan Sibu
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond20" name="s1">
                                    <label for="radioSecond20">JN Cawangan Tawau
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond21" name="s1">
                                    <label for="radioSecond21">JN Cawangan Sandakan
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioSecond22" name="s1">
                                    <label for="radioSecond22">JN Cawangan Keningau
                                    </label>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="col-md-12"><hr></div>
                                <h4>13. SEBAB-SEBAB MEMOHON JAWATAN INI :</h4>
                                <div class="col-md-12"><hr></div>
                                <form id="quickForm">
                                <textarea id="sbbmohon">
                                </textarea>
                                </form>

                                <div class="col-md-12"><hr></div>
                                <h4>14. LAPORAN PERISYTIHARAN HARTA: </h4>
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-12">
                                <label>
                                    <p>
                                    Saya
                                    <select>
                                        <option>Belum</option>
                                        <option>Sudah</option>
                                    </select>
                                    mengisytiharkan harta.
                                    </p>
                                    <p>Kelulusan pengisytiharan harta adalah seperti berikut : </p>
                                </label>
                                </div>
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                    <label for="exampleInputPassword1">Rujukan:</label>
                                    <input type="text" name="rujukan" class="form-control" id="rujukan" placeholder="Rujukan">
                                    </div>
                                </div>
                                <div class="offset-md-1 col-md-2">
                                    <label for="exampleInputPassword1">Tarikh:</label>
                                    <div class="input-group date" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#isytiharharta" id="isytiharharta" name="isytiharharta" placeholder="--Sila nyatakan--"/>
                                    <div class="input-group-append" data-target="#isytiharharta" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="col-md-12"><hr></div>
                                <h4>15. PENGESAHAN: </h4>
                                <div class="col-md-12"><hr></div>
                                <div class="col-md-12">
                                <label>
                                    Saya mengaku bahawa segala keterangan di atas adalah benar dan bersetuju untuk ditempatkan di mana-mana Pejabat Jemaah Nazir.
                                </label>
                                </div>
                                <br>
                                <div class="row">
                                <div class="col-md-2">
                                    <label for="col-md-1 exampleInputPassword1">Tarikh:</label>
                                    <div class="input-group date" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#pengesahantarikh" id="pengesahantarikh" name="pengesahantarikh" placeholder="--Sila nyatakan--"/>
                                    <div class="input-group-append" data-target="#pengesahantarikh" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="offset-md-1 col-md-4">
                                    <label for="col-md-1 exampleInputPassword1">Tandatangan Pemohon</label>
                                    <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="kp">
                                        <label class="custom-file-label" for="exampleInputFile">--Pilih Fail--</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text  bg-navy">Muat Naik</span>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-info back-step"><i class="fa fa-arrow-circle-left"></i>Kembali</button>
                                <button class="btn btn-info float-right next-step">Seterusnya<i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        
                        </div>

                    </div>
                </form>
                
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer bg-navy">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      
    </div>
    <!-- Default to the left -->
    <strong>Hak Cipta &copy; 2021 <a href="https://adminlte.io">JEMAAH NAZIR</a>.</strong> Hak Cipta Terpelihara.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- BS-Stepper -->
<script src="plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- dropzonejs -->
<script src="plugins/dropzone/min/dropzone.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- captcha -->
<script src="dist/js/script.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- inserttable -->
<script src="dist/js/inserttable.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Page specific script -->

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date picker
    $('#tarikhlahir').datetimepicker({
        format: 'L'
    });

    $('#tarikhlantikanpertama').datetimepicker({
        format: 'L'
    });

    $('#tarikhlantikankeskimperkhidmatansekarang').datetimepicker({
        format: 'L'
    });

    $('#tarikhdisahkandalamperkhidmatan').datetimepicker({
        format: 'L'
    });

    $('#srdari').datetimepicker({
        format: 'L'
    });

    $('#srsehingga').datetimepicker({
        format: 'L'
    });

    $('#smdari').datetimepicker({
      format:'L'
    });

    $('#smsehingga').datetimepicker({
      format:'L'
    });

    $('#bhgdari').datetimepicker({
      format:'L'
    });

    $('#bhgsehingga').datetimepicker({
      format:'L'
    });

    $('#pglmndari').datetimepicker({
      format:'L'
    });

    $('#pglmnsehingga').datetimepicker({
      format:'L'
    });

    $('#kssdari').datetimepicker({
      format:'L'
    });

    $('#ksssehingga').datetimepicker({
      format:'L'
    });

    $('#tarikhkajian').datetimepicker({
      format:'L'
    });

    $('#sumtarikh').datetimepicker({
      format:'L'
    });

    $('#tarikhnpqel').datetimepicker({
      format:'L'
    });

    $('#tamatpjj').datetimepicker({
      format:'L'
    });

    $('#isytiharharta').datetimepicker({
      format:'L'
    });

    $('#pengesahantarikh').datetimepicker({
      format:'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })

  })
  // BS-Stepper Init
  document.addEventListener('DOMContentLoaded', function () {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
  })

  // DropzoneJS Demo Code Start
  Dropzone.autoDiscover = false

  // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
  var previewNode = document.querySelector("#template")
  previewNode.id = ""
  var previewTemplate = previewNode.parentNode.innerHTML
  previewNode.parentNode.removeChild(previewNode)

  var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: "/target-url", // Set the url
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
  })

  myDropzone.on("addedfile", function(file) {
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
  })

  // Update the total progress bar
  myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
  })

  myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1"
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
  })

  // Hide the total progress bar when nothing's uploading anymore
  myDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0"
  })

  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.
  document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
  }
  document.querySelector("#actions .cancel").onclick = function() {
    myDropzone.removeAllFiles(true)
  }
  // DropzoneJS Demo Code End
</script>
<!-- summernote -->
<script>
  $(function () {
    // $('#summernote').summernote()
    var t = $('#sbbmohon').summernote({
        height: 300,
        focus: false
      });

      var t = $('#ulasankj').summernote({
        height: 300,
        focus: false
      });
  
      // $("#btn").click(function(){
      //   $('div.note-editable').height(150);
      // });
  
    $.validator.setDefaults({
      submitHandler: function () {
        alert( "Form successful submitted!" );
      }
    });
    $('#quickForm').validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 5,
          maxlength:10
        },
        terms: {
          required: true
        },
      },
      messages: {
        email: {
          required: "Please enter a email address",
          email: "Please enter a valid email address"
        },
        password: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long",
          maxlength:"Terlalu panjang"
        },
        terms: "Please accept our terms"
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });
  });
</script>

<!--formwizard -->
    <!--[if gte IE 9]><!-->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->
    <!--<![endif]-->
    
    
    <!-- <script src="../../plugins2/perfect-scrollbar/src/jquery.mousewheel.js"></script> -->
    <script src="plugins2/perfect-scrollbar/src/perfect-scrollbar.js"></script>
    <script src="plugins2/jquery-cookie/jquery.cookie.js"></script>
    <script src="js2/main.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="plugins2/iCheck/jquery.icheck.min.js"></script>
    <script src="plugins2/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="plugins2/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <script src="js2/form-wizard.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script>
        jQuery(document).ready(function() {
            Main.init();
            FormWizard.init();
        });
    </script>
<!--./formwizard -->

<!-- uploadimage -->
<script src="dist/js/uploadimg.js"></script>
</body>
</html>
