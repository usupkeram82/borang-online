<?php
if(isset($_POST['submit'])){
	
	$gredpenyandang = $_POST['gredpenyandang'];
	$opsyen_major = $_POST['opsyen_major'];
	$opsyen_minor = $_POST['opsyen_minor'];
	$penempatan1 = $_POST['penempatan1'];
	$penempatan2 = $_POST['penempatan2'];

	$gelaran = $_POST['gelaran'];
	$namapenuh = $_POST['namapenuh'];
	$namalain = $_POST['namalain'];
	$nokpbaru = $_POST['nokpbaru'];
	$nokplama = $_POST['nokplama'];

	//Profile Picture
	// $profile_pic = $_FILES['profile_picture'];
	$file_tmp = $_FILES['profile_picture']['tmp_name'];
	// $file_ext=explode('.',$_FILES['profile_picture']['name']);
	$profile = $nokpbaru."_profile.png";
	

	$tarikhlahirdate = $_POST['tarikhlahirdate'];
	$jantina = $_POST['jantina'];
	$tempatlahir = $_POST['tempatlahir'];
	$alamatpej = $_POST['alamatpej'];
	$poskodpej = $_POST['poskodpej'];
	$negeripej = $_POST['negeripej'];
	$notelpej = $_POST['notelpej'];
	$alamatrum = $_POST['alamatrum'];
	$poskodrum = $_POST['poskodrum'];
	$negerirum = $_POST['negerirum'];
	$notelbim = $_POST['notelbim'];

	$email = $_POST['email'];
	$tarafkahwin = $_POST['tarafkahwin'];
	$namasuis =$_POST['namasuis'];
	$pekerjaansuis = $_POST['pekerjaansuis'];
	$alamatsuis = $_POST['alamatsuis'];

	$namagb =$_POST['namagb'];
	$jawatangb = $_POST['jawatangb'];
	$emelgb = $_POST['emelgb'];

	//Cek data
	$cek = new formdata();
	$cek->select("fm_tbl_pemohon","*"," pem_nokp_baru = '$nokpbaru'");
	$result = $cek->sql;
	if($result->num_rows<=0){
		$baru = new formdata();
		$baru->insert('fm_tbl_pemohon',[
			'pem_gred'=>$gredpenyandang,
			'pem_opsyen_major'=>$opsyen_major,
			'pem_opsyen_minor'=>$opsyen_minor,
			'pem_pilihan1'=>$penempatan1,
			'pem_pilihan2'=>$penempatan2,
			'pem_gambar'=> $profile,

			'pem_gelaran'=>$gelaran,
			'pem_nama_main'=>$namapenuh,
			'pem_jantina'=>$jantina,
			'pem_nokp_baru'=>$nokpbaru,
			
			'pem_tkh_Lahir'=>date('Y-m-d', strtotime($tarikhlahirdate)),
			
			'pem_tpt_lahir'=>$tempatlahir,
			'pem_pjbt_alamat'=>$alamatpej,
			'pem_pjbt_poskod'=>$poskodpej,
			'pem_pjbt_negeri'=>$negeripej,
			'pem_pjbt_phone'=>$notelpej,
			'pem_rumah_alamat'=>$alamatrum,
			'pem_rumah_poskod'=>$poskodrum,
			'pem_rumah_negeri'=>$negerirum,
			'pem_rumah_phone'=>$notelbim,
			
			'pem_kahwin_status'=>$tarafkahwin,
			'pem_kahwin_psg_nama'=>$namasuis,
			'pem_kahwin_psg_kerja'=>$pekerjaansuis,
			'pem_kahwin_psg_pejabat'=>$alamatsuis,

			'pem_email'=>$email,
			'pem_nama_pengesah'=>$namagb,
			'pem_jawatan_pengesah'=>$jawatangb,
			'pem_emel_pengesah'=>$emelgb
		]);
		if ($baru == true) {
			move_uploaded_file($file_tmp,"datafiles/profile/".$profile);
			header('location:bhgB/permohonannazirB-1.php');
			// echo "Baru";
		}
	}
	else{
		$ubah = new formdata();
		$ubah->update('fm_tbl_pemohon',[
			'pem_gred'=>$gredpenyandang,
			'pem_opsyen_major'=>$opsyen_major,
			'pem_opsyen_minor'=>$opsyen_minor,
			'pem_pilihan1'=>$penempatan1,
			'pem_pilihan2'=>$penempatan2,
			'pem_gambar'=> $profile,
			'pem_gelaran'=>$gelaran,
			'pem_nama_main'=>$namapenuh,
			'pem_nama_option'=>$namalain,
			'pem_jantina'=>$jantina,
			'pem_nokp_baru'=>$nokpbaru,		
			'pem_tkh_Lahir'=>date('Y-m-d', strtotime($tarikhlahirdate)),			
			'pem_tpt_lahir'=>$tempatlahir,
			'pem_pjbt_alamat'=>$alamatpej,
			'pem_pjbt_poskod'=>$poskodpej,
			'pem_pjbt_negeri'=>$negeripej,
			'pem_pjbt_phone'=>$notelpej,
			'pem_rumah_alamat'=>$alamatrum,
			'pem_rumah_poskod'=>$poskodrum,
			'pem_rumah_negeri'=>$negerirum,
			'pem_rumah_phone'=>$notelbim,			
			'pem_kahwin_status'=>$tarafkahwin,
			'pem_kahwin_psg_nama'=>$namasuis,
			'pem_kahwin_psg_kerja'=>$pekerjaansuis,
			'pem_kahwin_psg_pejabat'=>$alamatsuis,
			'pem_email'=>$email,
			'pem_nama_pengesah'=>$namagb,
			'pem_jawatan_pengesah'=>$jawatangb,
			'pem_emel_pengesah'=>$emelgb
		],"pem_nokp_baru='$nokpbaru'");

		if ($ubah == true) {
			move_uploaded_file($file_tmp,"datafiles/profile/".$profile);
			header('location:bhgB/permohonannazirB-1.php');

		}	
	}

}

?>