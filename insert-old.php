<?php
//insert.php
include('utilities/dbcon.php');


// akatable
if(isset($_POST["akanama"]))
{
 $akanama = $_POST["akanama"];
 $akaperingkat = $_POST["akaperingkat"];
 $akabidang = $_POST["akabidang"];
 $akatahun = $_POST["akatahun"];
 $query = '';
 for($count = 0; $count<count($akanama); $count++)
 {
  $akanama_clean = mysqli_real_escape_string($conn, $akanama[$count]);
  $akaperingkat_clean = mysqli_real_escape_string($conn, $akaperingkat[$count]);
  $akabidang_clean = mysqli_real_escape_string($conn, $akabidang[$count]);
  $akatahun_clean = mysqli_real_escape_string($conn, $akatahun[$count]);
  if($akanama_clean != '' && $akaperingkat_clean != '' && $akabidang_clean != '' && $akatahun_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_akademik(aka_institusi, aka_peringkat, aka_bidang, aka_tahun) 
   VALUES("'.$akanama_clean.'", "'.$akaperingkat_clean.'", "'.$akabidang_clean.'", "'.$akatahun_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// srtable
if(isset($_POST["srnama"]))
{
 $srnama = $_POST["srnama"];
 $srjawatan = $_POST["srjawatan"];
 $srdari = $_POST["srdari"];
 $srsehingga = $_POST["srsehingga"];
 $query = '';
 for($count = 0; $count<count($srnama); $count++)
 {
  $srnama_clean = mysqli_real_escape_string($conn, $srnama[$count]);
  $srjawatan_clean = mysqli_real_escape_string($conn, $srjawatan[$count]);
  $srdari_clean = mysqli_real_escape_string($conn,$srdari[$count]);
  $Srdari_clean = date('Y-m-d', strtotime($srdari_clean));
  $srsehingga_clean = mysqli_real_escape_string($conn, $srsehingga[$count]);
  $Srsehingga_clean = date('Y-m-d', strtotime($srsehingga_clean));
  if($srnama_clean != '' && $srjawatan_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat, pglmn_mula, pglmn_tamat) 
   VALUES("4.1.1", "Sekolah Rendah","'.$srnama_clean.'", "'.$srjawatan_clean.'", "'.$Srdari_clean.'", "'.$Srsehingga_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// smtable
if(isset($_POST["smnama"]))
{
 $smnama = $_POST["smnama"];
 $smjawatan = $_POST["smjawatan"];
 $smdari = $_POST["smdari"];
 $smsehingga = $_POST["smsehingga"];
 $query = '';
 for($count = 0; $count<count($smnama); $count++)
 {
  $smnama_clean = mysqli_real_escape_string($conn, $smnama[$count]);
  $smjawatan_clean = mysqli_real_escape_string($conn, $smjawatan[$count]);
  $smdari_clean = mysqli_real_escape_string($conn, $smdari[$count]);
  $Smdari_clean = date('Y-m-d', strtotime($smdari_clean));
  $smsehingga_clean = mysqli_real_escape_string($conn, $smsehingga[$count]);
  $Smsehingga_clean = date('Y-m-d', strtotime($smsehingga_clean));
  if($smnama_clean != '' && $smjawatan_clean != '' && $Smdari_clean != '' && $Smsehingga_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat, pglmn_mula, pglmn_tamat) 
   VALUES("4.1.2", "Sekolah Menengah","'.$smnama_clean.'", "'.$smjawatan_clean.'", "'.$Smdari_clean.'", "'.$Smsehingga_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// bhgtable
if(isset($_POST["bhgnama"]))
{
 $bhgnama = $_POST["bhgnama"];
 $bhgjawatan = $_POST["bhgsehingga"];
 $bhgdari = $_POST["bhgdari"];
 $bhgsehingga = $_POST["bhgsehingga"];
 $query = '';
 for($count = 0; $count<count($bhgnama); $count++)
 {
  $bhgnama_clean = mysqli_real_escape_string($conn, $bhgnama[$count]);
  $bhgjawatan_clean = mysqli_real_escape_string($conn, $bhgjawatan[$count]);
  $bhgdari_clean = mysqli_real_escape_string($conn, $bhgdari[$count]);
  $Bhgdari_clean = date('Y-m-d', strtotime($bhgdari_clean));
  $bhgsehingga_clean = mysqli_real_escape_string($conn, $bhgsehingga[$count]);
  $Bhgsehingga_clean = date('Y-m-d', strtotime($bhgsehingga_clean));
  if($bhgnama_clean != '' && $bhgjawatan_clean != '' && $Bhgdari_clean != '' && $Bhgsehingga_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat, pglmn_mula, pglmn_tamat) 
   VALUES("4.1.3","Bahagian/JPN/PPD","'.$bhgnama_clean.'", "'.$bhgjawatan_clean.'", "'.$Bhgdari_clean.'", "'.$Bhgsehingga_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// pdstable
if(isset($_POST["pdsmpd"]))
{
 $pdsmpd = $_POST["pdsmpd"];
 $pdsdt = $_POST["pdsdt"];
 $pglmndari = $_POST["pglmndari"];
 $pglmnsehingga = $_POST["pglmnsehingga"];
 $query = '';
 for($count = 0; $count<count($pdsmpd); $count++)
 {
  $pdsmpd_clean = mysqli_real_escape_string($conn, $pdsmpd[$count]);
  $pdsdt_clean = mysqli_real_escape_string($conn, $pdsdt[$count]);
  $pglmndari_clean = mysqli_real_escape_string($conn, $pglmndari[$count]);
  $Pglmndari_clean = date('Y-m-d',strtotime($pglmndari_clean));
  $pglmnsehingga_clean = mysqli_real_escape_string($conn, $pglmnsehingga[$count]);
  $Pglmnsehingga_clean = date('Y-m-d',strtotime($pglmnsehingga_clean));
  if($pdsmpd_clean != '' && $pdsdt_clean != '' && $Pglmndari_clean != '' && $Pglmnsehingga_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat, pglmn_mula, pglmn_tamat) 
   VALUES("4.2","Pengalaman Di Sekolah","'.$pdsmpd_clean.'", "'.$pdsdt_clean.'", "'.$Pglmndari_clean.'", "'.$Pglmnsehingga_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// tugaslaintable
if(isset($_POST["tugaslainnama"]))
{
 $tugaslainnama = $_POST["tugaslainnama"];
 $tugaslainjawatan = $_POST["tugaslainjawatan"];
 $query = '';
 for($count = 0; $count<count($tugaslainnama); $count++)
 {
  $tugaslainnama_clean = mysqli_real_escape_string($conn, $tugaslainnama[$count]);
  $tugaslainjawatan_clean = mysqli_real_escape_string($conn, $tugaslainjawatan[$count]);
  if($tugaslainnama_clean != '' && $tugaslainjawatan_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
   VALUES("4.3","Tugas Dan Tanggungjawab Lain Semasa Di Sekolah (3 Tahun Terakhir)","'.$tugaslainnama_clean.'", "'.$tugaslainjawatan_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// kursustable
if(isset($_POST["kursusnama"]))
{
 $kursusnama = $_POST["kursusnama"];
 $kursustempat = $_POST["kursustempat"];
 $kssdari = $_POST["kssdari"];
 $ksssehingga = $_POST["ksssehingga"];
 $query = '';
 for($count = 0; $count<count($kursusnama); $count++)
 {
  $kursusnama_clean = mysqli_real_escape_string($conn, $kursusnama[$count]);
  $kursustempat_clean = mysqli_real_escape_string($conn, $kursustempat[$count]);
  $kssdari_clean = mysqli_real_escape_string($conn, $kssdari[$count]);
  $Kssdari_clean = date('Y-m-d',strtotime($kssdari_clean));
  $ksssehingga_clean = mysqli_real_escape_string($conn, $ksssehingga[$count]);
  $Ksssehingga_clean = date('Y-m-d',strtotime($ksssehingga_clean));
  if($kursusnama_clean != '' && $kursustempat_clean != '' && $Kssdari_clean != '' && $Ksssehingga_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat, pglmn_mula, pglmn_tamat) 
   VALUES("5","Kursus-Kursus Yang Dihadiri","'.$kursusnama_clean.'", "'.$kursustempat_clean.'", "'.$Kssdari_clean.'", "'.$Ksssehingga_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// kajiantable
if(isset($_POST["kajiannama"]))
{
 $kajiannama = $_POST["kajiannama"];
 $tarikhkajian = $_POST["tarikhkajian"];
 $query = '';
 for($count = 0; $count<count($kajiannama); $count++)
 {
  $kajiannama_clean = mysqli_real_escape_string($conn, $kajiannama[$count]);
  $tarikhkajian_clean = mysqli_real_escape_string($conn, $tarikhkajian[$count]);
  $Tarikhkajian_clean = date('Y-m-d',strtotime($tarikhkajian_clean));
  if($kajiannama_clean != '' && $Tarikhkajian_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
   VALUES("6","KAJIAN / PENERBITAN IKHTISAS YANG PERNAH DIHASILKAN","'.$kajiannama_clean.'", "'.$Tarikhkajian_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// sumbangantable
if(isset($_POST["sumbanganjenis"]))
{
 $sumbanganjenis = $_POST["sumbanganjenis"];
 $sumbanganperingkat = $_POST["sumbanganperingkat"];
 $sumtarikh = $_POST["sumtarikh"];
 $query = '';
 for($count = 0; $count<count($sumbanganjenis); $count++)
 {
  $sumbanganjenis_clean = mysqli_real_escape_string($conn, $sumbanganjenis[$count]);
  $sumbanganperingkat_clean = mysqli_real_escape_string($conn, $sumbanganperingkat[$count]);
  $sumtarikh_clean = mysqli_real_escape_string($conn, $sumtarikh[$count]);
  $Sumtarikh_clean = date('Y-m-d',strtotime($sumtarikh_clean));
  if($sumbanganjenis_clean != '' && $sumbanganperingkat_clean != '' && $Sumtarikh_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat, pglmn_mula) 
   VALUES("7","SUMBANGAN PROFESIONAL DI LUAR TUGAS RASMI","'.$sumbanganjenis_clean.'", "'.$sumbanganperingkat_clean.'", "'.$Sumtarikh_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// kegiatantable
if(isset($_POST["kegiatannama"]))
{
 $kegiatannama = $_POST["kegiatannama"];
 $kegiatanjawatan = $_POST["kegiatanjawatan"];
 $query = '';
 for($count = 0; $count<count($kegiatannama); $count++)
 {
  $kegiatannama_clean = mysqli_real_escape_string($conn, $kegiatannama[$count]);
  $kegiatanjawatan_clean = mysqli_real_escape_string($conn, $kegiatanjawatan[$count]);
  if($kegiatannama_clean != '' && $kegiatanjawatan_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
   VALUES("8","KEGIATAN LUAR","'.$kegiatannama_clean.'", "'.$kegiatanjawatan_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// anugerahtable
if(isset($_POST["anugerahjenis"]))
{
 $anugerahjenis = $_POST["anugerahjenis"];
 $anugerahtahun = $_POST["anugerahtahun"];
 $query = '';
 for($count = 0; $count<count($anugerahjenis); $count++)
 {
  $anugerahjenis_clean = mysqli_real_escape_string($conn, $anugerahjenis[$count]);
  $anugerahtahun_clean = mysqli_real_escape_string($conn, $anugerahtahun[$count]);
  if($anugerahjenis_clean != '' && $anugerahtahun_clean != '')
  {
   $query .= '
   INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
   VALUES("9","PENGIKTIRAFAN / ANUGERAH YANG DITERIMA","'.$anugerahjenis_clean.'", "'.$anugerahtahun_clean.'"); 
   ';
  }
 }
 if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
   echo 'Item Data Inserted';
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo 'All Fields are Required';
 }
}

// maklumatlain
if(isset($_POST["savemaklumatlain"]))
{

    // ya @ tidak
    $ei_jawapan1 = mysqli_real_escape_string($conn,$_POST["ei_jawapan1"]);
    $ei_jawapan2 = mysqli_real_escape_string($conn,$_POST["ei_jawapan2"]);
    $ei_jawapan3 = mysqli_real_escape_string($conn,$_POST["ei_jawapan3"]);
    $ei_jawapan4 = mysqli_real_escape_string($conn,$_POST["ei_jawapan4"]);
    $ei_jawapan5 = mysqli_real_escape_string($conn,$_POST["ei_jawapan5"]);

    //  tahun
    $ei_extra11 = mysqli_real_escape_string($conn,$_POST["ei_extra11"]);
    $ei_extra12 = mysqli_real_escape_string($conn,$_POST["ei_extra12"]);
    $ei_extra13 = mysqli_real_escape_string($conn,$_POST["ei_extra13"]);
    $ei_extra14 = mysqli_real_escape_string($conn,$_POST["ei_extra14"]);
    $ei_extra16 = mysqli_real_escape_string($conn,$_POST["ei_extra16"]);

    //  date
    $ei_extra24 = mysqli_real_escape_string($conn,$_POST["ei_extra24"]);
    $Ei_extra24 = date('Y-m-d', strtotime($ei_extra24));
    $ei_extra25 = mysqli_real_escape_string($conn,$_POST["ei_extra25"]);
    $Ei_extra25 = date('Y-m-d', strtotime($ei_extra25));
    
    $query = '';
    
    $query .= '
    INSERT INTO fm_tbl_extrainfo(ei_code, ei_jawapan, ei_extra1, ei_extra2) 
    VALUES("11.1","'.$ei_jawapan1.'", "'.$ei_extra11.'" , ""); 
    ';

    $query .= '
    INSERT INTO fm_tbl_extrainfo(ei_code, ei_jawapan, ei_extra1, ei_extra2) 
    VALUES("11.2","'.$ei_jawapan2.'", "'.$ei_extra12.'" , ""); 
    ';

    $query .= '
    INSERT INTO fm_tbl_extrainfo(ei_code, ei_jawapan, ei_extra1, ei_extra2) 
    VALUES("11.3","'.$ei_jawapan3.'", "'.$ei_extra13.'" , ""); 
    ';

    $query .= '
    INSERT INTO fm_tbl_extrainfo(ei_code, ei_jawapan, ei_extra1, ei_extra2) 
    VALUES("11.4","'.$ei_jawapan4.'", "'.$ei_extra14.'" , "'.$Ei_extra24.'"); 
    ';

    $query .= '
    INSERT INTO fm_tbl_extrainfo(ei_code, ei_jawapan, ei_extra1, ei_extra2) 
    VALUES("11.5","'.$ei_jawapan5.'", "" , "'.$Ei_extra25.'"); 
    ';

    $query .= '
    INSERT INTO fm_tbl_extrainfo(ei_code, ei_jawapan, ei_extra1, ei_extra2) 
    VALUES("11.6","", "'.$ei_extra16.'" , ""); 
    ';

    
    if($query != '')
    {
    if(mysqli_multi_query($conn, $query))
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-16.php';</script>";
    }
    else
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-15.php';</script>";
    }
    }
    else
    {
    echo 'All Fields are Required';
    }
}

// Pengalaman Perkhidmatan
if(isset($_POST["save4point1"]))
{
    $srtahun= mysqli_real_escape_string($conn,$_POST["pglmnsrtahun"]);
    $smtahun= mysqli_real_escape_string($conn,$_POST["pglmnsmtahun"]);
    $bhgntahun= mysqli_real_escape_string($conn,$_POST["pglmnbhgntahun"]);
    $totaltahun= mysqli_real_escape_string($conn,$_POST["pglmnjumlahtahun"]);

    $query = '';

    $query .= '
    INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
    VALUES("4.1","Bilangan tahun berkhidmat","Sekolah Rendah", "'.$srtahun.'"); 
    ';

    $query .= '
    INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
    VALUES("4.1","Bilangan tahun berkhidmat","Sekolah Rendah", "'.$srtahun.'"); 
    ';

    $query .= '
    INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
    VALUES("4.1","Bilangan tahun berkhidmat","Sekolah Menengah", "'.$smtahun.'"); 
    ';

    $query .= '
    INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat) 
    VALUES("4.1","Bilangan tahun berkhidmat","Jumlah Tahun Berkhidmat", "'.$totaltahun.'"); 
    ';

    if($query != '')
    {
    if(mysqli_multi_query($conn, $query))
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-4.php';</script>";
    }
    else
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-3.php';</script>";
    }
    }
    else
    {
    echo 'All Fields are Required';
    }
}

// Butir perkhidmatan
if(isset($_POST["savebutirperkhidmatan"]))
{
  $tarikhlantikanpertama= mysqli_real_escape_string($conn,$_POST["tarikhlantikanpertama"]);
  $Tarikhlantikanpertama= date('Y-m-d', strtotime($tarikhlantikanpertama));
  $tarikhlantikankeskimperkhidmatansekarang= mysqli_real_escape_string($conn,$_POST["tarikhlantikankeskimperkhidmatansekarang"]);
  $Tarikhlantikankeskimperkhidmatansekarang= date('Y-m-d', strtotime($tarikhlantikankeskimperkhidmatansekarang));
  $tarikhdisahkandalamperkhidmatan= mysqli_real_escape_string($conn,$_POST["tarikhdisahkandalamperkhidmatan"]);
  $Tarikhdisahkandalamperkhidmatan= date('Y-m-d', strtotime($tarikhdisahkandalamperkhidmatan));
  $gredhakiki= mysqli_real_escape_string($conn,$_POST["gredhakiki"]);
  $jwtnskrg= mysqli_real_escape_string($conn,$_POST["jwtnskrg"]);
  $tarafjwtn= mysqli_real_escape_string($conn,$_POST["tarafjwtn"]);

  $query = '';
  
  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_mula)
  VALUES("2.1","Tarikh lantikan pertama", "'.$Tarikhlantikanpertama.'");
  ';

  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_mula)
  VALUES("2.2","Tarikh lantikan ke skim perkhidmatan sekarang", "'.$Tarikhlantikankeskimperkhidmatansekarang.'");
  ';

  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_mula)
  VALUES("2.3","Tarikh disahkan dalam perkhidmatan", "'.$Tarikhdisahkandalamperkhidmatan.'");
  ';

  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan)
  VALUES("2.4","Gred hakiki penyandang", "'.$gredhakiki.'");
  ';

  $query .= '
  INSERT INTO tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan)
  VALUES("2.5"," Jawatan Sekarang", "'.$jwtnskrg.'");
  ';

  $query .= '
  INSERT INTO tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan)
  VALUES("2.6","Taraf Jawatan", "'.$tarafjwtn.'");
  ';

  if($query != '')
    {
    if(mysqli_multi_query($conn, $query))
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-2.php';</script>";
    }
    else
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-1.php';</script>";
    }
    }
    else
    {
    echo 'All Fields are Required';
    }
}

// Kemahiran Literasi
if(isset($_POST["savekemahiran"]))
{
  $msword= mysqli_real_escape_string($conn,$_POST["msword"]);
  $msexcel= mysqli_real_escape_string($conn,$_POST["msexcel"]);
  $mspp= mysqli_real_escape_string($conn,$_POST["mspp"]);
  $msaccess= mysqli_real_escape_string($conn,$_POST["msaccess"]);
  $db= mysqli_real_escape_string($conn,$_POST["db"]);
  $spss= mysqli_real_escape_string($conn,$_POST["spss"]);

  $query = '';

  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat)
  VALUES("10","KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI", "MS Word", "'.$msword.'");
  ';
  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat)
  VALUES("10","KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI", "MS Excel","'.$msexcel.'");
  ';
  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat)
  VALUES("10","KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI", "MS PowerPoint","'.$mspp.'");
  ';
  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat)
  VALUES("10","KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI", "MS Access","'.$msaccess.'");
  ';
  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat)
  VALUES("10","KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI", "Database","'.$db.'");
  ';
  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat)
  VALUES("10","KEMAHIRAN LITERASI KOMPUTER DAN PENGGUNAAN APLIKASI", "SPSS","'.$spss.'");
  ';

  if($query != '')
    {
    if(mysqli_multi_query($conn, $query))
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-15.php';</script>";
    }
    else
    {
      echo "<script>location.href = 'bhgB/permohonannazirB-14.php';</script>";
    }
  }
  else
  {
  echo 'All Fields are Required';
  }
}

// 1215
if(isset($_POST["save1215"]))
{
  $pusattemuduga= mysqli_real_escape_string($conn,$_POST["pusattemuduga"]);
  $sbbmohon= mysqli_real_escape_string($conn,$_POST["sbbmohon"]);
  $statusisytiharharta= mysqli_real_escape_string($conn,$_POST["statusisytiharharta"]);
  $rujukan= mysqli_real_escape_string($conn,$_POST["rujukan"]);
  $isytiharharta= mysqli_real_escape_string($conn,$_POST["isytiharharta"]);
  $Isytiharharta= date('Y-m-d', strtotime($isytiharharta));
  $pengesahantarikh= mysqli_real_escape_string($conn,$_POST["pengesahantarikh"]);
  $Pengesahantarikh= date('Y-m-d', strtotime($pengesahantarikh));

  $query = '';

  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan)
  VALUES("12","PUSAT TEMU DUGA YANG DIPILIH", "'.$pusattemuduga.'");
  ';
  
  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan)
  VALUES("13","SEBAB-SEBAB MEMOHON JAWATAN JEMAAH NAZIR", "'.$sbbmohon.'");
  ';

  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_peringkat, pglmn_mula)
  VALUES("14","LAPORAN PERISYTIHARAN HARTA", "'.$rujukan.'", "'.$statusisytiharharta.'", "'.$Isytiharharta.'");
  ';

  $query .= '
  INSERT INTO fm_tbl_pengalaman(pglmn_group, pglmn_jns, pglmn_keterangan, pglmn_mula)
  VALUES("15","PENGESAHAN", "Tarikh Pengesahan Permohon",, "'.$Pengesahantarikh.'");
  ';

  if($query != '')
 {
  if(mysqli_multi_query($conn, $query))
  {
    echo "<script>location.href = 'permohonannazirC.php';</script>";
  }
  else
  {
   echo 'Error';
  }
 }
 else
 {
  echo "<script>location.href = 'bhgB/permohonannazirB-16.php';</script>";
 }
}

// saveC
if(isset($_POST["saveC"]))
{
  $c1= mysqli_real_escape_string($conn,$_POST["c1"]);
  $c2_i= mysqli_real_escape_string($conn,$_POST["c2_i"]);
  $c2_ii= mysqli_real_escape_string($conn,$_POST["c2_ii"]);
  $c2_iii= mysqli_real_escape_string($conn,$_POST["c2_iii"]);
  $c2_iv= mysqli_real_escape_string($conn,$_POST["c2_iv"]);
  $c2_v= mysqli_real_escape_string($conn,$_POST["c2_v"]);
  $c2_vi= mysqli_real_escape_string($conn,$_POST["c2_vi"]);
  $sokongan= mysqli_real_escape_string($conn,$_POST["sokongan"]);
  $ulasankj= mysqli_real_escape_string($conn,$_POST["ulasankj"]);
  $tarikhpengesahankj= mysqli_real_escape_string($conn,$_POST["tarikhpengesahankj"]);
  $Tarikhpengesahankj= date('Y-m-d', strtotime($tarikhpengesahankj));

  $ctindakan_i= mysqli_real_escape_string($conn,$_POST["ctindakan_i"]);
  $ctindakan_ii= mysqli_real_escape_string($conn,$_POST["ctindakan_ii"]);
  $ctindakan_iii= mysqli_real_escape_string($conn,$_POST["ctindakan_iii"]);
  $ctindakan_iv= mysqli_real_escape_string($conn,$_POST["ctindakan_iv"]);
  $ctindakan_v= mysqli_real_escape_string($conn,$_POST["ctindakan_v"]);
  $ctindakan_vi= mysqli_real_escape_string($conn,$_POST["ctindakan_vi"]);
  $ctindakan_vii= mysqli_real_escape_string($conn,$_POST["ctindakan_vii"]);
  $tahuntindakan= mysqli_real_escape_string($conn,$_POST["tahuntindakan"]);
  $tarikhperakuantatatertib= mysqli_real_escape_string($conn,$_POST["tarikhperakuantatatertib"]);
  $Tarikhperakuantatatertib= date('Y-m-d', strtotime($tarikhperakuantatatertib));

  $query = '';

  if(isset($_POST['c1'])&& $_POST['c1']== 'Ya')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("1","Sepanjang pengetahuan saya, butir-butir yang dinyatakan oleh pemohon seperti di atas adalah benar", "Ya");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("1","Sepanjang pengetahuan saya, butir-butir yang dinyatakan oleh pemohon seperti di atas adalah benar", "Tidak");
    ';
  }

  if(isset($_POST['c2_i'])&& $_POST['c2_i']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2i","Satu salinan Kad Pengenalan", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2i","Satu salinan Kad Pengenalan", "Tiada");
    ';
  }

  if(isset($_POST['c2_ii'])&& $_POST['c2_ii']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2ii","Satu salinan Buku Perkhidmatan Kerajaan yang lengkap dan kemas kini serta disahkan", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2ii","Satu salinan Buku Perkhidmatan Kerajaan yang lengkap dan kemas kini serta disahkan", "Tiada");
    ';
  }

  if(isset($_POST['c2_iii'])&& $_POST['c2_iii']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2iii","Perakuan markah prestasi bagi 3 tahun terkini", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2iii","Perakuan markah prestasi bagi 3 tahun terkini", "Tiada");
    ';
  }

  if(isset($_POST['c2_iv'])&& $_POST['c2_iv']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2iv","Perakuan Tindakan Tatatertib / Surcaj / Keterhutangan Kewangan Yang Serius", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2iv","Perakuan Tindakan Tatatertib / Surcaj / Keterhutangan Kewangan Yang Serius", "Tiada");
    ';
  }

  if(isset($_POST['c2_v'])&& $_POST['c2_v']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2v","Kelayakan Cuti yang telah dikemas kini dan disahkan", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2v","Kelayakan Cuti yang telah dikemas kini dan disahkan", "Tiada");
    ';
  }

  if(isset($_POST['c2_vi'])&& $_POST['c2_vi']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2vi","Salinan sijil-sijil kelayakan akademik yang disahkan", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("2vi","Salinan sijil-sijil kelayakan akademik yang disahkan", "Tiada");
    ';
  }

  if(isset($_POST['sokongan'])&& $_POST['sokongan']== 'sokong')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan, pgshn_sokongan)
    VALUES("3","Permohonan", "Disokong", "1");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan, pgshn_sokongan)
    VALUES("3","Permohonan", "Tidak disokong", "0");
    ';
  }

  $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("4","Ulasan Ketua Jabatan", "'.$ulasankj.'");
    ';

  $query .= '
  INSERT INTO fm_tbl_pengesahan(pgshn_keterangan, pgshn_tarikh)
  VALUES("Tarikh Pengesahan", "'.$Tarikhpengesahankj.'");
  ';

  if(isset($_POST['ctindakan_i'])&& $_POST['ctindakan_i']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("i","Amaran", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("i","Amaran", "Tiada");
    ';
  }

  if(isset($_POST['ctindakan_ii'])&& $_POST['ctindakan_ii']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("ii","Denda", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("ii","Denda", "Tiada");
    ';
  }

  if(isset($_POST['ctindakan_iii'])&& $_POST['ctindakan_iii']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("iii","Lucut Hak Emolumen", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("iii","Lucut Hak Emolumen", "Tiada");
    ';
  }

  if(isset($_POST['ctindakan_iv'])&& $_POST['ctindakan_iv']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("iv","Tangguh Pergerakan Gaji", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("iv","Tangguh Pergerakan Gaji", "Tiada");
    ';
  }

  if(isset($_POST['ctindakan_v'])&& $_POST['ctindakan_v']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("v","Turun Gaji", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("v","Turun Gaji", "Tiada");
    ';
  }

  if(isset($_POST['ctindakan_vi'])&& $_POST['ctindakan_vi']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("vi","Turun Pangkat", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("vi","Turun Pangkat", "Tiada");
    ';
  }

  if(isset($_POST['ctindakan_vii'])&& $_POST['ctindakan_vii']== 'Ada')
  {
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("vii","Buang Kerja", "Ada");
    ';
  }

  else{
    $query .= '
    INSERT INTO fm_tbl_pengesahan(pgshn_jns, pgshn_keterangan, pgshn_ulasan)
    VALUES("vii","Buang Kerja", "Tiada");
    ';
  }

  $query .= '
  INSERT INTO fm_tbl_pengesahan(pgshn_keterangan, pgshn_hukum_tahun)
  VALUES("Tahun dijatuhkan hukuman tatatertib", "'.$tahuntindakan.'");
  ';

  $query .= '
  INSERT INTO fm_tbl_pengesahan(pgshn_keterangan, pgshn_tarikh)
  VALUES("Tarikh Perakuan", "'.$Tarikhperakuantatatertib.'");
  ';

  if($query != '')
  {
    if(mysqli_multi_query($conn, $query))
    {
      echo "<script>location.href = 'index.php';</script>";
    }
    else
    {
    echo 'Error';
    }
  }
  else
  {
    echo "<script>location.href = 'permohonannazirC.php';</script>";
  }

}

?>
