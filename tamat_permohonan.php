<?php
session_start();
include 'utilities/formdata.php';
$nokppemohon=$_SESSION['nokppemohon'];
if(isset($_POST['saveC'])){
	
	$pengesahan = $_POST['pengesahan'];
	$sokongan = $_POST['sokongan'];
	$ulasankj = $_POST['ulasankj'];
   $statusttb = $_POST['statusttb'];
   if($statusttb=='pernah'){
      if(isset($_POST['ctindakan_i'])){
         $ctindakan_i = $_POST['ctindakan_i'];
      }
      if(isset($_POST['ctindakan_ii'])){
         $ctindakan_ii = $_POST['ctindakan_ii'];
      }
      if(isset($_POST['ctindakan_iii'])){
         $ctindakan_iii = $_POST['ctindakan_iii'];
      }
      if(isset($_POST['ctindakan_iv'])){
         $ctindakan_iv = $_POST['ctindakan_iv'];
      }
      if(isset($_POST['ctindakan_v'])){
         $ctindakan_v = $_POST['ctindakan_v'];
      }
      
      if(isset($_POST['ctindakan_vi'])){
         $ctindakan_vi = $_POST['ctindakan_vi'];
      }

      if(isset($_POST['ctindakan_vii'])){
         $ctindakan_vii = $_POST['ctindakan_vii'];
      }

      if(isset($_POST['tahuntindakan'])){
         $tahuntindakan = $_POST['tahuntindakan'];
      }
      
      if(isset($_POST['tarikhperakuantatatertib'])){
         $tarikhperakuantatatertib = date('Y-m-d', strtotime($_POST['tarikhperakuantatatertib']));
      }  
   }
   else{
      $ctindakan_i = '';
      $ctindakan_ii = '';
      $ctindakan_iii = '';
      $ctindakan_iv = '';
      $ctindakan_v = '';
      $ctindakan_vi = '';
      $ctindakan_vii = '';
      $tahuntindakan = '';
      $tarikhperakuantatatertib = '';
   }
   
   //Semak pengesahan dah wjud?
   $semak = new formdata();
   $semak->select("fm_tbl_pengesahan","*"," pgshn_pemohon_nokp='$nokppemohon'");
   $result = $semak->sql;
   if($result->num_rows>0){
      echo '<script>alert("Pengesahan telahpun dibuat");';
      echo 'window.history.go(-1);</script>';
      exit;
   }
   else{
      $baru = new formdata();
		$baru->insert('fm_tbl_pengesahan',[
         'pgshn_pemohon_nokp'=>$nokppemohon,
			'pgshn_kj'=>$pengesahan,
			'pgshn_sokongan'=>$sokongan,
			'pgsgn_ulasan'=>$ulasankj,
         'pgshn_status_ttb'=>$statusttb,
			'pgshn_tindakan1'=>$ctindakan_i,
			'pgshn_tindakan2'=>$ctindakan_ii,
         'pgshn_tindakan3'=>$ctindakan_iii,
			'pgshn_tindakan4'=>$ctindakan_iv,
			'pgshn_tindakan5'=>$ctindakan_v,
			'pgshn_tindakan6'=>$ctindakan_vi,
         'pgshn_tindakan7'=>$ctindakan_vii,
         'pgshn_tahun'=>$tahuntindakan,
			'pgshn_perakuan'=>$tarikhperakuantatatertib
		]);
		if ($baru == true) {
         include 'PHPMailerForm/maklumansah_email.php';
         if(isset($_SESSION['nokppemohon'])){   
            $update = new formdata();
            $update->update('fm_tbl_aktifkan',['aktif_status'=>3],"aktif_nokp='$nokppemohon'");

            if ($update == true) {
               $mesej = 'Pengesahan ketua Jabatan/Guru Besar\nTelah berjaya dilakukan\nTERIMA KASIH';
               echo "<script>
               alert('".$mesej."');
               window.location.href='index.php';
               </script>";
               exit;
            }
            else{
               echo '<script>alert("Terdapat kesilapan, Sila cuba semula");';
               echo 'window.history.back();</script>';
               exit;
            }
         }
		}
      else{
         echo '<script>alert("Terdapat kesilapan, Sila cuba semula");';
         echo 'window.history.go(-1);</script>';
         exit;
      }
   }
}
?>