<?php
use App\Http\Controllers\PemohonController;
use App\Http\Controllers\NegeriController;
use App\Http\Controllers\JawatanController;
use App\Http\Controllers\GelaranController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/menu', 'AdminController@menu')->name('menu');
// Route::get('/senarai-permohonan', 'PemohonController@index');
Route::get('/senarai-temuduga', 'AdminController@senarai_temuduga')->name('senarai-temuduga');
Route::get('/senarai-pencerapan', 'AdminController@senarai_pencerapan')->name('senarai-pencerapan');

Route::get('/emel', 'EmailController@index')->name('emel');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/senarai-permohonan', 'PemohonController@index')->name('senarai-permohonan');
Route::get('/senarai-temuduga', 'PemohonController@temuduga')->name('senarai-temuduga');
Route::get('/senarai-pencerapan', 'PemohonController@pencerapan')->name('senarai-pencerapan');
Route::get('/pemohonan/update-status', 'PemohonController@ajaxUpdateStatus')->name('update-status');
Route::get('/update-temuduga', 'PemohonController@ajaxUpdateTemuduga')->name('update-temuduga');

Route::prefix("/tetapan/negeri")->group(function(){
    Route::any('/index','NegeriController@index')->name('index-negeri');
    Route::get('/create','NegeriController@create')->name('create-negeri');
    Route::post('/save','NegeriController@save')->name('save-negeri');
    Route::get('/edit/{negeri_id}','NegeriController@edit')->name('edit-negeri');
    Route::get('/delete/{negeri_id}','NegeriController@delete')->name('delete-negeri');
});

Route::prefix("/tetapan/jawatan")->group(function(){
    Route::any('/index','JawatanController@index')->name('index-jwt');
    Route::get('/create','JawatanController@create')->name('create-jwt');
    Route::post('/save','JawatanController@save')->name('save-jwt');
    Route::get('/edit/{jawatan_id}','JawatanController@edit')->name('edit-jwt');
    Route::get('/delete/{jawatan_id}','JawatanController@delete')->name('delete-jwt');
});

Route::prefix("/tetapan/gelaran")->group(function(){
    Route::any('/index','GelaranController@index')->name('index-glr');
    Route::get('/create','GelaranController@create')->name('create-glr');
    Route::post('/save','GelaranController@save')->name('save-glr');
    Route::get('/edit/{gelaran_id}','GelaranController@edit')->name('edit-glr');
    Route::get('/delete/{gelaran_id}','GelaranController@delete')->name('delete-glr');
});