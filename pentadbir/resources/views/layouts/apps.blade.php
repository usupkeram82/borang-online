<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistem Admin</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">

  @yield('includecss')

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/menu/" class="nav-link">MENU</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
		<!-- Navbar Search -->
		<li class="nav-item">
			<a class="nav-link" data-widget="navbar-search" href="#" role="button">
			  <i class="fas fa-search"></i>
			</a>
			<div class="navbar-search-block">
			  <form class="form-inline">
				<div class="input-group input-group-sm">
				  <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
				  <div class="input-group-append">
					<button class="btn btn-navbar" type="submit">
					  <i class="fas fa-search"></i>
					</button>
					<button class="btn btn-navbar" type="button" data-widget="navbar-search">
					  <i class="fas fa-times"></i>
					</button>
				  </div>
				</div>
			  </form>
			</div>
		  </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">KELUAR</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
      <img src="{{ asset('dist/img/jata_white.png')}}" alt="AdminLTE Logo"  class="brand-image img-square elevation-0" style="opacity: .8" >
      <span class="brand-text font-weight-light">Pengurusan JN</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('dist/img/user_profile.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Fairuze</a>
        </div>
      </div>


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-header">SENARAI NAZIR BAHARU</li>
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
              <i class="fas fa-chart-pie"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <li class="nav-item">
              <a href="{{ route('senarai-permohonan') }}" class="nav-link">
                <i class="far fa-user"></i>
                <p>Permohonan Baharu</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('senarai-temuduga') }}" class="nav-link">
                <i class="fas fa-headphones"></i>
                <p>Layak Temuduga</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('senarai-pencerapan') }}" class="nav-link">
                <i class="fas fa-graduation-cap"></i>
                <p>Senarai Pencerapan</p>
              </a>
            </li>
          </li>          
          <li class="nav-header">TETAPAN</li>
		      <li class="nav-item">
            <a href="{{ route('index-negeri') }}" class="nav-link">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Selenggara Negeri
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('index-jwt') }}" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Selenggara Jawatan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('index-glr') }}" class="nav-link">
              <i class="nav-icon fas fa-list-ol"></i>
              <p>
                Selenggara Gelaran
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          @yield('content')
        </div>
	    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2022 <a href="https://adminlte.io">Sistem Jemaah Nazir</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

@yield('includejs')

<!-- overlayScrollbars -->
<script src="{{ asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
@yield('js')
</body>
</html>
