{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
@extends('layouts.apps')

@section('content')
<div class="container">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>150</h3>
  
                  <p>Permohonan Nazir Baharu</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="/senarai-permohonan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>53<sup style="font-size: 20px"></sup></h3>
  
                  <p>Layak Temuduga</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="/senarai-temuduga" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3>44</h3>
  
                  <p>Layak Pencerapan</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="/senarai-pencerapan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3>65</h3>
  
                  <p>Simpanan</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>
          <!-- /.row -->
        <div class="row">
       <!-- right column -->
       <div class="col-md-12">
        <!-- Form Element sizes -->
        <div class="card card-orange card-outline">
          <div class="card-header">
            <h5 class="card-title m-0 card-outline text-align:center">INFO SEMASA</h5>
          </div>
            <div class="card-body ">
              <h5 style="text-align: center;"><b>MANUAL PENGGUNAAN SISTEM</b></h5>
              <h6 style="color:#ff0000; text-align: center;">Sila pastikan anda adalah admin pengurusan bagi mengendalikan sistem ini.</h6><br>
              <p style="text-align: justify;">
                1. Senarai pemohon yang membuat permohonan nazir baharu akan disimpan di menu senarai permohonan. <br>
                2. Senarai pemohon yang telah ditapis akan disimpan di dalam menu senarai temuduga. <br>
                3. Senarai pemohon yang lulus temuduga akan disimpan di dalam menu senarai pencerapan bagi proses seterusnya. <br>

                Nota: Sebarang kesulitan sila hubungi urusetia pihak pengurusan Jemaah Nazir
            </p>
            </div>
        </div>

      </div>
      <!--/.col (right) -->
        
        </div>
    </div>   
</div>
@endsection

