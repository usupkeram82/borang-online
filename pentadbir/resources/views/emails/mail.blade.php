Assalammulaikum <strong>{{ $name }}</strong><br><br>
<h3>PERMOHONAN JAWATAN NAZIR SEKOLAH DI JEMAAH NAZIR (JN) KEMENTERIAN PENDIDIKAN MALAYSIA</h3>
<p>
    Nama : {{ $name }}<br>
    No Kad Pengenalan :{{ $nokp }}<br><br>
    Dengan hormatnya saya merujuk perkara diatas.
</p>
<p style="text-align: justify" >
    2. Dimaklumkan bahawa Jemaah Nazir telah menerima permohonan tuan/puan untuk menjawat jawatan Nazir Sekolah 
    dan mengucapkan terima kasih kasih atas minat untuk berkhidmat di Bahagian ini.<br><br>
    3. Bagaimanapun, sukacita dimaklumkan bahawa temuduga tuan/puan telah berjaya kerana memenuhi syarat 
    dan kriteria permohonan.<br><br>
    4. Bahagian ini mengucapkan tahniah kepada tuan/puan. Sehubungan itu, diharap tuan/puan bersedia untuk sesi pencerapan
    yang akan berlangsung pada :
</p><br>
<p>
    TARIKH: {{ date('d-m-Y') }}<br>
    TEMPAT: XXXX<br>
    MASA  : XXXX<br><br>

    Sekian, terima kasih.<br>
    <b>
        PRIHATIN RAKYAT: DARURAT MEMERANGI COVID-19 <br>
        BERKHIDMAT UNTUK NEGARA
    </b>
</p><br>

<hr><p style="text-align: center"><i>Emel ini dihantar secara automatik oleh komputer, sila jangan balas emel ini</i></p>