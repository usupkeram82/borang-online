@extends('layouts.apps')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">SENARAI PENDEK TEMUDUGA JEMAAH NAZIR BAHARU</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="">
            <thead class="label label-sm bg-navy">
                <tr>
                    <th><center>Nama Pemohon</center></th>
                    <th><center>Kad Pengenalan</center></th>  
                    <th><center>Emel</center></th>
                    <th><center>Tarikh Permohonan</center></th>
                    <th><center>Lulus/Gagal</center></th>    							
                </tr>
            </thead>
                <tbody id="">
                      <tr>
                      <td><center>JOHN DOE</center></td>
                      <td><center>123456789</center></td>
                      <td><center>john@gmailcom</center></td>
                      <td><center>01/01/2022</center></td>.
                      <td><center>
                          <div class="form-group">
                              <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="option1">
                                <label for="customCheckbox1" class="custom-control-label"></label>
                              </div>
                          </div>
                      </center>
                      </td>
                      </tr>
                      
                      <tr>
                      <td><center>SAM JOE</center></td>
                      <td><center>123456789</center></td>
                      <td><center>sam@gmailcom</center></td>
                      <td><center>01/01/2022</center></td>.
                      <td><center>
                          <div class="form-group">
                              <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="customCheckbox3" value="option2">
                                <label for="customCheckbox3" class="custom-control-label"></label>
                              </div>
                          </div>
                      </center>
                      </td>
                      </tr>

                      <tr>
                          <td><center>SAM JOE</center></td>
                          <td><center>123456789</center></td>
                          <td><center>sam@gmailcom</center></td>
                          <td><center>01/01/2022</center></td>.
                          <td><center>
                              <div class="form-group">
                                  <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckbox3" value="option3">
                                    <label for="customCheckbox3" class="custom-control-label"></label>
                                  </div>
                              </div>
                          </center>
                          </td>
                      </tr>

                      <tr>
                          <td><center>SAM JOE</center></td>
                          <td><center>123456789</center></td>
                          <td><center>sam@gmailcom</center></td>
                          <td><center>01/01/2022</center></td>.
                          <td><center>
                              <div class="form-group">
                                  <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckbox4" value="option4">
                                    <label for="customCheckbox4" class="custom-control-label"></label>
                                  </div>
                              </div>
                          </center>
                          </td>
                      </tr>

                    </tr>  
                </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
</div>
@endsection
