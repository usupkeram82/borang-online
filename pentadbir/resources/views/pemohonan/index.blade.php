@extends('layouts.apps')
@section('includecss')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">SENARAI PERMOHONAN BAHARU JEMAAH NAZIR</h3>
                    </div>
                    <div class="card-body">
                        {{-- <div class="row">
                            <div class="col-md-12">
                                <form action="">
                                    <div class="col-md-12 mb-2">
                                        <div class="row">
                                        <form method="get" action="/carian">
                                            @csrf
                                            <div class="col-md-6"></div>
                                            <div class="col-md-2 mt-1 text-right">Cari</div>
                                            <div class="col-md-3 mt-1">
                                                <input type="text" name="carian" class="form-control">
                                            </div>
                                            <div class="col-md-1 mt-1">
                                                <button type="submit" class="form-control btn btn-success">CARI</button>
                                            </div>
                                        </form>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6"></div>
                                            <div class="col-md-2 mt-1 text-right">Set Status</div>
                                            <div class="col-md-3 mt-1">
                                                <select name="status" class="form-control">
                                                    <option value="">--Sila Pilih--</option>
                                                    <option value="Layak">Layak</option>
                                                    <option value="Tidak Layak">Tidak Layak</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1 mt-1">
                                                <button type="button" class="form-control btn btn-success" id="setbtn">SET</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> --}}
                        {{-- <div class="row">
                            <div class="col-md-12"> --}}
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead class="label label-sm bg-navy">
                                        <tr>
                                            <th>Bil</th>
                                            <th>Pilih</th>
                                            <th>Nama Pemohon</th>
                                            <th>Kad Pengenalan</th>
                                            <th>Emel</th>
                                            <th class="text-center">Tarikh Permohonan</th>
                                            <th class="text-center">Senarai Pendek</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = $data->firstItem();
                                        @endphp
                                        @foreach ($data as $d)

                                            <tr>
                                                <td class="text-center">{{ $no++ }}</td>
                                                <td class="text-center">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="cstatus[]"
                                                            id="c{{ $d->pemohon_id }}" value="{{ $d->pemohon_id }}">
                                                        <label for="c{{ $d->pemohon_id }}"
                                                            class="custom-control-label"></label>
                                                    </div>
                                                </td>
                                                <td>{{ $d->pem_nama_main }}</td>
                                                <td><a href="http://localhost/admin-borang/pdfFromDB/pdf_BPermohonan.php?u={{ $d->pem_nokp_baru }}" target="_blank">{{ $d->pem_nokp_baru }}</a></td>
                                                <td>{{ $d->pem_email }}</td>
                                                <td class="text-center">{{ $d->pem_created_date }}</td>
                                                <td class="text-center">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input"
                                                                name="tstatus[]" id="{{ $d->pemohon_id }}"
                                                                {{-- @php
                                                                    if ($d->pem_tapisan_status == 'Ya') {
                                                                        echo 'checked';
                                                                    }
                                                                @endphp --}}
                                                                onclick="ajaxUpdateStatus({{ $d->pemohon_id }});">
                                                            <label class="custom-control-label"
                                                                for="{{ $d->pemohon_id }}"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            {{-- </div>
                        </div> --}}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection
@section('includejs')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
@endsection
@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true, 
            "lengthChange": false, 
            "autoWidth": true,
            "buttons": ["excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        // AJAX function. get list of penilaian
        function ajaxUpdateStatus(pemohon_id) {
            $('#' + pemohon_id).load('/admin-borang/pentadbir/public/pemohonan/update-status?pemohon_id=' + pemohon_id);
        }


        $('#setbtn').click(function(){
            let pilihan = 
            alert("Button clicked");

        });
        
    </script>

@endsection
