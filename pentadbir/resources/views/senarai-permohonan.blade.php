@extends('layouts.apps')

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">SENARAI PERMOHONAN BAHARU JEMAAH NAZIR</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="">
            <thead class="label label-sm bg-navy">
                <tr>
                    <th>Bil</th>
                    <th>Nama Pemohon</th>
                    <th>Kad Pengenalan</th>  
                    <th>Emel</th>
                    <th class="text-center">Tarikh Permohonan</th>
                    <th class="text-center">Senarai Pendek</th>    							
                </tr>
            </thead>
                <tbody>
                  @foreach ($data as $d)                   
                  
                    <tr>
                        <td><a href="">{{ $d->pem_nama_main }}</a></td>
                        <td>{{ $d->pem_nokp_baru }}</td>
                        <td>{{ $d->pem_email }}</td>
                        <td class="text-center"></td>.
                        <td class="text-center">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                  <input class="custom-control-input" type="checkbox" id="customCheckbox1" value="option1">
                                  <label for="customCheckbox1" class="custom-control-label">Layak/Tidak Layak</label>
                                </div>
                            </div>
                        </td>
                    </tr>
                  @endforeach
                      <tr>
                      <td><center>SAM JOE</center></td>
                      <td><center>123456789</center></td>
                      <td><center>sam@gmailcom</center></td>
                      <td><center>01/01/2022</center></td>.
                      <td><center>
                          <div class="form-group">
                              <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="customCheckbox3" value="option2">
                                <label for="customCheckbox3" class="custom-control-label">Layak/Tidak Layak</label>
                              </div>
                          </div>
                      </center>
                      </td>
                      </tr>

                      <tr>
                          <td><center>SAM JOE</center></td>
                          <td><center>123456789</center></td>
                          <td><center>sam@gmailcom</center></td>
                          <td><center>01/01/2022</center></td>.
                          <td><center>
                              <div class="form-group">
                                  <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckbox3" value="option3">
                                    <label for="customCheckbox3" class="custom-control-label">Layak/Tidak Layak</label>
                                  </div>
                              </div>
                          </center>
                          </td>
                      </tr>

                      <tr>
                          <td><center>SAM JOE</center></td>
                          <td><center>123456789</center></td>
                          <td><center>sam@gmailcom</center></td>
                          <td><center>01/01/2022</center></td>.
                          <td><center>
                              <div class="form-group">
                                  <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckbox4" value="option4">
                                    <label for="customCheckbox4" class="custom-control-label">Layak/Tidak Layak</label>
                                  </div>
                              </div>
                          </center>
                          </td>
                      </tr>

                    </tr>  
                </tbody>
            </table>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
</div>
@endsection
