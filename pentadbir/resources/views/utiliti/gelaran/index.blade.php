@extends('layouts.apps')
@section('includecss')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">SENARAI GELARAN</h3>
                    </div>
                    <div class="card-body">
                      <a href="{{ route('create-glr') }}" class="btn btn-success btn-sm mb-1" style="float:right">Tambah</a>
                      <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr class="bg-gray">
                            <th class="text-center">#ID</th>
                            <th >Gelaran</th>
                            <th>Singkatan</th>
                            <th>Status</th>
                            <th class="text-center">#</th>
                          </tr>
                          </thead>
                          <tbody>
                          @foreach ($gelaran as $g)
                            <tr>
                              <td class="text-center">{{ $g->gelaran_id }}</td>
                              <td>{{ $g->gelaran }}</td>
                              <td>{{ $g->singkatan }}</td>
                              <td>{{ $g->status }}</td>
                              <td class="text-center">
                                <a href="{{ route('edit-glr', ['gelaran_id' => $g->gelaran_id ] ) }}">
                                  <i class="fas fa-edit"></i>
                                </a>
                                <a href="{{ route('delete-glr', ['gelaran_id' => $g->gelaran_id ] ) }}"
                                    onclick=" return confirm('Anda pasti untuk padam')">
                                    <i class="fas fa-trash text-danger"></i>
                                </a>
                              </td>
                            </tr>               
                          @endforeach                                        
                          </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection
@section('includejs')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
@endsection
@section('js')
    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, 
        "lengthChange": false, 
        "autoWidth": true,
        "buttons": ["excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        // $('#example2').DataTable({
        // "paging": true,
        // "lengthChange": false,
        // "searching": false,
        // "ordering": true,
        // "info": true,
        // "autoWidth": false,
        // "responsive": true,
        // });
    });
    </script>
@endsection
