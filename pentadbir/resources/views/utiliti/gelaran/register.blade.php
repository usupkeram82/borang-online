@extends('layouts.apps')s
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">SENARAI GELARAN</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('save-glr') }}" method="POST">
                        <input type="hidden" name="gelaran_id" value="{{ $gelaran->gelaran_id }}">
                        @csrf
                        <div class="row mt-2">
                            <div class="col-md-2">Gelaran</div>
                            <div class="col-md-6"> 
                                <input type="text" name="gelaran" id="gelaran" class="form-control" value="{{ $gelaran->gelaran }}">
                                @error('gelaran')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">Singkatan</div>
                            <div class="col-md-6">
                                <input type="text" name="singkatan" id="singkatan" class="form-control" value="{{ $gelaran->singkatan }}">
                                @error('singkatan')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">Status</div>
                            <div class="col-md-2">
                                <select name="status" id="status" class="form-control">
                                    <option value="" 
                                    @if ($gelaran->status == "")
                                        selected
                                    @endif
                                    >--Sila Pilih--</option>
                                    <option value="Aktif"
                                    @if ($gelaran->status == "Aktif")
                                        selected
                                    @endif
                                    >Aktif</option>
                                    <option value="Tidak Aktif"
                                    @if ($gelaran->status == "Tidak Aktif")
                                        selected
                                    @endif
                                    >Tidak Aktif</option>
                                </select>
                                @error('status')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>                    
                        </div>                
                        <div class="row mt-2">
                            <div class="col-md-2"></div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-primary" value="Simpan">
                                <a href="{{ route('index-jwt') }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
