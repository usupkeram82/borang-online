@extends('layouts.apps')s
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">SENARAI JAWATAN</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('save-jwt') }}" method="POST">
                        <input type="hidden" name="jawatan_id" value="{{ $jawatan->jawatan_id }}">
                        @csrf
                        <div class="row mt-2">
                            <div class="col-md-2">Kod Jawatan</div>
                            <div class="col-md-6"> 
                                <input type="text" name="jwt_kod" id="jwt_kod" class="form-control" value="{{ $jawatan->jwt_kod }}">
                                @error('jwt_kod')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">Jawatan</div>
                            <div class="col-md-6">
                                <input type="text" name="jwt_keterangan" id="jwt_keterangan" class="form-control" value="{{ $jawatan->jwt_keterangan }}">
                                @error('jwt_keterangan')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">Status</div>
                            <div class="col-md-2">
                                <select name="jwt_status" id="jwt_status" class="form-control">
                                    <option value="" 
                                    @if ($jawatan->jwt_status == "")
                                        selected
                                    @endif
                                    >--Sila Pilih--</option>
                                    <option value="Aktif"
                                    @if ($jawatan->jwt_status == "Aktif")
                                        selected
                                    @endif
                                    >Aktif</option>
                                    <option value="Tidak Aktif"
                                    @if ($jawatan->jwt_status == "Tidak Aktif")
                                        selected
                                    @endif
                                    >Tidak Aktif</option>
                                </select>
                                @error('jwt_status')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>                    
                        </div>                
                        <div class="row mt-2">
                            <div class="col-md-2"></div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-primary" value="Simpan">
                                <a href="{{ route('index-jwt') }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
