@extends('layouts.apps')s
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">SENARAI NEGERI</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('save-negeri') }}" method="POST">
                        <input type="hidden" name="negeri_id" value="{{ $negeri->negeri_id }}">
                        @csrf
                        <div class="row mt-2">
                            <div class="col-md-2">Kod Negeri</div>
                            <div class="col-md-6"> 
                                <input type="text" name="negeri_code" id="negeri_code" class="form-control" value="{{ $negeri->negeri_code }}">
                                @error('negeri_code')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">Negeri</div>
                            <div class="col-md-6">
                                <input type="text" name="negeri_name" id="negeri_name" class="form-control" value="{{ $negeri->negeri }}">
                                @error('negeri_name')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">Status</div>
                            <div class="col-md-2">
                                <select name="negeri_status" id="negeri_status" class="form-control">
                                    <option value="" 
                                    @if ($negeri->negeri_status == "")
                                        selected
                                    @endif
                                    >--Sila Pilih--</option>
                                    <option value="Aktif"
                                    @if ($negeri->negeri_status == "Aktif")
                                        selected
                                    @endif
                                    >Aktif</option>
                                    <option value="Tidak Aktif"
                                    @if ($negeri->negeri_status == "Tidak Aktif")
                                        selected
                                    @endif
                                    >Tidak Aktif</option>
                                </select>
                                @error('negeri_status')
                                    <label class="text-danger">{{ $message }}</label>
                                @enderror
                            </div>                    
                        </div>                
                        <div class="row mt-2">
                            <div class="col-md-2"></div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-primary" value="Simpan">
                                <a href="{{ route('index-negeri') }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
    <script>
        // // on load
        // $(function() {
        //     let id_jpn = $('[name=id_jpn]').val();
        //     let id_ppd = $('[name=id_ppd]').val();
        //     getPPD(id_jpn);
        // });

        // $('[name=id_jpn]').change(function() {
        //     let id_jpn = $(this).val();
        //     getPPD(id_jpn);
        // });


        // //AJAX function. get list of penilaian
        // function getPPD(id_jpn) {
        //     //GET PPD LIST
        //     let url = '/selenggara-aktiviti/ajax-ppd?id_jpn=' + id_jpn;
        //     $.get(url, function(data) {
        //         // bg event pd dropdwon yg baru (penilaian)
        //         $('#list-ppd').html(data);
        //     });
        // }
    </script>
@endsection
