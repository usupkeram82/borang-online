@extends('layouts.apps')
@section('includecss')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">SENARAI PENCRAPAN</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="">
                                    <div class="col-md-12 mb-2">
                                        <div class="row">
                                            <div class="col-md-5"></div>
                                            <div class="col-md-2 mt-1 text-right">Set Keputusan</div>
                                            <div class="col-md-3 mt-1">
                                                <select name="status" class="form-control">
                                                    <option value="">--Sila Pilih--</option>
                                                    <option value="Layak">Berjaya</option>
                                                    <option value="Tidak Layak">Tidak Berjaya</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2 mt-1">
                                                <button type="button" class="form-control btn btn-success"
                                                    id="setbtn">SET</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5"></div>
                                            <div class="col-md-2 mt-1 text-right">Hantar Emel</div>
                                            <div class="col-md-3 mt-1">
                                                <select name="status" class="form-control">
                                                    <option value="">--Sila Pilih--</option>
                                                    <option value="Layak">Emel Pencerapan</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2 mt-1">
                                                <button type="button" class="form-control btn btn-success"
                                                    id="setbtn">HANTAR</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead class="label label-sm bg-navy">
                                        <tr>
                                            <th>Bil</th>
                                            <th>Pilih</th>
                                            <th>Nama Pemohon</th>
                                            <th>Kad Pengenalan</th>
                                            <th>Emel</th>
                                            <th class="text-center">Tarikh Temuduga</th>
                                            <th class="text-center">Berjaya?</th>
                                            <th class="text-center">#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = $data->firstItem();
                                        @endphp
                                        @foreach ($data as $d)

                                            <tr>
                                                <td class="text-center">{{ $no++ }}</td>
                                                <td class="text-center">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="cstatus[]"
                                                            id="c{{ $d->pemohon_id }}" value="{{ $d->pemohon_id }}">
                                                        <label for="c{{ $d->pemohon_id }}"
                                                            class="custom-control-label"></label>
                                                    </div>
                                                </td>
                                                <td>{{ $d->pem_nama_main }}</td>
                                                <td>{{ $d->pem_nokp_baru }}</td>
                                                <td>{{ $d->pem_email }}</td>
                                                <td class="text-center">{{ $d->pem_created_date }}</td>.
                                                <td class="text-center">
                                                    <div class="form-group">
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input"
                                                                name="tstatus[]" id="{{ $d->pemohon_id }}"
                                                                @php
                                                                    if ($d->pem_temuduga_status == 'Ya') {
                                                                        echo 'checked';
                                                                    }
                                                                @endphp
                                                                onclick="ajaxUpdateTemduga({{ $d->pemohon_id }});">
                                                            <label class="custom-control-label"
                                                                for="{{ $d->pemohon_id }}"></label>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" data-toggle="modal" data-target="#modal-lg">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <a href="http://localhost/admin-borang/pdfFromDB/pdf_gen.php?u={{ $d->pem_nokp_baru }}"
                                                        target="_blank">
                                                        <i class="fas fa-search "></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Maklumat Pencerapan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="get" action="/carian">
                        @csrf
                        <div class="row">
                            <div class="col-md-2 mt-1">Tarikh Pencerapan</div>
                            <div class="col-md-4 mt-1">
                                <input type="date" name="tarikh_temuduga">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 mt-1">Pegawai 1</div>
                            <div class="col-md-4 mt-1">
                                <input type="text" name="tarikh_temuduga">
                            </div>
                            <div class="col-md-2 mt-1">Emel</div>
                            <div class="col-md-4 mt-1">
                                <input type="email" name="tarikh_temuduga">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 mt-1">Pegawai 2</div>
                            <div class="col-md-4 mt-1">
                                <input type="text" name="tarikh_temuduga">
                            </div>
                            <div class="col-md-2 mt-1">Emel</div>
                            <div class="col-md-4 mt-1">
                                <input type="email" name="tarikh_temuduga">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Simpan Maklumat</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('includejs')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
@endsection
@section('js')
    <script>
        $(function () {
            $("#example1").DataTable({
            "responsive": true, 
            "lengthChange": false, 
            "autoWidth": true,
            "buttons": ["excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        // AJAX function. get list of penilaian
        function ajaxUpdateStatus(pemohon_id) {
            $('#' + pemohon_id).load('/admin-borang/pentadbir/public/pemohonan/update-status?pemohon_id=' + pemohon_id);
        }


        $('#setbtn').click(function(){
            let pilihan = 
            alert("Button clicked");

        });
        
        // AJAX function. get list of penilaian
        function ajaxUpdateTemduga(pemohon_id) {
            $('#' + pemohon_id).load('/admin-borang/pentadbir/public/update-temuduga?pemohon_id=' + pemohon_id);
        }


        $('#setbtn').click(function() {
            // let pilihan =
            alert("Button clicked");

        });
    </script>

@endsection
