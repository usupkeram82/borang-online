<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FmTblPemohon extends Model
{
    public $table='fm_tbl_pemohon';
    public $primaryKey='pemohon_id';
    public $timestamps=false;
}
