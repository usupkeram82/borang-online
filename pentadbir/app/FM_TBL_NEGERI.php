<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FM_TBL_NEGERI extends Model
{
    public $table='fm_tbl_negeri';
    public $primaryKey='negeri_id';
    public $timestamps=false;
}
