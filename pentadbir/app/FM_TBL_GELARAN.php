<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FM_TBL_GELARAN extends Model
{
    public $table='fm_tbl_gelaran';
    public $primaryKey='gelaran_id';
    public $timestamps=false;
}
