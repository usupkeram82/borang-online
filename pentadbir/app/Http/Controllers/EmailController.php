<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    function index(){

        $to_name = 'USUP KERAM';
        $to_email = 'usupkeram82@gmail.com';
        $data = array(
            'name'=>'En. Usup Bin Keram',
            'nokp'=>'820117-13-5835'
        );
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) { 
            $message->to($to_email, $to_name)->subject('PANGGILAN TEMUDUGA');
            $message->from('usup.keram@moe.gov.my','JEMAAH NAZIR');
        });
        
        return redirect('/senarai-temuduga');
    }
}