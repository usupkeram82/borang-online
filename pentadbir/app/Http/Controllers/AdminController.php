<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    function menu(){
        return view('menu');
    }

    function senarai_permohonan(){
        return view('senarai-permohonan');
    }

    function senarai_temuduga(){
        return view('senarai-temuduga');
    }
   
    function senarai_pencerapan(){
        return view('senarai-pencerapan');
    }
}
