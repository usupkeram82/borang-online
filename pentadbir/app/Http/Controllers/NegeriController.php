<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FM_TBL_NEGERI;
use Validator;

class NegeriController extends Controller
{
    function index(){

        $negeri = FM_TBL_NEGERI::all();
        return view('utiliti/negeri.index', compact('negeri'));
    }

    function create(){
        $negeri = new FM_TBL_NEGERI();
        return view('utiliti/negeri.register', compact('negeri'));
    }

    function edit($negeri_id){
        $negeri = FM_TBL_NEGERI::find($negeri_id);
        return view('utiliti/negeri.register', compact('negeri'));
    }

    function save(Request $req){
        $negeri_id = $req->negeri_id;
        if(empty($negeri_id)) {
            // insert
            $negeri = new FM_TBL_NEGERI();
        } else {
            // update
            $negeri = FM_TBL_NEGERI::find($negeri_id);
        }        
        $negeri->negeri_code = $req->negeri_code;
        $negeri->negeri = $req->negeri_name;
        $negeri->negeri_status = $req->negeri_status;

        $data = $req->all();
        $rules = [
            'negeri_code'       => 'required',
            'negeri_name'       => 'required',
            'negeri_status'     => 'required'
        ];
        $msg = [
            'negeri_code.required'      => 'Sila masukkan Kod Negeri',
            'negeri_name.required'      => 'Sila masukkan Negeri',
            'negeri_status.required'    => 'Sila pilih Status'
        ];
        $v = Validator::make($data, $rules, $msg);
        if($v->fails()) {
            return view('/utiliti/negeri.register', compact('negeri'))->withErrors($v);
        } else {
            // berjaya
            $negeri->save();
            return redirect('/tetapan/negeri/index')->with('msg', 'Maklumat Negeri telah berjaya disimpan');
        } 
    }
    
    function delete($negeri_id) {
        FM_TBL_NEGERI::find($negeri_id)->delete();
        return redirect('/tetapan/negeri/index')->with('msg', 'Rekod telah berjaya dihapuskan');
    }
}