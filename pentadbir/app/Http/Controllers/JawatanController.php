<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FM_TBL_JAWATAN;
use Validator;

class JawatanController extends Controller
{
    function index(){
        $jawatan = FM_TBL_JAWATAN::all();
        return view('utiliti/jawatan.index', compact('jawatan'));
    }

    function create(){
        $jawatan = new FM_TBL_JAWATAN();
        return view('utiliti/jawatan.register', compact('jawatan'));
    }

    function edit($jawatan_id){
        $jawatan = FM_TBL_JAWATAN::find($jawatan_id);
        // dd($jawatan);
        return view('utiliti/jawatan.register', compact('jawatan'));
    }

    function save(Request $req){
        $jawatan_id = $req->jawatan_id;
        if(empty($jawatan_id)) {
            // insert
            $jawatan = new FM_TBL_JAWATAN();
        } else {
            // update
            $jawatan = FM_TBL_JAWATAN::find($jawatan_id);
        }        
        $jawatan->jwt_kod= $req->jwt_kod;
        $jawatan->jwt_keterangan = $req->jwt_keterangan;
        $jawatan->jwt_status = $req->jwt_status;

        $data = $req->all();
        $rules = [
            'jwt_kod'       => 'required',
            'jwt_keterangan' => 'required',
            'jwt_status'     => 'required'
        ];
        $msg = [
            'jwt_kod.required'         => 'Sila masukkan Kod Jawatan',
            'jwt_keterangan.required'   => 'Sila masukkan JaWatan',
            'jwt_status.required'       => 'Sila pilih Status'
        ];
        $v = Validator::make($data, $rules, $msg);
        if($v->fails()) {
            return view('/utiliti/jawatan.register', compact('jawatan'))->withErrors($v);
        } else {
            // berjaya
            $jawatan->save();
            return redirect('/tetapan/jawatan/index')->with('msg', 'Maklumat Jawatan telah berjaya disimpan');
        } 
    }
    
    function delete($jawatan_id) {
        FM_TBL_JAWATAN::find($jawatan_id)->delete();
        return redirect('/tetapan/jawatan/index')->with('msg', 'Rekod telah berjaya dihapuskan');
    }
}
