<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FM_TBL_GELARAN;
use Validator;

class GelaranController extends Controller
{
    function index(){
        $gelaran = FM_TBL_GELARAN::all();
        return view('utiliti/gelaran.index', compact('gelaran'));
    }

    function create(){
        $gelaran = new FM_TBL_GELARAN();
        return view('utiliti/gelaran.register', compact('gelaran'));
    }

    function edit($gelaran_id){
        $gelaran = FM_TBL_GELARAN::find($gelaran_id);
        // dd($gelaran);
        return view('utiliti/gelaran.register', compact('gelaran'));
    }

    function save(Request $req){
        $gelaran_id = $req->gelaran_id;
        if(empty($gelaran_id)) {
            // insert
            $gelaran = new FM_TBL_GELARAN();
        } else {
            // update
            $gelaran = FM_TBL_GELARAN::find($gelaran_id);
        }        
        $gelaran->gelaran= $req->gelaran;
        $gelaran->singkatan = $req->singkatan;
        $gelaran->status = $req->status;

        $data = $req->all();
        $rules = [
            'gelaran'   => 'required',
            'singkatan' => 'required',
            'status'    => 'required'
        ];
        $msg = [
            'gelaran.required'      => 'Sila masukkan Gelaran',
            'singkatan.required'    => 'Sila masukkan Singkatan Gelaran',
            'status.required'       => 'Sila pilih Status'
        ];
        $v = Validator::make($data, $rules, $msg);
        if($v->fails()) {
            return view('/utiliti/gelaran.register', compact('gelaran'))->withErrors($v);
        } else {
            // berjaya
            $gelaran->save();
            return redirect('/tetapan/gelaran/index')->with('msg', 'Maklumat Gelaran telah berjaya disimpan');
        } 
    }
    
    function delete($gelaran_id) {
        FM_TBL_GELARAN::find($gelaran_id)->delete();
        return redirect('/tetapan/gelaran/index')->with('msg', 'Rekod telah berjaya dihapuskan');
    }
}
