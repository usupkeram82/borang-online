<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FmTblPemohon;
use Collective\Html\Eloquent\FormAccessible;

class PemohonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FmTblPemohon::paginate(5);
        return view('pemohonan.index', compact('data'));
    }

    public function temuduga()
    {
        $data = FmTblPemohon::where('pem_tapisan_status','Ya')->paginate(5);
        return view('temuduga.index', compact('data'));
    }

    public function pencerapan()
    {
        $data = FmTblPemohon::where('pem_temuduga_status','Ya')->paginate(5);
        return view('pencerapan.index', compact('data'));
    }


    public function ajaxUpdateStatus(Request $req){
        //Update status permohonan
        $pemohon_id = $req->pemohon_id;
        $rs = FmTblPemohon::find($pemohon_id);
 
        if($rs->pem_tapisan_status=='Tidak'){
            $rs->pem_tapisan_status = 'Ya';
        }
        else{
            $rs->pem_tapisan_status = 'Tidak';
        }
        $rs->save();
    }

    public function ajaxUpdateTemuduga(Request $req){
        //Update status temuduga
        $pemohon_id = $req->pemohon_id;
        $rs = FmTblPemohon::find($pemohon_id);
 
        if($rs->pem_temuduga_status=='Tidak'){
            $rs->pem_temuduga_status = 'Ya';
        }
        else{
            $rs->pem_temuduga_status = 'Tidak';
        }
        $rs->save();
    }
}
