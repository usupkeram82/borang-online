<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FM_TBL_JAWATAN extends Model
{
    public $table='fm_tbl_jawatan';
    public $primaryKey='jawatan_id';
    public $timestamps=false;
}